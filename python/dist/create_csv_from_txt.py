#-----------------------------------------------------------------------
#   Classifies a unkown text to a institute using LinearSVC.
#
#   Project: IP6 FS18 FHNW
#   Author: Kevin Kirn <kevin.kirn@students.fhwn.ch>
#           Hoang Tran <hoang.tran@students.fhnw.ch>
#
#   USAGE: 
#     create_csv_from_txt.py "PATH TO TXT FILES" "PATH TO TRAIN CSV"
#-----------------------------------------------------------------------

import sys
import io
import re
import load_train_data as loader

documents = loader.get_doc(sys.argv[1])

lines = []
lines.append('"Institute";"Problem"')
for d in documents:
    words = " ".join(d[0])
    tag =  re.search("_[a-zA-Z0-9]*\.txt", str(d[1]), re.IGNORECASE).group(0)
    clean_tag = tag.replace('_', '').replace('.txt', '')
    lines.append('"{0}";"{1}"'.format(clean_tag, words))

with io.open(sys.argv[2],'w',encoding='utf8') as csv:
    for line in lines:
        csv.write(line + '\n')

csv.close()