#-----------------------------------------------------------------------
#   Loads text documents, cleans them and creates TaggedDocuments.
#
#   Project: IP6 FS18 FHNW
#   Author: Kevin Kirn <kevin.kirn@students.fhwn.ch>
#           Hoang Tran <hoang.tran@students.fhnw.ch>
#-----------------------------------------------------------------------
import io
import gensim
import os
import re
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim.models.doc2vec import TaggedDocument

# get a list of all text (.txt) files in a folder
def get_file_list(folder_name):
    file_list = [folder_name+'/'+name for name in os.listdir(folder_name) if name.endswith('txt')]
    print ('Found %s documents under the dir %s .....'%(len(file_list),folder_name))
    return file_list

# returns content for each file provided
def get_doc_list(file_list):
    doc_list = []
    
    for file in file_list:
        print(file)
        st = ""
        with io.open(file,'r',encoding='utf8') as f:
            st = f.read()
        doc_list.append(st)

    return doc_list

# creates TaggedDocuments for each file within the provided folder
# performes preprocessing such as stop word removal or tokenization
def get_doc(folder_name):
    file_list = get_file_list(folder_name)
    doc_list = get_doc_list(file_list)
 
    taggeddoc = []
    
    for index,i in enumerate(doc_list): 
        tokens = clean_and_tokenize_text(i)
        td = TaggedDocument(tokens, tags=[file_list[index]])
        taggeddoc.append(td)
 
    return taggeddoc

def clean_and_tokenize_text(text):
    p_stemmer = PorterStemmer()
    tokenizer = RegexpTokenizer(r'\w+')

    raw = text.lower()
    tokens = tokenizer.tokenize(raw)

    # stem tokens
    stemmed_tokens = [p_stemmer.stem(i) for i in tokens]
    
    # remove empty
    return gensim.utils.to_unicode(str.encode(' '.join(stemmed_tokens))).split()

def get_file_list_with_tag(folder_name, tag):
    file_list = [folder_name+'/'+name for name in os.listdir(folder_name) if name.endswith('_' + tag + '.txt')]
    print ('Found %s documents under the dir %s .....'%(len(file_list),folder_name))
    return file_list