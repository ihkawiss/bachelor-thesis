#-----------------------------------------------------------------------
#   Classifies a unkown text to a institute using LinearSVC.
#
#   Project: IP6 FS18 FHNW
#   Author: Kevin Kirn <kevin.kirn@students.fhwn.ch>
#           Hoang Tran <hoang.tran@students.fhnw.ch>
#
#   USAGE: 
#     problem_classifier.py "SOME TEXT TO CLASSIFY" "PATH TO TRAIN CSV"
#-----------------------------------------------------------------------
import pandas as pd
import sys
import load_train_data as loader
from io import StringIO
from sklearn.feature_extraction.text import TfidfVectorizer

# read train csv file
df = pd.read_csv(sys.argv[2], sep=';')

# clean up and map the data
col = ['Institute','Problem']
df = df[col]
df = df[pd.notnull(df['Problem'])]
df.columns = ['Institute','Problem']
df['institute_id'] = df['Institute'].factorize()[0]
category_id_df = df[['Institute', 'institute_id']].drop_duplicates().sort_values('institute_id')
category_to_id = dict(category_id_df.values)
id_to_category = dict(category_id_df[['institute_id', 'Institute']].values)

# find features in texts
tfidf = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='utf-8', ngram_range=(1, 2))
features = tfidf.fit_transform(df.Problem).toarray()
labels = df.institute_id

# train the model
from sklearn.svm import LinearSVC
model = LinearSVC()
model.fit(features, labels)

# extract features from input
taggedDoc = loader.clean_and_tokenize_text(sys.argv[1])
words = " ".join(taggedDoc)
texts = [words]
text_features = tfidf.transform(texts)

# predict and print institute class
predictions = model.predict(text_features)
for text, predicted in zip(texts, predictions):
  print(id_to_category[predicted])