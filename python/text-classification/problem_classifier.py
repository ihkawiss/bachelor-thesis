# TODO: Refactor!
# read train csv file
import pandas as pd
import sys
df = pd.read_csv(sys.argv[2], sep=';')

# clean up and map dataset
from io import StringIO
col = ['Institute','Problem']
df = df[col]
df = df[pd.notnull(df['Problem'])]
df.columns = ['Institute','Problem']

df['institute_id'] = df['Institute'].factorize()[0]
category_id_df = df[['Institute', 'institute_id']].drop_duplicates().sort_values('institute_id')
category_to_id = dict(category_id_df.values)
id_to_category = dict(category_id_df[['institute_id', 'Institute']].values)

# find features in texts
from sklearn.feature_extraction.text import TfidfVectorizer
from stop_words import get_stop_words
german_stop_words = get_stop_words('de')

tfidf = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='utf-8', ngram_range=(1, 2), stop_words=german_stop_words)

features = tfidf.fit_transform(df.Problem).toarray()
labels = df.institute_id

# Model Evaluation
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC
model = LinearSVC()
X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33, random_state=0)
model.fit(X_train, y_train)

import sys
sys.path.append('/Users/kevin/bachelor-thesis/python/doc2vec')
import load_train_data as loader


taggedDoc = loader.clean_and_tokenize_text(sys.argv[1])
words = " ".join(taggedDoc)

texts = [words]
text_features = tfidf.transform(texts)
predictions = model.predict(text_features)
for text, predicted in zip(texts, predictions):
  print(id_to_category[predicted])