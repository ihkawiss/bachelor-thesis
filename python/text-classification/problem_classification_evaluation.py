# read train csv file
import pandas as pd
df = pd.read_csv('data.csv', sep=';')

# clean up and map dataset
from io import StringIO
col = ['Institute','Problem']
df = df[col]
df = df[pd.notnull(df['Problem'])]
df.columns = ['Institute','Problem']

df['institute_id'] = df['Institute'].factorize()[0]
category_id_df = df[['Institute', 'institute_id']].drop_duplicates().sort_values('institute_id')
category_to_id = dict(category_id_df.values)
id_to_category = dict(category_id_df[['institute_id', 'Institute']].values)

# print balanceing of classes
import matplotlib.pyplot as plt
#fig = plt.figure(figsize=(8,6))
#df.groupby('Institute').Problem.count().plot.bar(ylim=0)
#plt.show()

# find features in texts
from sklearn.feature_extraction.text import TfidfVectorizer
from stop_words import get_stop_words
german_stop_words = get_stop_words('de')

tfidf = TfidfVectorizer(sublinear_tf=False, min_df=5, norm='l2', use_idf=True, encoding='utf-8', ngram_range=(1, 2))

features = tfidf.fit_transform(df.Problem).toarray()
labels = df.institute_id
print(f"features built from learning data {features.shape}")

# find most correlated terms per class
from sklearn.feature_selection import chi2
import numpy as np

N = 2
for Institute, category_id in sorted(category_to_id.items()):
    features_chi2 = chi2(features, labels == category_id)
    indices = np.argsort(features_chi2[0])
    feature_names = np.array(tfidf.get_feature_names())[indices]
    unigrams = [v for v in feature_names if len(v.split(' ')) == 1]
    bigrams = [v for v in feature_names if len(v.split(' ')) == 2]
    print("# '{}':".format(Institute))
    print("  . Most correlated unigrams:\n. {}".format('\n. '.join(unigrams[-N:])))
    print("  . Most correlated bigrams:\n. {}".format('\n. '.join(bigrams[-N:])))

# naive bayes
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
X_train, X_test, y_train, y_test = train_test_split(df['Problem'], df['Institute'], random_state = 0)
count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(X_train)
tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
clf = MultinomialNB().fit(X_train_tfidf, y_train)

# model selection
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import cross_val_score
from sklearn.dummy import DummyClassifier
models = [
    LinearSVC(),
    MultinomialNB(),
    DummyClassifier(strategy="stratified")
]
CV = 5
cv_df = pd.DataFrame(index=range(CV * len(models)))
entries = []
for model in models:
  model_name = model.__class__.__name__
  accuracies = cross_val_score(model, features, labels, scoring='accuracy', cv=CV)
  for fold_idx, accuracy in enumerate(accuracies):
    entries.append((model_name, fold_idx, accuracy))
cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])
import seaborn as sns
sns.boxplot(x='model_name', y='accuracy', data=cv_df)
sns.stripplot(x='model_name', y='accuracy', data=cv_df, size=8, jitter=True, edgecolor="gray", linewidth=2)
plt.show()

print(cv_df.groupby('model_name').accuracy.mean())

# Model Evaluation
model = LinearSVC()
X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33, random_state=0)
model.fit(X_train, y_train)
y_pred = model.predict(X_test)
from sklearn.metrics import confusion_matrix
conf_mat = confusion_matrix(y_test, y_pred)
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(conf_mat, annot=True, fmt='d', xticklabels=category_id_df.Institute.values, yticklabels=category_id_df.Institute.values)
plt.ylabel('Actual')
plt.xlabel('Predicted')
plt.show()

from sklearn import metrics
print(metrics.classification_report(y_test, y_pred, target_names=df['Institute'].unique()))