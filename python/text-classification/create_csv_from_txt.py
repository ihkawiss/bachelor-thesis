import sys
import io
import re
sys.path.append('/Users/kevin/bachelor-thesis/python/doc2vec')
import load_train_data as loader

documents = loader.get_doc(sys.argv[1])

lines = []
lines.append('"Institute";"Problem"')
for d in documents:
    words = " ".join(d[0])
    tag =  re.search("_[a-zA-Z0-9]*\.txt", str(d[1]), re.IGNORECASE).group(0)
    clean_tag = tag.replace('_', '').replace('.txt', '')
    lines.append('"{0}";"{1}"'.format(clean_tag, words))

with io.open(sys.argv[2],'w',encoding='utf8') as csv:
    for line in lines:
        csv.write(line + '\n')

csv.close()