#-----------------------------------------------------------------------
#   Trains and saves a doc2vec model.
#
#   Project: IP6 FS18 FHNW
#   Author: Kevin Kirn <kevin.kirn@students.fhwn.ch>
#           Hoang Tran <hoang.tran@students.fhnw.ch>
#-----------------------------------------------------------------------

import gensim.models as g
import logging
import load_train_data as loader

#enable logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger('train_doc2vec_model')

# doc2vec settings
vector_size = 300
window_size = 15
min_count = 1
sampling_threshold = 1e-5
negative_size = 5
train_epoch = 200
dm = 0 #0 = dbow; 1 = dmpv
worker_count = 1

#input corpus
documents = loader.get_doc('../project-offers/cleaned_hoang/')

#output model
saved_path = "doc2vec_model.bin"

# train doc2vec model
# pretrained_emb=pretrained_emb,
model = g.Doc2Vec(documents, size=vector_size, window=window_size, min_count=min_count, sample=sampling_threshold, workers=worker_count, hs=0, dm=dm, negative=negative_size, dbow_words=1, dm_concat=1, iter=train_epoch)

#save model
model.save(saved_path)

logger.info("Doc2Vec model was saved at " + saved_path)