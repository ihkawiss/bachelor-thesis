import load_train_data as loader
from gensim.models import Doc2Vec

# load trained doc2vec model from disk
model = Doc2Vec.load('doc2vec_model.bin')

st = "Möbelsuche Benutzer Möbel Möbelbilder Algorithmus Möbelstücke Benutzer Möbelstücke Geschmack Basierend Auswahl App Auswahl Möbelvorschläge Beispielablauf Benutzer Sofas Benutzer Bilder Sofa Suchresultate Sofas Kategorie Sofa Machine Learning Applikation Suchvorschlägen Filters Suchbegriffen Benutzer Klassifizierungen Bildern Suchresultat Möbel Möbeln Möbelstück Geschmack Geschmack Worte Filter Suchresultate Grund Listen Sofas Tischen Stühle Problem Geschmack Suchbegriffe Filter Möbelbilder Geschmack Tool Abhilfe Webapp Technologie Frontend Backend Projektverlauf UI Art Suche UI Benutzer Bilder Computer Vision Deep Learning Machine Learning Machine Learning"
tokens = loader.clean_and_tokenize_text(st)
new_vector = model.infer_vector(tokens, alpha=0.025, steps=1000)

print(model.docvecs.most_similar([new_vector]))