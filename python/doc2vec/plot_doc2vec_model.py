#-----------------------------------------------------------------------
#   Plots the doc2vec model vecotrs in a two-dimensional space.
#
#   Project: IP6 FS18 FHNW
#   Author: Kevin Kirn <kevin.kirn@students.fhwn.ch>
#           Hoang Tran <hoang.tran@students.fhnw.ch>
#-----------------------------------------------------------------------

import os
import load_train_data as loader
import numpy as np
import matplotlib.pyplot as plt
from gensim.models import Doc2Vec
from sklearn.manifold import TSNE

# load trained doc2vec model from disk
model = Doc2Vec.load('doc2vec_model.bin')

# plot I4DS vectors
imvs_files = loader.get_file_list_with_tag('../project-offers/cleaned_hoang', 'I4DS')
imvs_vectors = []
for file in imvs_files:
    st = open(file,'r').read()
    tokens = loader.clean_and_tokenize_text(st)
    new_vector = model.infer_vector(tokens, alpha=0.025, steps=1000)
    imvs_vectors.append(new_vector)
    print(file)

# transform vectors to 2D
tsne = TSNE(n_components=2, random_state=0)
Y = tsne.fit_transform(imvs_vectors)

# build cooridnate arrays
x_coords = Y[:, 0]
y_coords = Y[:, 1]

# save the plot
plt.scatter(x_coords, y_coords)

# plot IMVS vectors
imvs_files = loader.get_file_list_with_tag('../project-offers/cleaned_hoang', 'IMVS')
imvs_vectors = []
for file in imvs_files:
    st = open(file,'r').read()
    tokens = loader.clean_and_tokenize_text(st)
    new_vector = model.infer_vector(tokens, alpha=0.025, steps=1000)
    imvs_vectors.append(new_vector)
    print(file)

# transform vectors to 2D
tsne = TSNE(n_components=2, random_state=0)
Y = tsne.fit_transform(imvs_vectors)

# build cooridnate arrays
x_coords = Y[:, 0]
y_coords = Y[:, 1]

# save the plot
plt.scatter(x_coords, y_coords, color='r')

# plot IMVS vectors
imvs_files = loader.get_file_list_with_tag('../project-offers/cleaned_hoang', 'IIT')
imvs_vectors = []
for file in imvs_files:
    st = open(file,'r').read()
    tokens = loader.clean_and_tokenize_text(st)
    new_vector = model.infer_vector(tokens, alpha=0.025, steps=1000)
    imvs_vectors.append(new_vector)
    print(file)

# transform vectors to 2D
tsne = TSNE(n_components=2, random_state=0)
Y = tsne.fit_transform(imvs_vectors)

# build cooridnate arrays
x_coords = Y[:, 0]
y_coords = Y[:, 1]

# save the plot
plt.scatter(x_coords, y_coords, color='y')

# plot IMVS vectors
'''imvs_files = loader.get_file_list_with_tag('../project-offers/cleaned_hoang', 'ip14')
imvs_vectors = []
for file in imvs_files:
    st = open(file,'r').read()
    tokens = loader.clean_and_tokenize_text(st)
    new_vector = model.infer_vector(tokens, alpha=0.025, steps=1000)
    imvs_vectors.append(new_vector)
    print(file)

# transform vectors to 2D
tsne = TSNE(n_components=2, random_state=0)
Y = tsne.fit_transform(imvs_vectors)

# build cooridnate arrays
x_coords = Y[:, 0]
y_coords = Y[:, 1]

# save the plot
plt.scatter(x_coords, y_coords, color='g')'''

# save the plot
plt.title('Projektausschreibungen', fontsize=13)
plt.savefig('doc2vec_model_plot.png')
plt.show()