


































 
IMVS56: Mobile Unterstützung für die halbautomatische Sicht-
befundung von Bodenproben
 

 
Ausgangslage 
Das Forschungsinstitut für biologischen Landbau (FiBL) berät Land-
wirte bezüglich der ressourcenschonenden und nachhaltigen Nut-
zung ihrer Anbauflächen. Dazu wird der Boden der Anbaufläche un-
tersucht. Die Landwirte werden mit einer mobilen Applikation durch
den Beprobungsprozess geführt. 
  
In einem P5 Vorgängerprojekt wurde eine Applikation auf Basis von
JavaFX, JavaFXPorts und Gluon für die Android und iOS Platfor-
men erstellt, die die grundlegenden Prozessschritte abdeckt. Darauf
aufbauend möchte das FiBL noch mehr Mehrwert bieten, z.B. eine
halbautomatische Sichtbefundung der Bodenprobe. 
 
Ziel der Arbeit 
Das Ziel ist, die bestehende Applikation um halbautomatische Sicht-
befundung zu erweitern: mit dem Smartphone wird ein Bild der Spa-
tenprobe erfasst und ggf. mit manuellen Eingriffen bearbeitet. Aus
dem Bild soll eine vergleichbare Klassifikation der Bodenprobe erfol-
gen, z.B. Tiefe des Pflughorizonts in cm, biologische Aktivität,
Staunässe, etc. 
 
Problemstellung 
Neben den offensichtlichen Herausforderungen bei der Analyse von
Kameraaufnahmen wie der Toleranz für unterschiedliche Beleuch-
tung, Kontrast, Farbspektrum, etc. ist auch die Einbettung des Pro-
zesses in den Ablauf der Spatenprobe und die Umgebungsbedin-
gungen des landwirtschaftlichen Einsatzgebietes (Feuchtigkeit, erdige Hände, schwache Netzabdeckung) zu
beachten. So könnte unter Umständen Sprachsteuerung nützlich sein.  
  
Für das FiBL ist eine solche Anwendung auch ein Werbeträger. Dem muss das Design Rechnung tragen. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 
Java, JavaFX, JavaFXPorts, Gluon, Android, iOS. 
 
Bemerkung 
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 
Dieses Projekt ist für Jennifer Müller und Lukas Marchesi reserviert. 

Betreuer: Dierk König Priorität 1 Priorität 2
Dieter Holz Arbeitsumfang: P6 (360h pro

Student)
P6 (360h pro
Student)

Auftragsgeber: Forschungsinstitut für biologischen
Landbau (FiBL)

Teamgrösse: 2er Team Einzelarbeit

Sprachen: Deutsch oder Englisch

 Computer Science/IMVS/Student projects 18FS

mailto:dierk.koenig@fhnw.ch
mailto:dieter.holz@fhnw.ch

