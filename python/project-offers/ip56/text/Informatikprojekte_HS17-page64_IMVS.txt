


































•
•
•
•
•
•
•
•
•

 
IMVS13: Bessere Updates für VSG
 

 
Ausgangslage 
Die Ventoo GmbH hat im Verlaufe des letzten Jahres eine eigene OPNsense (Open Source Firewall Softwa-
re) Distribution entwickelt, welche Treiber für 4G Modems von Sierra Wireless mit integriert. Auf Basis dieser
Software wird das Ventoo Security Gateway (VSG), eine Hardware Plattform mit 4G Fail-over Verbindung
zur unterbrechungsfreien Internetverbindung, betrieben. Das Aktualisieren der bestehenden Plattform mit
den aktuellsten Security Patches und weiterentwickelten Versionen gestaltet sich derzeit als ein mühsamer
und manueller Prozess. 
 
Ziel der Arbeit 
Um den Update Prozess des VSG einfacher zu gestalten, soll eine Mirror-Server-Infrastruktur, sowie ein
Software Deployment Prozess entwickelt und implementiert werden, welcher das Einfache Überprüfen der
derzeit aktuell laufenden Software auf dem jeweiligen Endgerät sowie das Updaten der Software aus der
Ferne durch eine serverseitige Anwendung ermöglicht. Dabei soll neben dem Update-Zustand auch jeweils
der Health- bzw. Failover-Zustand des jeweiligen Gerätes ermittelt und sichtbar gemacht werden. 
  
Eine optionale Erweiterung wäre das Deployment der jeweilig aktuellen Version der Software als Docker
Image. Dies würde im Fehlerfall ein Einfaches zurückgehen auf ein altes Image ermöglichen bzw. weitere
Anwendungsfälle bieten wie bspw. einen parallel betriebenen Proxy/Content-Filter etc. 
 
Problemstellung 
Sollten die Anforderungen mit der FreeBSD basierenden Open Source Firewall Lösung OPNsense nicht er-
füllt werden können, so darf eine bspw. Linux-basierte OSS Firewall Lösung zum Einsatz kommen. 
  
Der Hauptteil der Herausforderung liegt im sicheren und robusten Aufbau einer Cloud-/Server-basierenden
Update-Infrastruktur sowie eines sicheren und robusten sowie Fehler-minimierenden Deployment-Prozesses
welcher es erlaubt mit wenigen Handgriffen die VSG-Firmware aus der Ferne zu aktualisieren. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

Linux/Unix
Cloud-Dienste (Azure)
Shell Scripts
HTTP-Daemons (e.g. Nginx, Apache)
HTML
CSS
JavaScript
C
Docker 

 
Bemerkungen 
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 
Dieses Projekt ist für Dominic Huggenberger und Dominique Dubler reserviert. 

Betreuer: Martin Gwerder Priorität 1 Priorität 2
Auftraggeber: Ventoo GmbH Arbeitsumfang: P6 (360h pro

Student)
---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

Studiengang Informatik/IMVS/Studierendenprojekte 17HS

mailto:martin.gwerder@fhnw.ch

