


































•
•

•

•
•
•
•

 
i4Ds25: Interactive Competence Network
 

 
Ausgangslage 
Innerhalb von grösseren Organisationen ist es
häufig schwierig, einen aktuellen Überblick über
das verfügbare Wissen zu behalten und Querbe-
züge zwischen Projekten, Kompetenzen und Per-
sonen herzustellen. Gerade aber dieses Wissen
ist zentral für einen aktiven Austausch und eine
interdisziplinäre Kooperation. Das Ziel des Pro-
jekts ist die Entwicklung einer dynamischen, inter-
aktiven Netzwerkvisualisierung, die an einem bei-
spielhaften Datensatz die vorhandenen Kompe-
tenzen, Personen und Projekte anzeigen kann.
Die Applikation soll als Pilot für das i4Ds ent-
wickelt werden und sowohl für eine grossflächige
Projektion wie auch für die Darstellung am Bild-
schirm optimiert sein. Die Applikation soll internen
Mitarbeitenden erleichtern, sich eine Übersicht
über vorhandene Kompetenzen zu verschaffen
und auch Aussenstehenden einen Einblick ins In-
stitut geben können. 
 
Ziel der Arbeit 
Das Ziel dieses Projektes ist die Entwicklung einer Webapplikation. Diese soll eine dynamische und interakti-
ve Darstellung der Projekte, Kompetenzen und Personen an einem beispielhaften Datensatz ermöglichen
und auch eine benutzerfreundliche Verwaltung der Inhalte über eine Eingabemaske anbieten. Die Netzwerk-
visualisierung soll für eine grossformatige Projektion oder die Darstellung auf der Touchwall des i4Ds opti-
miert sein und auch auf einem Bildschirm dargestellt werden können.  
Mit Hilfe dieser Informationsvisualisierung sollen einzelne Inhalte mit ihren Abhängigkeiten und Verbindun-
gen durch einen überzeugende UX angezeigt und explorativ erkundet werden können. 
 
Problemstellung 

Entwickeln, testen und auswerten mehreren Visualisierungskonzepte. 
Implementierung des Konzeptes mit der Möglichkeit einer interaktiven Bedienung auf unterschiedlichen
Medien. 
Die Arbeit erhebt den Anspruch an eine komplexe Visualisierung mit explorativen Interaktionsmöglichkei-
ten. 

 
Technologien/Fachliche Schwerpunkte/Referenzen 

Webtechnologien im Front- und Backend
Evaluation geeigeter Frameworks (z.B. AngularJS, NodeJS, ExpressJS D3.js etc.)
Netzwerkmetriken
UX, Custom UI und Informationsvisualisierung 

Betreuer: Doris Agotai Priorität 1 Priorität 2
Fabian Affolter Arbeitsumfang: P5 (180h pro

Student)
---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

Studiengang Informatik/i4Ds/Studierendenprojekte HS16

mailto:doris.agotai@fhnw.ch
mailto:fabian.affolter@fhnw.ch

