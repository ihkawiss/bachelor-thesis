


































•
•

 
i4Ds07: Badenfahrt 2017 - Interaktives Projection Mapping
 

 
Ausgangslage 
Alle 5 Jahre findet in Baden ein grosses Fest statt, und kommendes Jahr vom 17. - 26. August ist es wieder
soweit mit der „grossen Badenfahrt" (http://www.badenfahrt.ch). Während dieser 10 Tage wird das Fest je-
weils von ca. 1 Million Personen besucht und ist weit über Stadt Baden hinaus bekannt. In Zusammenarbeit
mit der Leitung der Badenfahrt wird das Institut für 4D-Technologien eine interaktive Installation realisieren.
Voraussichtlich wird das Objekt die Limmathochbrücke sein, deren Bogen mit Projektionen beleuchtet wird.
Diese Projektionen können von den Besuchern kontrolliert werden. 
 
Ziel der Arbeit 
Hauptziel der Arbeit ist der Entwurf und die Umsetzung eines Systems, welches mittels einer Game Engine
(Unity) interaktives Projection Mapping mit mehreren Projektoren erlaubt. Weiter sollen die Inhalte via exter-
ne User Inputs gesteuert werden können. Da das System in einem Real-World Szenario eingesetzt wird, soll
es robust sein. 
 
Problemstellung 
„Projection Mapping" ist die gängige Methode, um mittels Projektoren auf physische 3D Objekte zu projizie-
ren. Dabei müssen verschiedene Faktoren wie Kalibration, Alignment, Lichtstärke berücksichtigt werden,
und in einem ersten Schritt soll die Arbeit die theoretischen Grundlagen zu Projection Mapping studieren.
Weiter gilt es zu studieren, wie die Technik mit Unity umgesetzt werden kann (unter Einbezug existierender
Ansätze), und wie Inhalte mittels Sensoren gesteuert werden können. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

Projection Mapping; Interaktive Installationen; Media Art; Computer Graphics
Unity, C# 

 
Bemerkungen 
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Stefan Arisona Priorität 1 Priorität 2
Simon Schubiger Arbeitsumfang: P5 oder P6 ---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

Studiengang Informatik/i4Ds/Studierendenprojekte HS16

mailto:stefan.arisona@fhnw.ch
mailto:simon.schubiger@fhnw.ch
http://www.badenfahrt.ch

