


































 
i4Ds26: Computing groundwater bodies in 3D meshes
 

 
Ausgangslage 
Karst rock covers a fifth of Switzerland and karst
aquifers make up 18% of our drinking water sup-
ply. Understanding karst is vital for finding
groundwater resources, for implementing ground-
water protection zones, for preventing flooding or
hazards during underground construction
(tunnels, deep deposits, etc.), and more. KAR-
SYS is a 3D-based approach to model karst sy-
stems developed by ISSKA. KARSYS models
enable us to make informed decisions, for exam-
ple to find the optimal location for digging a
borehole (see illustration) or to decide whether an area is prone to flooding. To ease the application of KAR-
SYS, the FHNW and ISSKA are currently building the Visual KARSYS platform. This platform will simplify the
creation of karst models, thus helping geology institutes worldwide to understand karst environments. 
 
Ziel der Arbeit 
Creating a groundwater model is one mandatory step of applying the KARSYS approach. This model is
currently created using a manual process. The aim is to devise and implement an algorithm to compute the
groundwater model automatically without any human interaction. 
  
The input will be the geological 3D model and the location of the springs in the area. The geological 3D mo-
del determines the geometry of the groundwater reservoir, whereas the locations of the springs indicate the
minimal water level. 
  
The algorithm should output the groundwater 3D model with the computed groundwater bodies - i.e. where
the rock is full of water. The algorithms should be fast enough to be used in a semi-interactive setting. Exam-
ple input 3D geological models and spring data will be provided. 
 
Problemstellung 
In a first step, the groundwater body shall be determined for simple geological models, i.e. the groundwater
is not confined and bounded by a single impervious geological formation. In a second step, the algorithm
shall be extended to advanced geological models. Advanced models may contain confined groundwater bo-
dies and the groundwater boundary may be determined by multiple distinct geological formations. 
  
Achieving good performance for large models could pose a challenge. A basic benchmark should be carried
out measuring algorithm speed. Optionally, a parallelized version of the algorithm may be implemented. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 
Scala (or Java) 
3D Algorithms 
 
Bemerkungen 
Final report would be preferred to be in English, but German is also possible.
  
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Manfred Vogel Priorität 1 Priorität 2
Philipp Hausmann Arbeitsumfang: P6 (360h pro

Student)
P5 (180h pro
Student)

Auftraggeber: ISSKA Teamgrösse: 2er Team 2er Team
Sprachen: Deutsch oder Englisch

Studiengang Informatik/i4Ds/Studierendenprojekte 17HS

mailto:manfred.vogel@fhnw.ch
mailto:philipp.hausmann@fhnw.ch

