


































•
•

 
IMVS23: aWall - Information View
 

 
Ausgangslage 
Das Projekt "Agile Collaboration Wall" (aWall) ist ein fortlau-
fendes Forschungsprojekt am IMVS, in welchem der Einsatz
von extra-grossen Multi-touch Displays zur Förderung der
Zusammenarbeit von co-located und verteilten Agilen Soft-
wareentwicklungs-Teams untersucht wird. Das Projekt wird
in Zusammenarbeit mit dem Institut für Kooperationsfor-
schung der Hochschule für angewandte Psychologie durch-
geführt. In Zentrum steht dabei die Förderung der Kollabora-
tion und Kommunikation von co-located und verteilten Teams
mittels neuer Visualisierungs- und Interaktionskonzepten auf
extra-grossen Multi-Touch Displays. Das System ergänzt be-
stehende Backend-Systeme wie Jira durch eine optimale Unterstützung der Team-Arbeit bei Agilen Team
Meetings wie das Daily Standup, das Sprint Planning, oder Retrospektiven. Die Agile Collaboration Wall soll
dabei sowohl als Workspace während den Team-Meetings wie auch als Information-Radiator für das Team
und andere Stakeholder dienen. Ein zentrales Konzept der Agile Wall ist die "Information View" im oberen
Bereich des Displays. Sie dient einerseits als Information-Radiator für alle Stakeholder und stellt das Tran-
sactional Memory des Teams dar, in dem sie alle kontextspezifischen Informationen repräsentiert. 
 
Ziel der Arbeit 
Im Rahmen dieses Projektes sollen einige der in der Information View geplanten Widgets entworfen und um-
gesetzt werden. Ein besondere Rolle spielt dabei das Team-Widget, in dem nicht nur das komplette Team
dargestellt werden soll (durch Avatars), sondern auch zusätzliche Informationen wie Verfügbarkeit, Ferienab-
wesenheiten, Anteil% am Projekt, etc. dargestellt und bearbeitet werden können soll. Bei der Auswahl der
umzusetzenden Widgets kann auf die Wünsche der Studierenden eingegangen werden. Das Bearbeiten der
Informationen eines Widgets soll dabei auch für verteilte Teams möglich sein. 
 
Problemstellung 
Die Information View soll die kontextspezifischen Informationen für jedes Meeting enthalten. Widgets sind
als "Floating" Elemente auf dem ganzen Display verschiebbar, skalierbar (mit entsprechend mehr Informatio-
nen) und editierbar. Typische Widgets sind z.B. Projekt-Uebersichten, Sprint-Uebersichten, Definiti-
on-of-Done, Burndown-Charts, Avatars, private Informationen (Fun-Widget), und Team-Informationen. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

JavaScript, interactJS, Python
JIRA, REST 

[1] M. Kropp, et al. Enhancing Agile Team Collaboration Through the Use of Large Digital Multi-touch Card-
walls. At XP 2017: Agile Processes in Software Engineering and Extreme Programming pp 119-134.
https://link.springer.com/chapter/10.1007/978-3-319-57633-6_8 
 
Bemerkungen 
Da es eine sehr grosse Anzahl umzusetzender Widgets gibt, sind auch mehrere Arbeiten möglich.
Bei Eignung kann die Arbeit zur Publikation an einer Konferenz eingereicht werden.
  
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Martin Kropp Priorität 1 Priorität 2
Arbeitsumfang: P6 (360h pro

Student)
P5 (180h pro
Student)

Teamgrösse: 1er oder 2er
Team

1er oder 2er
Team

Sprachen: Deutsch oder Englisch

Studiengang Informatik/IMVS/Studierendenprojekte 17HS

mailto:martin.kropp@fhnw.ch
https://link.springer.com/chapter/10.1007/978-3-319-57633-6_8

