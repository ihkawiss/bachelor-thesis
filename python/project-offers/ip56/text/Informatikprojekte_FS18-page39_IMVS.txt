


































 
IMVS11: Cookiecaster 2.0
 

 
Ausgangslage 
Das FHNW Maker Studio bietet in der Adventszeit Guetzli-
förmli-Kurse an. In diesen Kursen lernen Jung und Alt, wie
man ein Förmli am Computer konstruiert und dann am 3D-
Printer ausdruckt. 
Für das Zeichnen der Guetzli-Formen kommt aktuell die We-
bapplikation unter http://www.cookiecaster.com zum Einsatz. Mit
dieser Applikation kann man schnell einfache Formen kon-
struieren, eine 3D-View erzeugen und aus dem Formen das
STL-File für den 3D-Drucker generieren lassen. Die dabei
entstehenden Formen sind grundsätzlich brauchbar um Kek-
se auszustechen. Für komplexere Formen ist die Webappli-
kation jedoch nicht geeignet und auch der Editor ist nicht in-
tuitiv zu bedienen. 
 
Ziel der Arbeit 
Es ist eine Webapplikation in einem Mobile-First Ansatz zu entwickeln, um über einen Editor Guetzli-Formen
effizient und ohne grosse Einarbeitung zeichnen, in einer gemeinsamen Datenbank ablegen und schlus-
sendlich mit Hilfe eines 3D-Drucker ausdrucken zu können.  
Zentral ist die Client Applikation, die auf Basis eines entsprechenden UX-Konzeptes implementiert und über
Usability-Tests validiert wird. Die Guetzli-Formen müssen in ein 3D-Druck-Format exportiert werden können,
so dass man sie ausdrucken kann. 
 
Problemstellung 
Der Cookiecaster 2.0 soll smart sein. D.h. dass Logik eingebaut werden kann, um die Guetzli-Form auf ihre
Eignung für das Backen analysieren zu können. Die Formen sollen dahingehend geprüft werden, ob es
Areale gibt, welche das Backen erschweren (zu Spitze Ecken, zu schmale Durchgänge). 
Beim Gestalten der Formen (zusammenhängender Graph mit Knoten vom Grad 2) soll es möglich sein,
beim Freihandzeichnen Teile des Graphen zu kopieren/spiegeln/drehen. 
Alternativ soll auch ein Bild in eine Guetzli-Form konvertiert werden können (Nachzeichnen). Der automa-
tisch erstellte Graph soll dabei angezeigt werden und der Benutzer soll Kanten und Knoten lö-
schen/hinzufügen/verändern können. 
Es soll auch möglich sein, mehre Formen zu kombinieren (Beispiel Spitzbuben) sowie Konturen einzufügen,
damit „Einschnitte" gemacht werden können (wie z.B. ein Strich als Mund). 
Beim Export der Formen soll die Standfläche auf das Backen (Teig gut entfernbar) und Drucken optimiert
werden. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 
User Interface Design, Usability, Bilderkennung oder Graphen bzw. Geometrien 
SPA Applikation mit React und Mobile-First Ansatz, RESTful Backend in Node/Java 
 
Bemerkung 
Diese Arbeit ist für ein gemischtes Studierendenteam aus iCompetence (Usability) und iModular (Technik)
geeignet.
 
 
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Jürg Luthiger Priorität 1 Priorität 2
Barbara Scheuner Arbeitsumfang: P6 (360h pro

Student)
---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

 Computer Science/IMVS/Student projects 18FS

mailto:juerg.luthiger@fhnw.ch
mailto:barbara.scheuner@fhnw.ch
http://www.cookiecaster.com

