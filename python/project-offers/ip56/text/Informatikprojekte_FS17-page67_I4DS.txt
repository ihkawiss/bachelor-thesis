


































 
i4Ds06: Google Street View als Zugang zu einer Bilddatenbank
 

 
Ausgangslage 
Das SII der HTW Chur ist damit beauftragt zwei Bilddatenbanken eines Museums zu reorganisieren. Die Da-
tenbanken enthalten Bilder historischer Gebäude, die zu einem grossen Teil heute nicht mehr stehen. Aktu-
ell sind die Datenbanken beinahe nur für ExpertInnen wirklich nützlich. Es fehlt ein Zugang, der auch einem
breiteren Publikum einen Einstieg in diese Welt der Bilder erlauben würde. Mit dem neuen Zugang soll es
auch möglich werden, dass die Nutzenden Zusammenhänge erkennen können, indem sie z.B. auf regionale
Unterschiede in der Architektur o.Ä. aufmerksam werden. Auch sollen die Nutzenden sehen können, wie die
Gegend, in der das abgebildete Gebäude stand, heute aussieht. In dieser Arbeit soll eine Applikation reali-
siert werden, die es erlaubt über Google Street View einen Zugang zu den Bildern der Bilddatenbank zu
schaffen. Die Applikation erlaubt so eine Art Zeitreise. 
 
Ziel der Arbeit 
Die Benutzerinnen und Benutzer der Datenbank sollen die Möglichkeit erhalten, in einem bestimmten Ort mit
Street View durch die Strassen zu navigieren. Enthält die Datenbank Bilder einer bestimmten Adresse, kön-
nen diese eingeblendet werden, so dass neben den aktuellen Gebäuden auch die ehemaligen betrachtet
werden können. Von vielen Gebäuden sind mehrere Bilder in der Datenbank enthalten. Es soll nach Mög-
lichkeiten gesucht werden, wie diese unterschiedlichen Perspektiven auf das Gebäude positioniert und dar-
gestellt werden können. 
 
Problemstellung 
Die Arbeit soll sich mit dem Problem befassen, wie externe Bilder in Google Street View Szenen integriert
werden können, und wie durch die entstehenden Szenen navigiert und mit ihnen interagiert werden kann. Im
weiteren sollen geeignete Darstellungsformen gefunden werden, und Editiermöglichkeiten implementiert
werden. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 
Visualisierung, App-Entwicklung, HCI, UX. 
 
Bemerkungen 
Dieses Projekt ist für Elena Mastrandrea und Bettina Burri reserviert.
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Stefan Arisona Priorität 1 Priorität 2
Auftraggeber: Schweiz. Institut für

Informationswissenschaft (SII) der
HTW Chur

Arbeitsumfang: P6 (360h pro
Student)

---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

Studiengang Informatik/i4Ds/Studierendenprojekte HS16

mailto:stefan.arisona@fhnw.ch

