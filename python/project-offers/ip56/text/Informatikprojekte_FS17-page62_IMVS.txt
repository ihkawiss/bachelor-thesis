


































•
•
•
•
•
•

•

•
•
•

 
IMVS36: FormsFX
 

 
Ausgangslage 
Fast alle Geschäftsanwendungen setzen mehr oder weniger komplexe Formulare ein, um Daten zu erfas-
sen.  
  
Typische Beispiele sind die Eingabe von Kundenadressen oder Kreditkarteninformationen. JavaFX bietet je-
doch nur Unterstützung für einzelne Komponenten und nicht für ganze Formulare, d.h. diese müssen mo-
mentan mühsam zusammengebaut und mit Methoden zur Validierung und Navigation ergänzt werden. 
 
Ziel der Arbeit 
Implementierung eines leistungsfähigen Frameworks für JavaFX zur einfachen Erstellung von komplexen
Formularen und die Möglichkeit Formulare mit Hilfe von XML oder einer DSL (Domain Specific Language) zu
definieren. 
 
Problemstellung 
Grundlegende Funktionalität: 

Mapping von Properties zu Formular Feldern
Darstellung von Feldern mit Hilfe der in JavaFX vorhandenen Komponenten
Gruppierung von mehreren Komponenten zu Formularblöcken
Gruppierung von mehreren Formularblöcken zu einem Formular
Hinterlegung einer Validierungsregeln pro Feld
Hinterlegung von Navigationsregeln (TAB, SHIFT TAB) innerhalb des Formulars (Feld zu Feld, Block zu
Block)
Erstellung von Formularen mit Hilfe einer externen Beschreibung (XML, DSL, TXT) 

 
Technologien/Fachliche Schwerpunkte/Referenzen 

JavaFX
XML
DSL 

Betreuer: Dieter Holz Priorität 1 Priorität 2
Auftraggeber: DLSC Arbeitsumfang: P6 (360h pro

Student)
P5 (180h pro
Student)

Teamgrösse: 2er Team 2er Team
Sprachen: Deutsch oder Englisch

Studiengang Informatik/IMVS/Studierendenprojekte HS16

mailto:dieter.holz@fhnw.ch

