


































•
•
•
•

 
IIT06: Unity Mixed Reality Framework
 

 
Ausgangslage 
The game engine Unity offers simple
access into the world of virtual reality.
Virtual scenes can easily be created
and inspected with a head mounted
display. The i4Ds created for this pur-
pose a virtual reality environment,
using the HTC Vive, within the multi-
media lab.  
In a previous internal project, a small
green screen was built to investigate
existing tools and workflows within the
area of mixed reality. 
 
Ziel der Arbeit 
The goal of this project is to build a mi-
xed reality framework within Unity,
which combines the green screen with the existing HTC Vive setup. The framework should be an easy to
use, lightweight piece of software which allows the whole mixed reality setup in Unity. A deployment into the
Unity store is preferable, assumed that the framework is flexible enough to support a variety of off-the-shelf
hardware. 
 
Problemstellung 
The calibration of a mixed reality setup depends on multiple factors like the field of view of the used camera
and the distance to the greenscreen. Another problem is the handling of the several shades of green colour
through imperfect illumination. Tasks are therefore to create an easy tool which helps calibrating the mixed
reality system, find an efficient way to replace the greenscreen surface with the ingame generated
"background" picture and mix in the real "foreground" image which comes from a video camera. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

Unity
C#
Image processing
Computer graphics 

 
Bemerkung 
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Simon Schubiger Priorität 1 Priorität 2
Filip Schramka Arbeitsumfang: P5 (180h pro

Student)
---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

 Computer Science/IIT/Student projects 18FS

mailto:simon.schubiger@fhnw.ch
mailto:filip.schramka@fhnw.ch

