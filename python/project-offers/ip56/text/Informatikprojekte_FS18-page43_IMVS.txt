


































•

•
•
•
•

•
•

•
•
•
•
•

 
IMVS39: MakerStudio more alive
 

 
Ausgangslage 
Das MakerStudio ist eine Einrichtung für Maker an der FHNW. Unter anderem befinden sich im MakerStudio
eine variable Anzahl 3D-Drucker (sechs oder mehr). Die Drucker sind allesamt der Marke Ultimaker
(Ultimaker 2, 2+ und 3 Extended).  
Das MakerStudio soll nun seine Sichtbarkeit erhöhen und gleichzeitig seine internen Abläufe straffen. Zu
diesem Zweck soll es neu möglich sein, Drucker aus der Ferne zu starten und deren Status zu prüfen. 
 
Ziel der Arbeit 
Folgende Funktionen sollen durch die Arbeit abgedeckt werden können: 

Anzeige eines Live-Streams geeignet für den Kioskmodus eines Browsers (im Wechsel: Statische
Bild/HTML-Seiten und Ansicht des Druckraumes von laufenden Druckern mit Teilevorschau; Live-Stream
und Vorschau wird durch die API von Octoprint geliefert).
Druckaufträge für 3D-Drucker absetzbar und kontrollierbar (inklusive Queue).
Reservation der Drucker für einen Bestimmten Zeitbereich.
Übersichtsseite aller Livestreams.
History von allen gedruckten Teilen (auch 3D Ansichten davon) sowie allen Druckaufträgen.- zusätzliche
Komponenten (Raspberrys) im 3D Drucker ein- oder angebaut.
(optional) Selbst konfigurierbare Ansichten bestehend aus Lifestreams, 3D Teile previews, Bildern etc.
(optional) Analyse der Druckaufträge mit grafischen Anzeigen (verbrauchtes Material pro Stunde, durch-
schnittliche Länge Druckaufträge etc.). 

 
Problemstellung 
Folgende Dinge sind bei der Umsetzung zu berücksichtigen: 

Das System soll bereits ab Juni im Einsatz sein, um Fehler frühzeitig zu finden (Ziel ist 7x24).
Drucker, die extern an FHNW-Fremde Netzwerke angeschlossen werden, müssen funktionieren.
Das System soll vollständig über eine Website bedienbar sein.
Das System ist zu Dokumentieren.
Alle derzeitigen Drucker müssen zum Ende der Arbeit vollständig ausgerüstet sein. 

 
Technologien/Fachliche Schwerpunkte/Referenzen 
- Auf Basis von Raspberry und Octoprint (http://octoprint.org/) 
- Use cases müssen zusammen mit Labmanagern erarbeitet werden 
 
Bemerkung 
Es existiert bereits eine Arbeit "MakerStudio Live". Diese Arbeit hat wichtige Teile der oben aufgeführten
Aufgaben im Prinzip schon gelöst (Es existieren umgerüstete Drucker mit Raspi und vorinstalliertem Octo-
print sowie einem Livestream, der von einem existierenden, zentralen Webserver "gestreamt" und mit Bil-
dern zu einer Slideshow angereichert werden kann).
 
 
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Martin Gwerder Priorität 1 Priorität 2
Arbeitsumfang: P6 (360h pro

Student)
---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

 Computer Science/IMVS/Student projects 18FS

mailto:martin.gwerder@fhnw.ch
http://octoprint.org/

