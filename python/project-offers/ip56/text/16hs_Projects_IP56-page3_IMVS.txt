


































•
•
•

•
•
•
•

 
IMVS02: Fahrplanomat
 

 
Ausgangslage 
Bots are the new apps! Diese Aussage über Chat-Bots
machte Microsoft CEO Satya Nadella in seiner Keynote
an der diesjährigen Microsoft Developer-Konferenz. Chat-
Bots sind Programme, welche automatisch mit Nutzern
kommunizieren und Fragen beantworten bzw. den Nutzer
mit Informationen versorgen. Damit wird ein Messenger
zur Schnittstelle für alle möglichen Interaktionen im Web
wie „wie wird das Wetter morgen?", „wer spielt heute
Fussball?" etc. 
  
Die Firma gotomo GmbH, welche sich auf Mobile Marke-
ting spezialisiert hat, möchte mit Hilfe von Bots kontext-
sensitive Werbebotschaften platzieren können. 
 
Ziel der Arbeit 
Im Rahmen dieses Projektes soll ein Bot entwickelt werden welcher Fahrplananfragen oder evtl. auch
Wetteranfragen beantworten kann. Der Bot kann entweder auf Basis des Facebook Messengers oder als
Standalone App umgesetzt werden.  
  
Der Bot begrüsst den User mit der Bitte, im Chat Reiseziel und gewünschte Ankunftszeit anzugeben. Auf
diese Angaben antwortet der Bot mit den entsprechenden Reiseinformationen. 
Der Bot gibt im Sinne von Empfehlungen zusätzlich kontextbezogene Informationen, die vom User zwar
nicht primär angefordert wurden, aber im Zusammenhang mit seiner geplanten Reise (Uhrzeit, Wetter,
Reisedauer etc.) nützlich sein können. Die angebotenen Zusatzinformationen sind gesponserte Werbebei-
träge. 

Hinweis auf Brezelkönig falls der Nutzer in einem Bahnhof umsteigen muss
Hinweis auf Nachtzuschlag falls dieser bei der entsprechenden Verbindung anfällt
Hinweis auf Wetter falls ein Wetterumschwung absehbar ist 

 
Problemstellung 
Für die Entwicklung innerhalb Facebook kann das Facebook Messanger SDK verwendet werden. Eine
Umsetzung als native App (standalone) ist auch möglich, setzt aber eine Recherche bezüglich vorhande-
ner Bibliotheken voraus. Die Antworten der Nutzer müssen auf Schlüsselworte analysiert werden und die
Fahrplan- bzw. Wetterdaten können über Open-Data Schnittstellen abgefragt werden. Die Produktvor-
schläge und Werbehinweise werden über eine zu definierende Schnittstelle abgerufen. 
Als Programmierumgebung verwendet Facebook in seinen Tutorials Node.js, aber man kann mit beliebi-
gen Sprachen/Umgebungen mit dem Facebook-API kommunizieren (man muss einfach einen WebHook
bereitstellen). 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

Beispiel ChatBot: Poncho http://poncho.is/
Liste von Bots: https://botlist.co/
Useful Links: https://stanfy.com/blog/the-rise-of-chat-bots-useful-links-articles-libraries-and-platforms/
Facebook Plattform: https://messengerplatform.fb.com/ 

Betreuer: Dominik Gruntz Priorität 1 Priorität 2
Daniel Kröni Arbeitsumfang: P5 (180h pro

Student)
---

Auftraggeber: gotomo GmbH Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

Studiengang Informatik/IMVS/Studierendenprojekte HS16

mailto:dominik.gruntz@fhnw.ch
mailto:daniel.kroeni@fhnw.ch
http://poncho.is/
https://botlist.co/
https://stanfy.com/blog/the-rise-of-chat-bots-useful-links-articles-libraries-and-platforms/
https://messengerplatform.fb.com/

