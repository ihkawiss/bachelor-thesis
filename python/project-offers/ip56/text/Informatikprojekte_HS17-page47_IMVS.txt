


































•
•

 
IMVS22: aWall - Distributed Sprint Planning
 

 
Ausgangslage 
Das Projekt "Agile Collaboration Wall" (aWall) ist ein fortlau-
fendes Forschungsprojekt am IMVS, in welchem der Einsatz
von extra-grossen Multi-touch Displays zur Förderung der
Zusammenarbeit von co-located und verteilten Agilen Soft-
wareentwicklungs-Teams untersucht wird. Das Projekt wird
in Zusammenarbeit mit dem Institut für Kooperationsfor-
schung der Hochschule für angewandte Psychologie durch-
geführt. In Zentrum steht dabei die Förderung der Kollabora-
tion und Kommunikation von co-located und verteilten Teams
mittels neuer Visualisierungs- und Interaktionskonzepten auf
extra-grossen Multi-Touch Displays. Das System ergänzt be-
stehende Backend-Systeme wie Jira durch eine optimale Unterstützung der Team-Arbeit bei Agilen Team
Meetings wie das Daily Standup, das Sprint Planning, oder Retrospektiven. Die Agile Collaboration Wall soll
dabei sowohl als Workspace während den Team-Meetings wie auch als Information-Radiator für das Team
und andere Stakeholder dienen. 
 
Ziel der Arbeit 
Im Rahmen des Vorgängerprojektes SI-ATAM wurden erste Konzpete für die Durchführung von Sprint Plan-
ning Meetings erarbeitet z.B. in [1] publiziert. Im Rahmen dieses Projektes sollen die bestehenden Konzepte
für die Durchführung der Sprint Planning Meetings (SP1 und SP2) für die verteilte Durchführung erweitert
und umgesetzt werden. Ziel ist es, den aktuellen Prototyp um die notwendigen Funktionen zu erweitern, so
dass er bei einem Pilot-Kunden zum Einsatz kommen kann. 
 
Problemstellung 
Für die Anwendung gelten vor allem bzgl. Interaktion und der Kommunikationsförderung im Team besondere
Anforderungen: 
Gerade die Sprint Planning Meetings zeichnen sich durch eine hohe Interaktion zwischen den beteiligten
aus.  
Informationen müssen schnell auffindbar und darstellbar sein. Informationen sollen beliebig skalierbar darge-
stellt werden können mit entsprechendem Abstraktionskonzept, um sie dem ganzen Team gut lesbar prä-
sentieren zu können.  
Es sollen kontextabhängig immer alle relevanten Informationen sichtbar und direkt zugreifbar sein. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

JavaScript, interactJS, Python
JIRA, REST 

[1] M. Kropp, et al. Enhancing Agile Team Collaboration Through the Use of Large Digital Multi-touch Card-
walls. At XP 2017: Agile Processes in Software Engineering and Extreme Programming pp 119-134.
https://link.springer.com/chapter/10.1007/978-3-319-57633-6_8 
 
Bemerkungen 
Bei Eignung kann die Arbeit zur Publikation an einer Konferenz eingereicht werden.
  
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Martin Kropp Priorität 1 Priorität 2
Arbeitsumfang: P6 (360h pro

Student)
P5 (180h pro
Student)

Teamgrösse: 1er oder 2er
Team

2er Team

Sprachen: Deutsch oder Englisch

Studiengang Informatik/IMVS/Studierendenprojekte 17HS

mailto:martin.kropp@fhnw.ch
https://link.springer.com/chapter/10.1007/978-3-319-57633-6_8

