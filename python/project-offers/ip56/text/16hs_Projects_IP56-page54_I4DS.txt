


































•

•
•
•
•

•
•
•

 
i4Ds31: Generierung von Logikrätseln
 

 
Ausgangslage 
Die Rätsel Agentur AG produziert verschiedenste Wort- und Lo-
gik-Rätsel für Verlage und Zeitungen wie 20 Minuten oder Blick am
Abend. Im Rahmen eines KTI-Projektes entwickelt das Institut für 4D
Technologien eine neue, webbasierte Software für die Generierung
solcher Rätsel. Um die Produktion von verschiedenen Rätsel-Typen zu
unterstützen, soll ein Framework entwickelt werden, welches sich spä-
ter kontinuierlich um neue Rätsel-Typen erweitern lässt. 
 
Ziel der Arbeit 
Logik-Rätsel wie Sudoku, Bimaru, Binoxxo oder Nonogramme können
von einem Computer in der Regel auf eine vergleichbare Art gelöst
werden. Das Ziel dieser Arbeit ist die Entwicklung eines Frameworks,
mit welchem die wichtigsten Arten von Logik-Rätsel generiert werden können. Diese Rätsel sollen dann in
Zeitungen, in Rätsel-Heften oder im Internet publiziert werden können. Das geplante Framework soll mo-
dular aufgebaut sein, damit sich zukünftig neuen Rätsel-Typen möglichst einfach integrieren lassen. Wich-
tig ist auch, dass die Schwierigkeit der generierten Rätsel algorithmisch bestimmt werden kann, damit die-
se entsprechend gefiltert werden können.  
Als Minimalanforderung soll das zu entwickelnde Framework 3 verschiedene Arten von Logik-Rätsel gene-
rieren können: Nonogramme, ein selbst erfundenes Rätsel und ein beliebiges, bekanntes Logik-Rätsel. 
 
Problemstellung 

Simple, webbasierte Oberfläche zur Auswahl des Rätseltyps, der Schwierigkeit und allenfalls weiterer
Parameter
Einfach erweiterbares Framework für das Generieren von Logik-Rätsel
Generierung von mindestens 3 verschiedene Rätsel-Typen 
Darstellung des generierten Rätsels auf einer Webseite 
Erfinden eines neuen Logik-Rätsels 

  
Wird die Arbeit als Bachelor-Thesis gewählt, werden die Ansprüche an das Framework entsprechend an-
gepasst (z.B. Export-Funktionalitäten, zusätzliche Rätsel-Typen wie Zahlen- und Wort-Rätsel, etc.). 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

Java
Effiziente Algorithmen
Webtechnologien 

 
Bemerkungen 
Gute Programmierkenntnisse, Freude an algorithmischen Problemen 

Betreuer: Manfred Vogel Priorität 1 Priorität 2
Lucas Brönnimann Arbeitsumfang: P5 (180h pro

Student)
P6 (360h pro
Student)

Auftraggeber: Rätsel Agentur AG Teamgrösse: 2er Team 2er Team
Sprachen: Deutsch

Studiengang Informatik/i4Ds/Studierendenprojekte HS16

mailto:manfred.vogel@fhnw.ch
mailto:lucas.broennimann@fhnw.ch

