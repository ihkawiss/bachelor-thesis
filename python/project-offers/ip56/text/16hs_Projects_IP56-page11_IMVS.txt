


































•
•

•

 
IMVS07: Konfigurierbare Schnittstelle zwischen App und Akto-
ren/Sensoren
 

 
Ausgangslage 
Vermehrt werden Projekte an der HT bearbeitet, die sich der Mög-
lichkeiten des Internet of Things (IoT) bedienen: 
In der Projektschiene etwa wurden Kugelbahnen realisiert, auf deren
Kugeln via Handy Einfluss genommen werden kann. Es können Wei-
chen gestellt werden etc. 
In der Einführungswoche auf dem Bienenberg ist ein Parcours IoT-
unterstützt zu absolvieren. Auch hier werden Handys eingesetzt. 
In diesen Fällen spielt die Kommunikation Mensch-Maschine eine
wichtige Rolle. So sind Aktoren anzusprechen und Sensoren auszu-
lesen. 
Da bei einem Projektbeginn unter Umständen das genaue Zusam-
menspiel und die Bedienung noch nicht voll festgelegt werden kön-
nen, aber dennoch rasch Erfahrungen in Bedienung und IoT-seitig
gesammelt werden sollen, wäre eine rasch konfigurierbare App für die Menüführung und das Einbinden
von Aktoren und Sensoren hilfreich. Genau dies ist Ausgangslage für dieses Projekt. 
 
Ziel der Arbeit 
Für Handhelds soll eine Applikation entwickelt werden, die es erlaubt, mit einem Assistenten schnell und
einfach eine Verbindung zu Sensoren/Aktoren zu konfigurieren, ohne codieren zu müssen. 
Im Kommunikationsmodus soll es möglich sein, sich mit ausgewählten Aktoren und Sensoren zu verbin-
den, um eine Beschreibung anzufordern, welche eine entsprechende Kommunikationsoberfläche inklusive
hinterlegter Funktionalität realisiert. 
Im Live-Modus kann der User mit verschiedenen Aktoren/Sensoren menügeführt kommunizieren, sich z.B.
Messwerte anzeigen lassen, Relais ein- und ausschalten usw. 
Die Aufgabenstellung ist offen gehalten: Mit einer Konzeptentwicklung im Austausch mit der Betreuung
soll ein Pflichtenheft entwickelt werden, welches dann umgesetzt wird. 
 
Problemstellung 
Konzipierung und Entwicklung einer Applikation, die  

ohne Programmierung konfiguriert werden kann,
mit Sensoren/Aktoren Kontakt aufnehmen kann, welche selbst wissen, wie sie anzusprechen sind und
dies der Applikation mitteilen,
exemplarisch einige Aktoren anspricht und Sensoren ausliest. 

 
Technologien/Fachliche Schwerpunkte/Referenzen 
Die Kommunikationstechnologie ist nicht vorgegeben, sondern muss den Betreuern vorgeschlagen wer-
den. 
 
Bemerkungen 
Einsatzgebiet: Rapid Prototyping, User-Führung bezüglich Bedienung (HCI), IoT 
Konkrete Beispiele: Kugelbahn, Parcours Bienenberg, Bobbahn, Roboterspiel "Achtung Technik Los", Be-
dienung von Museumsexponaten 

Betreuer: Ruedi Müller Priorität 1 Priorität 2
Thekla Müller Arbeitsumfang: P5 (180h pro

Student)
---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

Studiengang Informatik/IMVS/Studierendenprojekte HS16

mailto:ruedi.mueller@fhnw.ch
mailto:thekla.mueller@fhnw.ch

