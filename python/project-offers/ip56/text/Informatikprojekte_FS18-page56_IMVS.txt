


































 
IMVS69: Mining Software Repositories
 

 
Ausgangslage 
Ein wichtiger Punkt bei der erfolgreichen Entwicklung und
Wartung von Software ist die Vermeidung von Technical
Debt. Besonders bei Legacy Code Systemen ist Technical
Debt aber oftmals anzutreffen. Dabei stellt sich für die Ent-
wickler die Frage, wo die wirklich problematischen Module
oder Komponenten sind, die das Team an der Weiterent-
wicklung behindern und so die Produktivität einschränken.  
Bestehende Static Code Analysis Tools können Technical
Debt anhand verschiedenster Metriken berechnen, aber ge-
rade bei Legacy Systemen ist die Anzahl gefundender Probleme schnell überwältigend und so nicht zielfüh-
rend. Hier hilft die sog. Hotspot Analyse (bekannt aus dem Buch "Your code as a crime scene"). Hotspots
sind Codestellen, die sich durch eine hohe Komplexität auszeichnen, und gleichzeitig auch oft geändert wer-
den, weshalb sie wirklich problematisch sind. Hotspots helfen uns dabei zu verstehen, welche Teile des Co-
des dazu tendieren, komplex und unübersichtlich zu werden. Eine weitere Problematik in Software Projek-
ten, die bei geplanten Refactorings grosse Priorität haben sollte, sind temporale Kopplungen. Solche Kopp-
lungen zeichnen sich dadurch aus, dass diese Komponenten oft zusammen geändert werden (bspw. im glei-
chen Commit) und so versteckte (implizite) Abhängigkeiten bilden können 
 
Ziel der Arbeit 
In dieser Arbeit geht es darum, eine Implementierung zum Finden von Hotspots und temporalen Abhängig-
keiten zu entwickeln. Für die Anzeige von gefundenen Hotspots und temporalen Abhängigkeiten sind geeig-
nete Visualisierungen zu finden, so dass auch bei sehr grossen Software Projekten diese noch übersichtlich
dargestellt werden können. Als zusätzliche Erweiterung wären noch tiefergreifende Code Analysen denkbar,
so z.B. die Frage, ob Hotspots temporale Abhängigkeiten zu anderen Modulen anderer Komponenten (und
Teams) haben und so spezielle Kosten im Bereich Koordination und Kommunikation verursachen. Ausser-
dem könnten Entwicklern bei Code Reviews gewarnt werden, wenn Sie Anpassungen an einem Modul ma-
chen, aber nicht in dessen Abhängigkeiten (bspw., weil eine Änderung vergessen wurde). Zuguterletzt ist
auch der Einsatz von Machine Learning denkbar, um lohnenswerte Refactoring Ziele mit hohem Technial
Debt ausfindig zu machen und diesen Prozess auch kontinuierlich zu verbessern. 
 
Problemstellung 
Die Analyse von Git Repositories soll auch mit sehr grossen Software Projekten (>= 1 Mio. LOC) funktionie-
ren und eine akzeptable Performance bieten (Test bspw. mit Git repository des Linux Kernels). Die Lösung
muss skalierbar sein, damit parallel hunderte Entwickler die Analysen durchführen und die Visualisierungen
anschauen können. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 
Als zugundleliegendes VCS soll Git verwendet werden (bspw. für die Analyse der Commit History). Bevor-
zugt soll die Implementierung als Bitbucket Plugin in Java oder Scala erfolgen. Für die Visualisierungen wäre
D3.js bevorzugt. 
  
Das Buch zum Thema: https://pragprog.com/book/atcrime/your-code-as-a-crime-scene 
Gource Visualisierungstool http://gource.io/ 
Link zur Konferenzserie zum Thema http://2017.msrconf.org/#/hall-of-fame 
 
Bemerkung 
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Martin Kropp Priorität 1 Priorität 2
Auftragsgeber: Mibex Software GmbH Arbeitsumfang: P6 (360h pro

Student)
---

Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

 Computer Science/IMVS/Student projects 18FS

mailto:martin.kropp@fhnw.ch
https://pragprog.com/book/atcrime/your-code-as-a-crime-scene
http://gource.io/
http://2017.msrconf.org/#/hall-of-fame

