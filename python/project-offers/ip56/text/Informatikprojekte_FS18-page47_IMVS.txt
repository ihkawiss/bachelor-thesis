


































Das Logo des Lernstick-Projektes

•
•
•
•

 
IMVS54: Lernstick Remote Desktop
 

 
Ausgangslage 
An der PH FHNW wird der Lernstick, eine mobile Lern- und
Prüfungsumgebung, entwickelt. Die Prüfungsumgebung
kann dabei in einer Vielzahl von verschiedenen Settings
(offline/online, closed book/open book, BYOD/schuleigene
Geräte, etc.) eingesetzt werden. 
  
Auch wenn Prüfungen mit der Lernstick-Prüfungsumgebung
durchgeführt werden, so kann auf eine Aufsicht nicht ganz
verzichtet werden. Um den Aufwand für die Prüfungsaufsicht
möglichst gering zu halten, soll in die Prüfungsumgebung ei-
ne Software integriert werden, die eine zentrale Überwa-
chung aller Bildschirme der Prüfungsgeräte erlaubt. 
 
Ziel der Arbeit 
Der Aufwand für eine Prüfungsaufsicht soll durch die Integra-
tion der geeignetsten Lösung verringert werden. Die Lösung
muss sowohl für Administratoren als auch für Anwender ein-
fach und zuverlässig anwendbar sein. Sie soll es erlauben auf den Monitor der Umgebung zuzugreifen.  
  
Um dies zu erreichen gibt es schon viele etablierte Lösungen. In eine ersten, kurzen Phase soll untersucht
werden, welche der verschiedenen vorhandenen Überwachungslösungen die geeignetste Variante für den
Lernstick darstellt.  
  
Die ausgewählte Lösung muss dann sowohl im Systemstart als auch im Willkommensprogramm des Lern-
sticks integriert werden. Angestrebt wird eine möglichst optimale Lösung, die aber aus Stan-
dard-Programmen zusammengesetzt sein soll. 
 
Problemstellung 
Es gibt viele verschiedene bereits vorhandene Lösungen, die möglicherweise direkt integriert werden kön-
nen. Neben den konventionellen Fernwartungsansätzen via VNC oder RDP, stehen auch komplette Applika-
tionen wie Epoptes am Start. Auch eine X11-basierte Konsole kann problemlos über das Netzwerk betrach-
tet werden. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

Java/JavaFX
Debian
X11/VNC/RDP
iTALC/Epoptes 

 
Bemerkung 
Die Lernstick-Umgebung selber ist sehr modular aufgebaut und der Buildprozess verfügt bereits über die
notwendigen Hooks um zusätzliche Applikationen zu installieren. Ausserdem ist das Projekt gut dokumen-
tiert und zusätzliches Knowhow ist beim Auftraggeber vorhanden.
 
 

Betreuer: Martin Gwerder Priorität 1 Priorität 2
Arbeitsumfang: P6 (360h pro

Student)
P5 (180h pro
Student)

Teamgrösse: 2er Team 2er Team
Sprachen: Deutsch oder Englisch

 Computer Science/IMVS/Student projects 18FS

mailto:martin.gwerder@fhnw.ch

