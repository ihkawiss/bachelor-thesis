


































•
•

 
i4Ds35: Job Annotations
 

 
Ausgangslage 
In Zusammenarbeit mit der Firma 4U Computing
entwickelt das Institut für 4D Technologien einen
Web-Crawler, um Stellen-Ausschreibungen im
deutschsprachigen Internet zu finden und zu ei-
nem Vakanzen-Feed zusammenzuführen. Dieser
Feed kann z.B. auf Job-Plattformen publiziert und
so den Stellensuchenden zugänglich gemacht
werden. Die Beschreibungen der Vakanzen be-
stehen hauptsächlich aus Fliesstext und variieren
in der Strukturierung von Quelle zu Quelle. Ein
noch fehlendes Element des Web-Crawler bildet
die eigentliche Extraktion und Strukturierung der Stellenausschreibungen. 
 
Ziel der Arbeit 
Das Ziel dieser Arbeit ist, vom Web-Crawler identifizierte Webseiten zu extrahieren und die Inhalte der
Job-Ausschreibungen zu annotieren. Dadurch sollen die ausgeschriebenen Jobs besser sortiert, gefiltert
und klassifiziert werden können um ein besseres Matching mit CV's zu erzielen, d.h. die gefundenen Jobs
möglichst geeigneten Kandidaten vorzuschlagen. Dazu sollen beispielsweise der Job-Titel, die Branche,
die benötigten Skills/Ausbildungen, die Firma und der Arbeitsort, etc. identifiziert werden, um so die Va-
kanzen einheitlich zu erfassen und im Feed bereitzustellen. Zusätzlich sollen Annotationen wie Job-Titel
oder Skills gruppiert und strukturiert werden, damit auch ähnliche oder übergeordnete Job-Titel und Skills
identifiziert werden können. 
 
Problemstellung 
Es soll eine Library entwickelt werden, welche zuverlässig Informationen einer Job-Ausschreibung identifi-
ziert und annotiert. In dieser Arbeit kann aus möglichen Ansätzen einer ausgewählt und verfolgt werden.
Mögliche Ansätze sind der (teilweise) automatische Aufbau einer Ontologie oder die Anwendung von Ma-
chine Learning Technologien zur Erzeugung eines Modells, welches die semantische Ähnlichkeit zwi-
schen Begriffen beurteilt,  
Der zu entwickelnde Job-Extraktor sollte beispielsweise erkennen, dass "Java-Entwickler" und "C#-
Entwickler" zwar nicht den gleichen Job bezeichnen, aber doch eine gewisse Ähnlichkeit haben, weil beide
vom Oberbegriff "Software-Entwickler" abstammen. Eine gut strukturierte Ontologie wäre in der Lage, sol-
che Relationen abzubilden und auch synonyme Begriffe einander zuzuordnen. Im Rahmen dieser Arbeit
könnte aber vorerst ein einfacherer Ansatz versucht werden, je nachdem, ob das Projekt als IP5 oder Ba-
chelor-Thesis gewählt wird. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

Natural Language Processing
Machine Learning 

 
Bemerkungen 
Gute Programmierkenntnisse, Freude an Text-Mining und/oder Machine Learning 

Betreuer: Manfred Vogel Priorität 1 Priorität 2
Ivo Nussbaumer Arbeitsumfang: P5 (180h pro

Student)
P6 (360h pro
Student)

Auftraggeber: 4U Computing Teamgrösse: 2er Team 2er Team
Sprachen: Deutsch

Studiengang Informatik/i4Ds/Studierendenprojekte HS16

mailto:manfred.vogel@fhnw.ch
mailto:ivo.nussbaumer@fhnw.ch

