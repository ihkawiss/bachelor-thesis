


































•
•

•

•
•

 
i4Ds17: Text-Mining auf Online-News
 

 
Ausgangslage 
Die Qualität der Schweizer Medien hat in den
letzten Jahren abgenommen. Insbesondere bei
Online-Medien ist diese Tendenz zu beobachten.
Mit dieser Thematik befassen sich verschiedene
Organisationen welche versuchen, die Qualität
durch Umfragen, Studien und Recherchen zu be-
stimmen. Zusätzliche Einsichten könnten durch
automatisierte, Software basierte Auswertungen
gewonnen werden. 
 
Ziel der Arbeit 
Das Ziel der Arbeit ist die Entwicklung eines
Tools, mit welchem folgende Informationen gewonnen werden können: 

Ähnlichkeit von Artikeln: Stammen die Artikel verschiedener News-Seiten von der gleichen Quelle ab?
Emotionalität von Artikeln: Wird durch die Wortwahl versucht Empörung/Sympathie zu erzeugen?
Wenn ja, gibt es diesbezüglich einen Trend über die Jahre?
Themenwahl: Über welche Themen wird häufig berichtet? Wie unterscheiden sich die verschiedenen
News-Seiten bei der Themenwahl und wie verändert sich diese über die Zeit? 

 
Problemstellung 
Als Basis für die Arbeit steht eine Dokumentensammlung von Artikeln bereit. Diese sollen mithilfe eines In-
formation Retrieval Systems ausgewertet werden können, beispielsweise mit der Berechnung von TF-IDF,
dem Cosinus- oder anderen Ähnlichkeitsmassen. Weiter soll eine (einfache) Sentiment Analyse entwickelt
werden. Weitere geeignete Methoden aus den Bereichen NLP (Natural Language Processing) und/oder
Machine Learning sollen getestet und umgesetzt werden. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 

Java oder Python
Information Retrieval, Text Mining 

 
Bemerkungen 
Gute Programmierkenntnisse, Freude an Text Mining 
Idealerweise sind die Vorlesungen igs und/oder ml besucht 

Betreuer: Manfred Vogel Priorität 1 Priorität 2
Samuel von Stachelski Arbeitsumfang: P5 (180h pro

Student)
---

Teamgrösse: 2er Team ---
Sprachen: Deutsch

Studiengang Informatik/i4Ds/Studierendenprojekte HS16

mailto:manfred.vogel@fhnw.ch
mailto:samuel.vonstachelski@fhnw.ch

