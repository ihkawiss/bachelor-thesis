


































•
•
•

 
I4DS04: Taubenabwehr-System
 

 
Ausgangslage 
Die Verschmutzung durch Vögel wird
im städtischen Bereich vielerorts zu-
nehmend zu einem kostspieligen Pro-
blem. Bisherige Systeme zur Vergrä-
mung der Vögel sind wenig wirkungs-
voll oder störend für die Menschen.
Tests mit Laserpointern zeigten, dass
Vögel das Licht eines Laserstrahls als
sehr unangenehm empfinden. In die-
sem Projekt soll deshalb ein System
entwickelt werden, welches Vögel mit
einem gepulsten Laserstrahl er-
schreckt, ohne sie zu gefährden oder
zu verletzen. 
 
Ziel der Arbeit 
Das Ziel dieser Arbeit ist die Entwick-
lung eines funktionierenden Taubenabwehr-Systems, welches die Vögel mittels Bilderkennung lokalisiert
und anschliessend durch Kreisen eines Laserstrahls (handelsübliches Effektgerät) in den jeweiligen Berei-
chen vertreibt. Damit sollen die Tiere verscheucht aber nicht verletzt werden. 
 
Problemstellung 
In einer früheren Studierendenarbeit würde bereits (mit Hilfe der Open Source Software OpenCV) ein Bilder-
kennungs-Algorithmus entwickelt, welcher Vögel in Videos lokalisiert. Nun geht es darum, den Lokalisie-
rungs-Algorithmus zu analysieren und zu verbessern (oder allenfalls durch einen komplett neuen Algorith-
mus zu ersetzen), damit das System auch bei veränderten Licht- und Wetterverhältnissen zuverlässig funk-
tioniert. Weiter sollen die Anforderungen an die Laser-Steuerung definiert werden. 
  
Das geplante System soll über ein GUI konfigurierbar sein, beispielsweise zum Festlegen des zu überwa-
chenden Bereichs.  
Möglich ist auch, in einem weiteren Schritt mit Hilfe eines Digital Analog Converter einen handelsüblichen
Showlaser anzusteuern. Dies kann von einer simplen Steuerung bis hin zu einer Selbstkalibrierung reichen. 
  
Wird das Projekt als IP6 gewählt, soll auch ein alternativer Lokalisierung-Algorithmus (z. B. YOLO, Redmon
et al., 2016, https://arxiv.org/abs/1506.02640) umgesetzt und evaluiert werden. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 
Freude an Bild- resp. Video-Erkennung, Algorithmen aber auch Schnittstellen zu Hardware 
  

Phython
Open CV, (allenfalls Convolutional Neural Networks)
Etherdream (Digital Analog Converter, https://ether-dream.com) 

Betreuer: Manfred Vogel Priorität 1 Priorität 2
Arbeitsumfang: P5 (180h pro

Student)
P6 (360h pro
Student)

Teamgrösse: 2er Team 2er Team
Sprachen: Deutsch oder Englisch

 Computer Science/I4DS/Student projects 18FS

mailto:manfred.vogel@fhnw.ch
https://arxiv.org/abs/1506.02640)
https://ether-dream.com

