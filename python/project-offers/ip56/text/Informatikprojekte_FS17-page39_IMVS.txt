


































•
•
•

 
IMVS06: Music Score Trainer
 

 
Ausgangslage 
Klavierspielen ist eine komplexe Sa-
che, die schnelles Erfassen von No-
tenbildern und zu einem gewissen
Grad intuitive Bewegungen der Finger
erfordert. Die bewusste Umsetzung
von Notenbild über Notennamen auf
die Tastatur ist letztlich zu langsam.
Solche intuitiven Prozesse können al-
leine mit gedruckten Noten am Klavier
nicht genügend gut trainiert werden. 
  
Es soll eine App für Tablets entstehen,
die als Lern- und Trainingswerkzeug
das intuitive und relative Umsetzen
von Noten auf die Klaviertastatur för-
dert, so dass der Einstieg in das No-
tenlesen bei vollkommenen Verzicht
auf die Benennung der Töne gelingt. 
 
Ziel der Arbeit 
Im Zentrum stehen dabei folgende Abläufe, die im Zuge des Projekts anhand von Prototypen und Untersu-
chungen mit Probanden verfeinert werden sollen: 
Das Tablet zeigt eine Klaviertastatur und Notenlinien. 

Spielen auf der Klaviertastatur erzeugt Notenbild und Klang.
Klingende Töne können auf der Tastatur gesucht und nachgespielt werden.
Gezeigte Notenfolgen werden klingend nachgespielt. 

Die App kontrolliert und gibt Feedback. 
  
Verschiedene "Schwierigkeitsstufen" z.B. mit ohne Vorzeichen sind vorstellbar. 
  
Die App soll nach Möglichkeit mit einer zweiten Trainingskomponente erweitert werden, die Tonlängen und
Rhythmus anhand von Pendelbewegungen vermittelt. Ein Pendel gibt einen Grundrhythmus vor. Mit dem
Finger kann dazu ein durch angezeigte Notenwerte vorgegebener Rhythmus auf dem Bildschirm geklopft
werden, wozu dann die App eine passende Melodie spielt. 
 
Problemstellung 
Es muss eine funktionierende, idealerweise über den App-Store verteilbare App für Android-Geräte (oder bei
Interesse auch Multiplattform-fähig) programmiert werden. Die oben beschriebene User Experience muss
konkretisiert, ausgestaltet und wenn möglich mit Hinblick auf den didaktischen Effekt hin untersucht werden. 
 
Technologien/Fachliche Schwerpunkte/Referenzen 
Android, Java 

Betreuer: Wolfgang Weck Priorität 1 Priorität 2
Auftraggeber: Wolfgang Clausnitzer, Klavierlehrer,

Scharans
Arbeitsumfang: P5 oder P6 P5 oder P6

Teamgrösse: 2er Team Einzelarbeit
Sprachen: Deutsch oder Englisch

Studiengang Informatik/IMVS/Studierendenprojekte HS16

mailto:wolfgang.weck@fhnw.ch

