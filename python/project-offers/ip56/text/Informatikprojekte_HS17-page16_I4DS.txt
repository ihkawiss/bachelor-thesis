


































Webrechner-Simulator

•
•
•
•
•

 
i4Ds17: Service-orientierter Assembler für Webrechner
 

 
Ausgangslage 
Im Studiengang Informatik werden die
Studierenden in die Grundlagen der
Mikrorechnersysteme eingeführt. Dies
geschieht anhand eines Webrech-
ner-Simulators. 
Momentan kommt für den Webrechner
ein in Java entwickelter Assembler
zum Einsatz, der zulässt, den Befehls-
satz des Rechners zu erweitern. 
Wünschenswert ist eine Neuentwick-
lung, die Erweiterungen noch flexibler
macht, mehr Befehlsarten abdeckt
u s w .  A n g e d a c h t  i s t  e i n  s e r -
vice-orientierter Assembler, der Syn-
tax-Checking, Texteinfärbung, Intelli-
sense und Hilfetext an den Editor lie-
fert, die der Editor dann darstellt. Da-
mit wären auch spätere Erweiterungen
an einem einzigen Ort - im Assembler - realisierbar. 
 
Ziel der Arbeit 
Mit den Erkenntnissen aus dem bestehenden Java-Assembler soll eine neue Architektur konzipiert und im-
plementiert werden. Fragen zu einem service-orientierten Ansatz sollen studiert und einer Lösung zugeführt
werden. Dabei wird Wert auf gute Konfigurierbarkeit gelegt (Befehlsarten, Intellisense etc.). Eine nahtlose
Einbindung in den bestehenden Websimulator, Abdeckung möglicher neuer Befehlsarten und informative
Fehlermeldungen sind zentral. 
 
Problemstellung 

Konzipierung einer neuen Assembler-Architektur (service-orientiert)
Festlegen von Services
Implementierung des Assemblers und Editors
Systematische Tests diverser Szenarien (zum Teil verhanden)
Integration in die bestehende Web-Applikation 

 
Technologien/Fachliche Schwerpunkte/Referenzen 
TypeScript, C# 
Service-orientientierte Assembler-Architektur 
Realisierung von Services: Neue Befehlsarten, Texteinfärbung, Intellisense 
 
Bemerkungen 
Ein bestehender Referenz-Assembler (reduziert) steht zur Verfügung.
webstebs mit Assembler: http://server1082.cs.technik.fhnw.ch/
  
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Ruedi Müller Priorität 1 Priorität 2
Arbeitsumfang: P5 (180h pro

Student)
P6 (360h pro
Student)

Teamgrösse: 2er Team 2er Team
Sprachen: Deutsch oder Englisch

Studiengang Informatik/i4Ds/Studierendenprojekte 17HS

mailto:ruedi.mueller@fhnw.ch

