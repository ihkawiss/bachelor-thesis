


































GROMOS simulates the behaviour of molecules by modelling the pairwise interacti-
ons of atoms in small time steps

•
•
•
•
•
•

•
•
•

 
i4Ds01: Optimization of Molecular Dynamics Simulations
 

 
Ausgangslage 
The software package GROMOS has
been developed by the University of
Groningen and ETH to perform Mole-
cular dynamics (MD) simulations. Mo-
lecular dynamics simulation has beco-
me a widely used technique in compu-
tational chemistry to study the behavi-
or of many-particle systems such as li-
quids, solutions and macromolecules.
MD simulations are a numerical soluti-
on to Newton's equations of motion.
Each atom is treated as a particle in
space with a mass and a partial char-
ge. The electrostatic and van der
Waals interactions between the particles are calculated at each time point to determine the force that is ac-
ting on the atoms. This is a computationally expensive step of quadratic complexity. 
  
To improve the performance, GROMOS uses an atom-based parallelisation scheme to calculate pairwise in-
teractions, i.e. each atom is assigned to a thread. But the scalability of this approach is limited and further
performance increases are sought. 
 
Ziel der Arbeit 
The goal of this work is to further optimize the calculation of the pairwise interactions within the MD software
package GROMOS. In this project you should identify and implement the most promising techniques: 

Better optimized data structures
Different parallelization strategies
Changing implementation details
Improve memory access locality
Use of SIMD vectorization (SSE, AVX and others)
Different algorithmic approaches 

 
Technologien/Fachliche Schwerpunkte/Referenzen 

Numerical algorithms in C++
Vectorization (SIMD, SSE, AVX, ...)
Parallelisation (OpenMP, MPI, OpenCL) 

 
Bemerkungen 
You should be interested in low-level code optimization and feel comfortable with C or C++. Attending or
having attended "EfAlg" is recommended. While GROMOS consists of 100k+ lines of code, your area of
work will be limited to a small part of the system.
  
Dieses Projekt muss in einem einzigen Semester durchgeführt werden. 

Betreuer: Manfred Vogel Priorität 1 Priorität 2
Simon Felix Arbeitsumfang: P6 (360h pro

Student)
---

Auftraggeber: ETH Zürich Teamgrösse: 2er Team ---
Sprachen: Deutsch oder Englisch

Studiengang Informatik/i4Ds/Studierendenprojekte 17HS

mailto:manfred.vogel@fhnw.ch
mailto:simon.felix@fhnw.ch

