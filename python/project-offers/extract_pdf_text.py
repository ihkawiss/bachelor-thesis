#-----------------------------------------------------------------------
#   Extract PDF contents to text files.
#
#   Project: IP6 FS18 FHNW
#   Author: Kevin Kirn <kevin.kirn@students.fhwn.ch>
#           Hoang Tran <hoang.tran@students.fhnw.ch>
#-----------------------------------------------------------------------

import re
import shutil
from os import listdir, remove, path
from os.path import isfile, join
from tika import parser

#mode = 'ip56'
mode = 'ip14'

# clear output folder
filelist = [ f for f in listdir(mode + '/text/')]
for f in filelist:
    remove(path.join(mode + '/text/', f))

# find all pdf files located in the folder ./pdf
pdf_files = [f for f in listdir(mode + '/pdf/') if isfile(join(mode + '/pdf/', f))]

# extract contents an write them into text files
for filename in pdf_files:
    text = parser.from_file(mode + '/pdf/' + filename)

    institute = ''

    # check if this file is a IMVS project
    regexp = re.compile(r"(?i)imvs[0-9]*:")
    if regexp.search(text['content']):
        institute = 'IMVS'

    # check if this file is a I4DS project
    regexp = re.compile(r"(?i)i4ds[0-9]*:")
    if regexp.search(text['content']):
        institute = 'I4DS'

    # check if this file is a IIT project
    regexp = re.compile(r"(?i)iit[0-9]*:")
    if regexp.search(text['content']):
        institute = 'IIT'

    if mode == 'ip14':
        institute = 'ip14'

    file = open(mode + '/text/' + filename.replace('.pdf', '_' + institute + '.txt').replace('.docx', '_' + institute + '.txt').replace('.doc', '_' + institute + '.txt') + '', "w")
    file.write(text['content'])
    file.close()