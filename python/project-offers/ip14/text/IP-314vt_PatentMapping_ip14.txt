

















































Semesterarbeit




	
	Studiengang Informatik      

	
	Bitte nur 1 A4-Seite                      , HS 14





Studiengang Informatik


SS    03

Studiengang Informatik


SS    03


Mai 2014		Markus Oehninger
Februar 2006		Manfred Vogel
Prof. Dr. Manfred Vogel / Prof. Dr. Manfred Breit

Titel	Patent-Mapping


Betreuung	noch zu definieren


Auftraggeber	Scambia Patent Services GmbH
	Thomas Sütterlin
                                     thomas.suetterlin@scambia.ch

  



Ausgangslage	Patente sind in öffentlichen Datenbanken gespeichert und immer häufiger werden solche Patentinformationen für die Analyse des technischen Marktumfelds beigezogen. Beispiele sind: Welche Firma ist in welchen Technologien tätig, z.B. Mechatronik, Robotik, was sind die technischen Trends, z.B. Robotersteuerung via Smartphone, worauf fokussieren die Wettbewerber, mit wem kann ich kooperieren. Es sind heute Tools erhältlich, die zwar eine solche Analyse anbieten, aber nur auf eine eigene Datenbank zugreifen, zudem sind sie wenig benutzerfreundlich und teuer.


Ziel der Aufgabe 	Benutzerfreundliche Nachbearbeitung von Abfrageinformationen und schnelle Visualisierung von Suchresultaten, sodass der heutige Zeitaufwand um 50% reduziert und die Datenqualität stark erhöht wird.

Problemstellung	Entwickeln eines Prototypen (Webapplikation) der aus 3 öffentlichen Datenbanken vordefinierte Felder extrahiert, in einer vorgegebenen Tabellenstruktur abspeichert und anzeigt. Die Applikation soll modular aufgebaut sein, z.B. Administration, Suchkonfigurator (mit der Möglichkeit Suchmuster abzuspeichern), Bearbeitung der Suchresultate, (z.B. bei schlechter Datenqualität). Dazu sind folgende Schritte notwendig:

· Analysieren der Exportschnittstellen von 3 Patentdatenbanken
· Evaluation und Weiterentwicklung der Visualisierungsgrafiken, z.B. Heat map
· Entwickeln des mehrstufigen Suchkonfigurators, z.B. Wahl des Patentgebietes
· Entwickeln der Datennachbearbeitung, z.B. Simns AG und Siemens AG finden, richtig umwandeln und speichern
· Entwickeln des Mappers für die Zusammenführung gleicher Feld-inhalte
· Dokumentation für die Weiterentwicklung durch Dritte

· Option: Wichtigste Funktionen, z.B. Abfrage mit Suchmuster, anpassen für mobile Clients


Studiengang Informatik


WS    04/05

	

	Studiengang Informatik      

	
	WS 06/07



Studiengang Informatik


SS    03


Juni 2004		Prof. Dr. Manfred Vogel
Prof. Dr. Manfred Vogel / Prof. Dr. Manfred Breit

Technologie 
Fachliche 
Schwerpunkte 

Java, MySQL, SOLR	http://lucene.apache.org/solr/
Ev. Statistik Software wie „R“ zur Datenverdichtung
Visualisierung: open-source Tools, HTML5
 

Studiengang Informatik


WS    04/05

	

	Studiengang Informatik      

	
	WS 06/07



Studiengang Informatik


SS    03


Juni 2004		Prof. Dr. Manfred Vogel
Prof. Dr. Manfred Vogel / Prof. Dr. Manfred Breit

Links 

 http://www.scambia.ch/de/
          
Projekttyp	IP34, 5-8 Studierende ca. 1’700 Personenstunden
