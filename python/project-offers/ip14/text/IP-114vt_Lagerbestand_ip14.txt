

















































Semesterarbeit




	
	Studiengang Informatik      

	
	Bitte nur 1 A4-Seite                      , HS 14





Studiengang Informatik


SS    03

Studiengang Informatik


SS    03


August 2014		Markus Oehninger
Februar 2006		Manfred Vogel
Prof. Dr. Manfred Vogel / Prof. Dr. Manfred Breit

Titel	Visualisierung Lagerbestand 




Betreuung	noch offen




Auftraggeber	FHNW
	Herr László Etesi 
		

  



Ausgangslage	Die grafische Darstellung von Lagerbeständen ist eine grosse Unterstützung bei Abklärung der Lieferbereitschaft von Lagerartikeln zu einem bestimmten Zeitpunkt, vor allem wenn dieser in der Zukunft liegt.  Die Lieferbereitschaft, ob ein Artikel geliefert werden kann, ist abhängig vom aktuellen Lagerbestand, den bestehenden Reservationen sowie den geplanten Lagerzugängen. Die neue Applikation dient als Prototyp für die Berechnung des Lagerbestandes zu einem bestimmten Zeitpunkt, für das Design der grafischen User Interfaces (GUI) sowie für die visuelle Unterstützung von Beschaffungsentscheiden zum Auffüllen des Lagerbestandes. Als Testdaten stehen ca. 5‘000 Artikel zur Verfügung. Die Applikation soll auf einem Laptop ablaufen.



Nutzen 	Minimierung des Aufwandes für  die Handhabung von  Eingabedaten für eine Lagerbuchhaltung


Problemstellung	Erstellen eines Prototypen mit den Teilsystemen  Auftragserfassung (Lagerabgang), Lagereingang, Berechnung, Visualisierung und Administration. 
Dazu sind folgende Schritte notwendig:
· Entwickeln der Berechnungen für den zeitabhängigen Lagerbestand
· Entwickeln der Datenbankstruktur für die Lagerbuchhaltung der Artikel, z.B. Lagereingangsmenge mit Datum
· Entwickeln der Eingabemasken, z.B. Lagerabgangsmenge mit Auftragsnummer 
· Entwickeln des Systemtests (konkretes Zahlenbeispiel) für die Weiterentwicklung durch Dritte
· Dokumentation für die Weiterentwicklung durch Dritte

Studiengang Informatik


WS    04/05

	

	Studiengang Informatik      

	
	WS 06/07



Studiengang Informatik


SS    03


Juni 2004		Prof. Dr. Manfred Vogel
Prof. Dr. Manfred Vogel / Prof. Dr. Manfred Breit

Technologie 
Fachliche 
Schwerpunkte 

Java; Usability, 
 

Studiengang Informatik


WS    04/05

	

	Studiengang Informatik      

	
	WS 06/07



Studiengang Informatik


SS    03


Juni 2004		Prof. Dr. Manfred Vogel
Prof. Dr. Manfred Vogel / Prof. Dr. Manfred Breit

Links 

          
Projekttyp	P1-2, [X]  5-8 Studierende ca. 1’700 Personenstunden; Doppel
