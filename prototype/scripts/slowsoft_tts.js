var TTSURL = "https://slang.slowsoft.ch/webslang/tts";
var scripts = document.getElementsByTagName('script');
for (var i = 0; i < scripts.length; i++) {
    var ind =  scripts[i].src.search("/static/scripts/slowsoft_tts.js");
    if (ind > 0) {
        TTSURL = scripts[i].src.substr(0, ind) + TTSURL;
    }
}

window.AudioContext = window.AudioContext || window.webkitAudioContext;

function TTS(audioContext) {
    this.audioContext = audioContext || new AudioContext();
}

TTS.prototype.toAudio = function(text, voiceOrLang, options) {
    this.synthesize("audio", text, voiceOrLang, options);
}
TTS.prototype.toWavefile = function(text, voiceOrLang, options) {
    this.synthesize("wav", text, voiceOrLang, options);
}
TTS.prototype.synthesize = function(type, text, voiceOrLang, options) {
    if (options === undefined) options = {};
    if (options.speed === undefined) options.speed = 100;
    if (options.pitch === undefined) options.pitch = 100;
    if (options.bot === undefined) options.bot = false;
    var tts = this;
    var fd=new FormData();
    var xhr=new XMLHttpRequest();
    if ((type == "wav") && typeof(downloadLoaded) !== 'undefined') {
        xhr.responseType = 'blob';
        xhr.onload = function(evt) {
            download(xhr.response, "audio.wav", "audio/wav" );
        };
    }
    else {
        xhr.responseType = 'arraybuffer';
        onDecodingError=function(err){
            trace("*** decoding error");
        }
        onDecoded = function(buffer) {
            trace("audio data decoded");
            var source = tts.audioContext.createBufferSource();
            source.buffer = buffer;
            source.connect(tts.audioContext.destination);
            source.start(0);
        }
        xhr.onload = function() {
            trace("audio data received");
            tts.audioContext.decodeAudioData(xhr.response, onDecoded, onDecodingError);
            return true;
        }
        this.unlockAudio(); // required on iOS
    }
    var data = {};
    data['text'] = text || "";
    data['voiceorlang'] = voiceOrLang || "gsw-CHE-gr";
    data['speed'] = options.speed;
    data['pitch'] = options.pitch;
    if (options.bot) {
        data['bot'] = "1";
    }
    xhr.open("POST", TTSURL);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "audio/wav");
    xhr.send(JSON.stringify(data));
}

TTS.prototype.unlockAudio = function() {
    // create empty buffer and play it
    var buffer = this.audioContext.createBuffer(1, 1, this.audioContext.sampleRate);
    var source = this.audioContext.createBufferSource();
    source.buffer = buffer;
    source.connect(this.audioContext.destination);
    source.start(0);
}

var consoleLogging = false;
trace = function(msg) {
    var notes = document.getElementById('notes');
    if (notes) {
        notes.innerHTML = msg + "<br>" + notes.innerHTML;
    }
    if (consoleLogging) {
        console.log(msg);
    }
}
