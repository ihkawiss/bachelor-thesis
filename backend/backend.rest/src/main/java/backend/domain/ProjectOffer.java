package backend.domain;

import java.util.Date;

/**
 * Simple entity to track a project offer being processed.
 * 
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 */
public class ProjectOffer {

	private final Date created;

	private String title;

	public ProjectOffer() {
		created = new Date();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreated() {
		return created;
	}
}
