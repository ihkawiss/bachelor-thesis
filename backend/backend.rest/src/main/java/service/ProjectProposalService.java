package service;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.scout.rt.platform.util.StringUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backend.domain.ProjectOffer;

public class ProjectProposalService {

	private final static Logger log = LoggerFactory.getLogger(ProjectProposalService.class);

	private volatile static Map<String, ProjectOffer> currentRequests = new HashMap<>();;

	private static ProjectProposalService instance;

	private ProjectProposalService() {
	}

	public static ProjectProposalService get() {
		if (instance == null) {
			instance = new ProjectProposalService();
		}
		log.error("ProjectProposalService CREATED!");
		return instance;
	}

	/**
	 * Creates a new project offer request entry for given token.
	 * 
	 * @param token
	 *            unique id of client sending request
	 */
	public boolean createProjectOfferRequest(String token) {
		if (currentRequests.containsKey(token)) {
			return false;
		}

		log.info("created a new project offer request for session_id={}", token);
		currentRequests.put(token, new ProjectOffer());
		return true;
	}

	/**
	 * Validation of a user entered project title.
	 * 
	 * @param rawTitle
	 *            entered title to validate
	 * @return true if title passed validation
	 */
	public boolean processTitle(String rawTitle, String token) {
		if (StringUtility.isNullOrEmpty(rawTitle)) {
			log.debug("title was null or empty, reject");
			return false;
		}

		String cleaned = StringUtility.cleanup(rawTitle);

		// check if title length is at least 15 chars
		if (cleaned.replaceAll(" ", "").length() < 15) {
			log.debug("title was too short, reject");
			return false;
		}

		// check if is numeric input
		if (cleaned.chars().allMatch(Character::isDigit)) {
			log.debug("title was a number, reject");
			return false;
		}

		// check if contains chars
		if (!cleaned.chars().anyMatch(Character::isAlphabetic)) {
			log.debug("title was not alphanumeric, reject");
			return false;
		}

		log.debug("accepted title={}", cleaned);
		currentRequests.get(token).setTitle(cleaned);
		return true;
	}

}
