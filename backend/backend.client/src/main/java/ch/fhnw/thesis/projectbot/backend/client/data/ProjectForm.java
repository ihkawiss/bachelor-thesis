package ch.fhnw.thesis.projectbot.backend.client.data;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.booleanfield.AbstractBooleanField;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.listbox.AbstractListBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.client.ui.form.fields.tabbox.AbstractTabBox;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;

import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.CancelButton;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox.Adviser2Field;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox.AdviserField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox.CustomerField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox.InstituteField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox.ProjectClassField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox.QuarantaineField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox.SkillsField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.DetailsBox.TitleField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.OkButton;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.CommentBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.CommentBox.CommentField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.InstituteClassificationBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.InstituteClassificationBox.InstituteFeaturesBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.ProblemBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.ProblemBox.ProblemField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.SummaryBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.SummaryBox.SummaryField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.TargetBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.TargetBox.TargetField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.TechnologyBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectForm.MainBox.TextsBox.TechnologyBox.TechnologyField;
import ch.fhnw.thesis.projectbot.backend.shared.data.CreateProjectOfferPermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteFeaturesCodeType;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteLookupCall;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectAdviserLookupCall;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectClassificationCodeType;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectFormData;
import ch.fhnw.thesis.projectbot.backend.shared.data.UpdateProjectOfferPermission;

@FormData(value = ProjectFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class ProjectForm extends AbstractForm {

	private Long id;

	private String instituteFeatures;

	@FormData
	public Long getId() {
		return id;
	}

	@FormData
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("ProjectOffer");
	}

	@Override
	protected int getConfiguredDisplayHint() {
		return DISPLAY_HINT_VIEW;
	}

	public void startModify() {
		startInternalExclusive(new ModifyHandler());
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public TitleField getTitleField() {
		return getFieldByClass(TitleField.class);
	}

	public DetailsBox getDetailsBox() {
		return getFieldByClass(DetailsBox.class);
	}

	public InstituteField getInstituteField() {
		return getFieldByClass(InstituteField.class);
	}

	public CustomerField getCustomerField() {
		return getFieldByClass(CustomerField.class);
	}

	public AdviserField getAdviserField() {
		return getFieldByClass(AdviserField.class);
	}

	public ProjectClassField getProjectClassField() {
		return getFieldByClass(ProjectClassField.class);
	}

	public TextsBox getTextsBox() {
		return getFieldByClass(TextsBox.class);
	}

	public SummaryBox getSummaryBox() {
		return getFieldByClass(SummaryBox.class);
	}

	public SummaryField getSummaryField() {
		return getFieldByClass(SummaryField.class);
	}

	public TargetBox getTargetBox() {
		return getFieldByClass(TargetBox.class);
	}

	public TargetField getTargetField() {
		return getFieldByClass(TargetField.class);
	}

	public ProblemBox getProblemBox() {
		return getFieldByClass(ProblemBox.class);
	}

	public ProblemField getProblemField() {
		return getFieldByClass(ProblemField.class);
	}

	public TechnologyBox getTechnologyBox() {
		return getFieldByClass(TechnologyBox.class);
	}

	public TechnologyField getTechnologyField() {
		return getFieldByClass(TechnologyField.class);
	}

	public SkillsField getSkillsField() {
		return getFieldByClass(SkillsField.class);
	}

	public InstituteClassificationBox getInstituteClassificationBox() {
		return getFieldByClass(InstituteClassificationBox.class);
	}

	public InstituteFeaturesBox getInstituteFeaturesBox() {
		return getFieldByClass(InstituteFeaturesBox.class);
	}

	public CommentBox getCommentBox() {
		return getFieldByClass(CommentBox.class);
	}

	public CommentField getCommentField() {
		return getFieldByClass(CommentField.class);
	}

	public Adviser2Field getAdviser2Field() {
		return getFieldByClass(Adviser2Field.class);
	}

	public QuarantaineField getQuarantaineField() {
		return getFieldByClass(QuarantaineField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@FormData
	public String getInstituteFeatures() {
		return instituteFeatures;
	}

	@FormData
	public void setInstituteFeatures(String instituteFeatures) {
		this.instituteFeatures = instituteFeatures;
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(0)
		public class DetailsBox extends AbstractGroupBox {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Details");
			}

			@Order(1000)
			public class TitleField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Title");
				}

				@Override
				protected int getConfiguredMaxLength() {
					return 128;
				}
			}

			@Order(2000)
			public class InstituteField extends AbstractSmartField<Long> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Institute");
				}

				@Override
				protected Class<? extends ILookupCall<Long>> getConfiguredLookupCall() {
					return InstituteLookupCall.class;
				}
			}

			@Order(3000)
			public class CustomerField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Customer");
				}

				@Override
				protected int getConfiguredMaxLength() {
					return 128;
				}
			}

			@Order(4000)
			public class AdviserField extends AbstractSmartField<Long> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Adviser");
				}

				@Override
				protected Class<? extends ILookupCall<Long>> getConfiguredLookupCall() {
					return ProjectAdviserLookupCall.class;
				}
			}

			@Order(4500)
			public class Adviser2Field extends AbstractSmartField<Long> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Adviser") + " 2";
				}
				
				@Override
				protected Class<? extends ILookupCall<Long>> getConfiguredLookupCall() {
					return ProjectAdviserLookupCall.class;
				}
			}

			@Order(5000)
			public class ProjectClassField extends AbstractSmartField<Long> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("ProjectClass");
				}

				@Override
				protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
					return ProjectClassificationCodeType.class;
				}
			}

			@Order(6000)
			public class SkillsField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Skills");
				}

				@Override
				protected int getConfiguredMaxLength() {
					return 128;
				}
			}

			@Order(7000)
			public class QuarantaineField extends AbstractBooleanField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Quarantaine");
				}
			}
			
		}

		@Order(1000)
		public class TextsBox extends AbstractTabBox {

			@Order(1000)
			public class SummaryBox extends AbstractGroupBox {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Summary");
				}

				@Order(1000)
				public class SummaryField extends AbstractStringField {
					@Override
					protected boolean getConfiguredLabelVisible() {
						return false;
					}

					@Override
					protected boolean getConfiguredMultilineText() {
						return true;
					}

					@Override
					protected int getConfiguredGridH() {
						return 3;
					}
					
					@Override
					protected boolean getConfiguredWrapText() {
						return true;
					}

					@Override
					protected int getConfiguredMaxLength() {
						return Integer.MAX_VALUE;
					}
				}

			}

			@Order(2000)
			public class TargetBox extends AbstractGroupBox {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Target");
				}

				@Order(1000)
				public class TargetField extends AbstractStringField {
					@Override
					protected boolean getConfiguredLabelVisible() {
						return false;
					}

					@Override
					protected boolean getConfiguredMultilineText() {
						return true;
					}

					@Override
					protected int getConfiguredGridH() {
						return 3;
					}

					@Override
					protected boolean getConfiguredWrapText() {
						return true;
					}
					
					@Override
					protected int getConfiguredMaxLength() {
						return Integer.MAX_VALUE;
					}
				}

			}

			@Order(3000)
			public class ProblemBox extends AbstractGroupBox {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Problem");
				}

				@Order(1000)
				public class ProblemField extends AbstractStringField {
					@Override
					protected boolean getConfiguredLabelVisible() {
						return false;
					}

					@Override
					protected boolean getConfiguredMultilineText() {
						return true;
					}

					@Override
					protected int getConfiguredGridH() {
						return 3;
					}

					@Override
					protected boolean getConfiguredWrapText() {
						return true;
					}
					
					@Override
					protected int getConfiguredMaxLength() {
						return Integer.MAX_VALUE;
					}
				}

			}

			@Order(4000)
			public class TechnologyBox extends AbstractGroupBox {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Technology");
				}

				@Order(1000)
				public class TechnologyField extends AbstractStringField {
					@Override
					protected boolean getConfiguredLabelVisible() {
						return false;
					}

					@Override
					protected boolean getConfiguredMultilineText() {
						return true;
					}

					@Override
					protected boolean getConfiguredWrapText() {
						return true;
					}
					
					@Override
					protected int getConfiguredGridH() {
						return 3;
					}

					@Override
					protected int getConfiguredMaxLength() {
						return Integer.MAX_VALUE;
					}
				}

			}

			@Order(4500)
			public class CommentBox extends AbstractGroupBox {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Comment");
				}
				
				@Order(1000)
				public class CommentField extends AbstractStringField {
					@Override
					protected boolean getConfiguredLabelVisible() {
						return false;
					}
					
					@Override
					protected boolean getConfiguredWrapText() {
						return true;
					}

					@Override
					protected boolean getConfiguredMultilineText() {
						return true;
					}

					@Override
					protected int getConfiguredGridH() {
						return 3;
					}

					@Override
					protected int getConfiguredMaxLength() {
						return Integer.MAX_VALUE;
					}
				}

			}

			@Order(5000)
			public class InstituteClassificationBox extends AbstractGroupBox {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("InstituteClassification");
				}

				@Order(1000)
				public class InstituteFeaturesBox extends AbstractListBox<Long> {
					@Override
					protected String getConfiguredLabel() {
						return TEXTS.get("Features");
					}

					@Override
					protected int getConfiguredGridH() {
						return 6;
					}

					@Override
					protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
						return InstituteFeaturesCodeType.class;
					}
				}

			}

		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class ModifyHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			IProjectService service = BEANS.get(IProjectService.class);
			ProjectFormData formData = new ProjectFormData();
			exportFormData(formData);
			formData = service.load(formData);
			importFormData(formData);

			// set institute features
			if (!formData.getInstituteFeatures().equals("")) {
				Set<Long> features = new HashSet<>();
				for (String value : formData.getInstituteFeatures().split(",")) {
					features.add(Long.valueOf(value));
				}

				getInstituteFeaturesBox().setValue(features);
			}

			setEnabledPermission(new UpdateProjectOfferPermission());
		}

		@Override
		protected void execStore() {
			IProjectService service = BEANS.get(IProjectService.class);
			ProjectFormData formData = new ProjectFormData();
			exportFormData(formData);

			// set values of institute features
			String separated = "";
			for (Long value : getInstituteFeaturesBox().getValue()) {
				separated += value + ",";
			}

			if (separated.endsWith(",")) {
				separated = separated.substring(0, separated.length() - 1);
			}

			formData.setInstituteFeatures(separated);

			service.store(formData);
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			IProjectService service = BEANS.get(IProjectService.class);
			ProjectFormData formData = new ProjectFormData();
			exportFormData(formData);
			formData = service.prepareCreate(formData);
			importFormData(formData);

			setEnabledPermission(new CreateProjectOfferPermission());
		}

		@Override
		protected void execStore() {
			IProjectService service = BEANS.get(IProjectService.class);
			ProjectFormData formData = new ProjectFormData();
			exportFormData(formData);

			// set values of institute features
			String separated = "";
			for (Long value : getInstituteFeaturesBox().getValue()) {
				separated += value + ",";
			}

			if (separated.endsWith(",")) {
				separated = separated.substring(0, separated.length() - 1);
			}

			formData.setInstituteFeatures(separated);

			service.create(formData);
		}
	}
}
