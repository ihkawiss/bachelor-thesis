package ch.fhnw.thesis.projectbot.backend.client.data;

import java.util.Set;

import org.eclipse.scout.rt.client.dto.Data;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.AbstractTable;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBooleanColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractLongColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractSmartColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;

import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserTablePage.Table;
import ch.fhnw.thesis.projectbot.backend.shared.Icons;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectAdviserService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteLookupCall;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectAdviserTablePageData;

@Data(ProjectAdviserTablePageData.class)
public class ProjectAdviserTablePage extends AbstractPageWithTable<Table> {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("ProjectAdviser");
	}

	@Override
	protected boolean getConfiguredLeaf() {
		return true;
	}

	@Override
	protected void execLoadData(SearchFilter filter) {
		importPageData(BEANS.get(IProjectAdviserService.class).getProjectAdviserTableData(filter));
	}

	public class Table extends AbstractTable {

		@Order(1000)
		public class EditMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("Edit");
			}

			@Override
			protected String getConfiguredIconId() {
				return Icons.Pencil;
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				ProjectAdviserForm form = new ProjectAdviserForm();
				form.setId(getIdColumn().getSelectedValue());
				form.startModify();
				form.waitFor();

				if (form.isFormStored()) {
					reloadPage();
				}
			}
		}

		@Order(2000)
		public class NewMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("New");
			}

			@Override
			protected String getConfiguredIconId() {
				return Icons.Plus;
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.EmptySpace);
			}

			@Override
			protected void execAction() {
				ProjectAdviserForm form = new ProjectAdviserForm();
				form.startNew();
			}
		}

		@Override
		protected boolean getConfiguredAutoResizeColumns() {
			return true;
		}

		public InstituteColumn getInstituteColumn() {
			return getColumnSet().getColumnByClass(InstituteColumn.class);
		}

		public MailColumn getMailColumn() {
			return getColumnSet().getColumnByClass(MailColumn.class);
		}

		public AvailableColumn getAvailableColumn() {
			return getColumnSet().getColumnByClass(AvailableColumn.class);
		}

		public NameColumn getNameColumn() {
			return getColumnSet().getColumnByClass(NameColumn.class);
		}

		public IdColumn getIdColumn() {
			return getColumnSet().getColumnByClass(IdColumn.class);
		}

		@Order(1000)
		public class IdColumn extends AbstractLongColumn {
			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected boolean getConfiguredDisplayable() {
				return false;
			}
		}

		@Order(2000)
		public class NameColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Name");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(2500)
		public class MailColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Mail");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(3000)
		public class InstituteColumn extends AbstractSmartColumn<Long> {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Institute");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}

			@Override
			protected Class<? extends ILookupCall<Long>> getConfiguredLookupCall() {
				return InstituteLookupCall.class;
			}
		}

		@Order(4000)
		public class AvailableColumn extends AbstractBooleanColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("AvailableForProjects");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

	}
}
