package ch.fhnw.thesis.projectbot.backend.client.data;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.booleanfield.AbstractBooleanField;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;

import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserForm.MainBox.CancelButton;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserForm.MainBox.DetailsBox;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserForm.MainBox.DetailsBox.AvailableField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserForm.MainBox.DetailsBox.InstituteField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserForm.MainBox.DetailsBox.MailField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserForm.MainBox.DetailsBox.NameField;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserForm.MainBox.OkButton;
import ch.fhnw.thesis.projectbot.backend.shared.data.CreateProjectAdviserPermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectAdviserService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteLookupCall;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectAdviserFormData;
import ch.fhnw.thesis.projectbot.backend.shared.data.UpdateProjectAdviserPermission;

@FormData(value = ProjectAdviserFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class ProjectAdviserForm extends AbstractForm {

	private Long id;

	@FormData
	public Long getId() {
		return id;
	}

	@FormData
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("ProjectAdviser");
	}

	public void startModify() {
		startInternalExclusive(new ModifyHandler());
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public DetailsBox getDetailsBox() {
		return getFieldByClass(DetailsBox.class);
	}

	public InstituteField getInstituteField() {
		return getFieldByClass(InstituteField.class);
	}

	public MailField getMailField() {
		return getFieldByClass(MailField.class);
	}

	public AvailableField getAvailableField() {
		return getFieldByClass(AvailableField.class);
	}

	public NameField getNameField() {
		return getFieldByClass(NameField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		public class DetailsBox extends AbstractGroupBox {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Details");
			}

			@Order(1000)
			public class NameField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Name");
				}

				@Override
				protected int getConfiguredMaxLength() {
					return 128;
				}
			}

			@Order(1500)
			public class MailField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Mail");
				}

				@Override
				protected int getConfiguredMaxLength() {
					return 128;
				}
			}

			@Order(2000)
			public class InstituteField extends AbstractSmartField<Long> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Institute");
				}

				@Override
				protected Class<? extends ILookupCall<Long>> getConfiguredLookupCall() {
					return InstituteLookupCall.class;
				}
			}

			@Order(3000)
			public class AvailableField extends AbstractBooleanField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("AvailableForProjects");
				}
			}

		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class ModifyHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			IProjectAdviserService service = BEANS.get(IProjectAdviserService.class);
			ProjectAdviserFormData formData = new ProjectAdviserFormData();
			exportFormData(formData);
			formData = service.load(formData);
			importFormData(formData);

			setEnabledPermission(new UpdateProjectAdviserPermission());
		}

		@Override
		protected void execStore() {
			IProjectAdviserService service = BEANS.get(IProjectAdviserService.class);
			ProjectAdviserFormData formData = new ProjectAdviserFormData();
			exportFormData(formData);
			service.store(formData);
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			IProjectAdviserService service = BEANS.get(IProjectAdviserService.class);
			ProjectAdviserFormData formData = new ProjectAdviserFormData();
			exportFormData(formData);
			formData = service.prepareCreate(formData);
			importFormData(formData);

			setEnabledPermission(new CreateProjectAdviserPermission());
		}

		@Override
		protected void execStore() {
			IProjectAdviserService service = BEANS.get(IProjectAdviserService.class);
			ProjectAdviserFormData formData = new ProjectAdviserFormData();
			exportFormData(formData);
			service.create(formData);
		}
	}
}
