package ch.fhnw.thesis.projectbot.backend.client.data;

import org.eclipse.scout.rt.client.dto.FormData;
import org.eclipse.scout.rt.client.ui.form.AbstractForm;
import org.eclipse.scout.rt.client.ui.form.AbstractFormHandler;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractCancelButton;
import org.eclipse.scout.rt.client.ui.form.fields.button.AbstractOkButton;
import org.eclipse.scout.rt.client.ui.form.fields.groupbox.AbstractGroupBox;
import org.eclipse.scout.rt.client.ui.form.fields.smartfield.AbstractSmartField;
import org.eclipse.scout.rt.client.ui.form.fields.stringfield.AbstractStringField;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;

import ch.fhnw.thesis.projectbot.backend.client.data.InstituteForm.MainBox.CancelButton;
import ch.fhnw.thesis.projectbot.backend.client.data.InstituteForm.MainBox.DetailsBox;
import ch.fhnw.thesis.projectbot.backend.client.data.InstituteForm.MainBox.DetailsBox.CoordinatorField;
import ch.fhnw.thesis.projectbot.backend.client.data.InstituteForm.MainBox.DetailsBox.NameField;
import ch.fhnw.thesis.projectbot.backend.client.data.InstituteForm.MainBox.OkButton;
import ch.fhnw.thesis.projectbot.backend.shared.data.CreateInstitutePermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.IInstituteService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteFormData;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectAdviserLookupCall;
import ch.fhnw.thesis.projectbot.backend.shared.data.UpdateInstitutePermission;

@FormData(value = InstituteFormData.class, sdkCommand = FormData.SdkCommand.CREATE)
public class InstituteForm extends AbstractForm {

	private Long id;

	@FormData
	public Long getId() {
		return id;
	}

	@FormData
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Institute");
	}

	public void startModify() {
		startInternalExclusive(new ModifyHandler());
	}

	public void startNew() {
		startInternal(new NewHandler());
	}

	public CancelButton getCancelButton() {
		return getFieldByClass(CancelButton.class);
	}

	public MainBox getMainBox() {
		return getFieldByClass(MainBox.class);
	}

	public DetailsBox getDetailsBox() {
		return getFieldByClass(DetailsBox.class);
	}

	public CoordinatorField getCoordinatorField() {
		return getFieldByClass(CoordinatorField.class);
	}

	public NameField getNameField() {
		return getFieldByClass(NameField.class);
	}

	public OkButton getOkButton() {
		return getFieldByClass(OkButton.class);
	}

	@Order(1000)
	public class MainBox extends AbstractGroupBox {

		@Order(1000)
		public class DetailsBox extends AbstractGroupBox {
			@Override
			protected String getConfiguredLabel() {
				return TEXTS.get("Details");
			}

			@Order(1000)
			public class NameField extends AbstractStringField {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Name");
				}

				@Override
				protected int getConfiguredMaxLength() {
					return 128;
				}
			}

			@Order(2000)
			public class CoordinatorField extends AbstractSmartField<Long> {
				@Override
				protected String getConfiguredLabel() {
					return TEXTS.get("Coordinator");
				}

				@Override
				protected Class<? extends ILookupCall<Long>> getConfiguredLookupCall() {
					return ProjectAdviserLookupCall.class;
				}
			}

		}

		@Order(100000)
		public class OkButton extends AbstractOkButton {
		}

		@Order(101000)
		public class CancelButton extends AbstractCancelButton {
		}
	}

	public class ModifyHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			IInstituteService service = BEANS.get(IInstituteService.class);
			InstituteFormData formData = new InstituteFormData();
			exportFormData(formData);
			formData = service.load(formData);
			importFormData(formData);

			setEnabledPermission(new UpdateInstitutePermission());
		}

		@Override
		protected void execStore() {
			IInstituteService service = BEANS.get(IInstituteService.class);
			InstituteFormData formData = new InstituteFormData();
			exportFormData(formData);
			service.store(formData);
		}
	}

	public class NewHandler extends AbstractFormHandler {

		@Override
		protected void execLoad() {
			IInstituteService service = BEANS.get(IInstituteService.class);
			InstituteFormData formData = new InstituteFormData();
			exportFormData(formData);
			formData = service.prepareCreate(formData);
			importFormData(formData);

			setEnabledPermission(new CreateInstitutePermission());
		}

		@Override
		protected void execStore() {
			IInstituteService service = BEANS.get(IInstituteService.class);
			InstituteFormData formData = new InstituteFormData();
			exportFormData(formData);
			service.create(formData);
		}
	}
}
