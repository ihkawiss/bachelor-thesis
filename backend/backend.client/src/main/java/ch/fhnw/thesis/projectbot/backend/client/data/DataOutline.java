package ch.fhnw.thesis.projectbot.backend.client.data;

import java.util.List;

import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractOutline;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;
import org.eclipse.scout.rt.shared.TEXTS;

import ch.fhnw.thesis.projectbot.backend.shared.Icons;

public class DataOutline extends AbstractOutline {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("DataOutline");
	}

	@Override
	protected String getConfiguredIconId() {
		return Icons.Folder;
	}
	
	@Override
	protected void execCreateChildPages(List<IPage<?>> pageList) {
		super.execCreateChildPages(pageList);
	}
	
}
