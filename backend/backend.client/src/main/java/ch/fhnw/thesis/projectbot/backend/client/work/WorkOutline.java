package ch.fhnw.thesis.projectbot.backend.client.work;

import java.util.List;

import org.eclipse.scout.rt.client.ui.desktop.outline.AbstractOutline;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.IPage;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;

import ch.fhnw.thesis.projectbot.backend.client.data.InstituteTablePage;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectAdviserTablePage;
import ch.fhnw.thesis.projectbot.backend.client.data.ProjectTablePage;
import ch.fhnw.thesis.projectbot.backend.shared.Icons;

/**
 * <h3>{@link WorkOutline}</h3>
 *
 * @author kevin
 */
@Order(1000)
public class WorkOutline extends AbstractOutline {

	@Override
	protected void execCreateChildPages(List<IPage<?>> pageList) {
		super.execCreateChildPages(pageList);
		pageList.add(new ProjectAdviserTablePage());
		pageList.add(new InstituteTablePage());
		pageList.add(new ProjectTablePage());
	}

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("Work");
	}

	@Override
	protected String getConfiguredIconId() {
		return Icons.Pencil;
	}
}
