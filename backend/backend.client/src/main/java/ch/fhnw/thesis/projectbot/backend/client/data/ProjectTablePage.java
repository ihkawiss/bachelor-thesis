package ch.fhnw.thesis.projectbot.backend.client.data;

import java.util.Set;

import org.eclipse.scout.rt.client.dto.Data;
import org.eclipse.scout.rt.client.ui.action.menu.AbstractMenu;
import org.eclipse.scout.rt.client.ui.action.menu.IMenuType;
import org.eclipse.scout.rt.client.ui.action.menu.TableMenuType;
import org.eclipse.scout.rt.client.ui.basic.table.AbstractTable;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractBooleanColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractLongColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractSmartColumn;
import org.eclipse.scout.rt.client.ui.basic.table.columns.AbstractStringColumn;
import org.eclipse.scout.rt.client.ui.desktop.outline.pages.AbstractPageWithTable;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.platform.util.CollectionUtility;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.ICodeType;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.rt.shared.services.lookup.ILookupCall;

import ch.fhnw.thesis.projectbot.backend.client.data.ProjectTablePage.Table;
import ch.fhnw.thesis.projectbot.backend.shared.Icons;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteLookupCall;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectAdviserLookupCall;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectClassificationCodeType;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectTablePageData;

@Data(ProjectTablePageData.class)
public class ProjectTablePage extends AbstractPageWithTable<Table> {

	@Override
	protected String getConfiguredTitle() {
		return TEXTS.get("ProjectOfferTablePage");
	}

	@Override
	protected boolean getConfiguredLeaf() {
		return true;
	}

	@Override
	protected void execLoadData(SearchFilter filter) {
		importPageData(BEANS.get(IProjectService.class).getProjectOfferTableData(filter));
	}

	public class Table extends AbstractTable {

		@Order(1000)
		public class EditMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("Edit");
			}

			@Override
			protected String getConfiguredIconId() {
				return Icons.Pencil;
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.SingleSelection);
			}

			@Override
			protected void execAction() {
				ProjectForm form = new ProjectForm();
				form.setId(getIdColumn().getSelectedValue());
				form.startModify();
				form.waitFor();
				if (form.isFormStored()) {
					reloadPage();
				}
			}
		}

		@Order(2000)
		public class NewMenu extends AbstractMenu {
			@Override
			protected String getConfiguredText() {
				return TEXTS.get("New");
			}

			@Override
			protected Set<? extends IMenuType> getConfiguredMenuTypes() {
				return CollectionUtility.hashSet(TableMenuType.EmptySpace);
			}

			@Override
			protected String getConfiguredIconId() {
				return Icons.Plus;
			}

			@Override
			protected void execAction() {
				ProjectForm form = new ProjectForm();
				form.startNew();
				form.waitFor();
				if (form.isFormStored()) {
					reloadPage();
				}
			}
		}

		@Override
		protected boolean getConfiguredAutoResizeColumns() {
			return true;
		}

		public TitleColumn getNameColumn() {
			return getColumnSet().getColumnByClass(TitleColumn.class);
		}

		public CustomerColumn getCustomerColumn() {
			return getColumnSet().getColumnByClass(CustomerColumn.class);
		}

		public AdviserColumn getAdviserColumn() {
			return getColumnSet().getColumnByClass(AdviserColumn.class);
		}

		public SummaryColumn getSummaryColumn() {
			return getColumnSet().getColumnByClass(SummaryColumn.class);
		}

		public TargetColumn getTargetColumn() {
			return getColumnSet().getColumnByClass(TargetColumn.class);
		}

		public TechnologyColumn getTechnologyColumn() {
			return getColumnSet().getColumnByClass(TechnologyColumn.class);
		}

		public SkillsColumn getSkillsColumn() {
			return getColumnSet().getColumnByClass(SkillsColumn.class);
		}

		public CommentColumn getCommentColumn() {
			return getColumnSet().getColumnByClass(CommentColumn.class);
		}

		public QuarantaineColumn getQuarantaineColumn() {
			return getColumnSet().getColumnByClass(QuarantaineColumn.class);
		}

		public ProblemColumn getProblemColumn() {
			return getColumnSet().getColumnByClass(ProblemColumn.class);
		}

		public ProjectClassColumn getProjectClassColumn() {
			return getColumnSet().getColumnByClass(ProjectClassColumn.class);
		}

		public InstituteColumn getInstituteColumn() {
			return getColumnSet().getColumnByClass(InstituteColumn.class);
		}

		public IdColumn getIdColumn() {
			return getColumnSet().getColumnByClass(IdColumn.class);
		}

		@Order(1000)
		public class IdColumn extends AbstractLongColumn {
			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected boolean getConfiguredDisplayable() {
				return false;
			}
		}

		@Order(2000)
		public class TitleColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Name");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(3000)
		public class InstituteColumn extends AbstractSmartColumn<Long> {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Institute");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}

			@Override
			protected Class<? extends ILookupCall<Long>> getConfiguredLookupCall() {
				return InstituteLookupCall.class;
			}
		}

		@Order(4000)
		public class CustomerColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Customer");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(5000)
		public class AdviserColumn extends AbstractSmartColumn<Long> {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Adviser");
			}

			@Override
			protected Class<? extends ILookupCall<Long>> getConfiguredLookupCall() {
				return ProjectAdviserLookupCall.class;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(6000)
		public class ProjectClassColumn extends AbstractSmartColumn<Long> {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("ProjectClass");
			}

			@Override
			protected Class<? extends ICodeType<?, Long>> getConfiguredCodeType() {
				return ProjectClassificationCodeType.class;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(7000)
		public class SummaryColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Summary");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(8000)
		public class TargetColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Target");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(9000)
		public class ProblemColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Problem");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(10000)
		public class TechnologyColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Technology");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(11000)
		public class SkillsColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Skills");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(12000)
		public class CommentColumn extends AbstractStringColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Comment");
			}

			@Override
			protected boolean getConfiguredVisible() {
				return false;
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

		@Order(13000)
		public class QuarantaineColumn extends AbstractBooleanColumn {
			@Override
			protected String getConfiguredHeaderText() {
				return TEXTS.get("Quarantaine");
			}

			@Override
			protected int getConfiguredWidth() {
				return 100;
			}
		}

	}
}
