// question page types
const QUESTION_TYPE_TEXT = 0;
const QUESTION_TYPE_TEXT_UNITED = 1;
const QUESTION_TYPE_YES_NO = 2;

// text input question types
const TEXT_INPUT_SINGLE_LINE = 0;
const TEXT_INPUT_MULTI_LINE = 1;

// question pages
let projectQuestionPages = [
    {
        type: QUESTION_TYPE_TEXT,
        audio: AUDIO_IDS.formTitle,
        questions: [
            {
                type: TEXT_INPUT_SINGLE_LINE,
                text: 'Projekt-Titel',
                mandatory: true,
                maxLength: 100,
                info: 'Falls Sie eine konkrete Idee haben, bitte diese mit Stichworten beschreiben.',
                endpoint: 'title',
            },
        ],
    },
    {
        type: QUESTION_TYPE_TEXT,
        audio: AUDIO_IDS.formInitialPositionAndProblemStatement,
        questions: [
            {
                type: TEXT_INPUT_MULTI_LINE,
                text: 'Ausgangslage',
                mandatory: true,
                endpoint: 'initial-position',
            },
            {
                type: TEXT_INPUT_MULTI_LINE,
                text: 'Problemstellung',
                mandatory: true,
                endpoint: 'problem-statement',
            },
        ],
    },
    {
        type: QUESTION_TYPE_TEXT,
        audio: AUDIO_IDS.formObjectives,
        questions: [
            {
                type: TEXT_INPUT_MULTI_LINE,
                text: 'Erwartete Ergebnisse',
                mandatory: true,
                endpoint: 'objective',
            },
        ],
    },
];
const CONTACT_INFORMATION_QUESTION_PAGE = {
    type: QUESTION_TYPE_TEXT_UNITED,
    endpoint: 'contact',
    audio: AUDIO_IDS.formContactData,
    questions: [
        {
            type: TEXT_INPUT_SINGLE_LINE,
            text: 'Firma / Institution',
            mandatory: true,
            maxLength: 50,
            jsonAttributeName: 'institution',
        },
        {
            type: TEXT_INPUT_SINGLE_LINE,
            text: 'Adresse',
            maxLength: 50,
            jsonAttributeName: 'address',
        },
        {
            type: TEXT_INPUT_SINGLE_LINE,
            text: 'PLZ / Ort',
            maxLength: 50,
            jsonAttributeName: 'postcodeCity',
        },
        {
            type: TEXT_INPUT_SINGLE_LINE,
            text: 'Name Kontaktperson',
            mandatory: true,
            maxLength: 50,
            jsonAttributeName: 'name',
        },
        {
            type: TEXT_INPUT_SINGLE_LINE,
            text: 'Telefon',
            maxLength: 50,
            jsonAttributeName: 'phone',
        },
        {
            type: TEXT_INPUT_SINGLE_LINE,
            text: 'E-Mail',
            mandatory: true,
            maxLength: 50,
            jsonAttributeName: 'mail',
        },
    ],
};
const STEP_TITLE_PROJECT_OFFER_QUESTIONS = 'Fragen zum Projekt';
const STEP_TITLE_CONTACT_INFORMATION_QUESTIONS = 'Kontaktdaten';

const QUESTION_PAGE_INDEX_YES_NO = 3;

let questionPageIndex = -1;
let questionPageIndexToEdit = -1;

const ANIMATION_DURATION = 500;

let contentDiv = $('#content');
let receivedSuggestions = false;
let ignoreSuggestions = false;
let showNextQuestionPageCounter = 0;

/**
 * Changes page content.
 * @param title Title to be shown
 * @param content HTML content to be shown
 * @param enableButton <code>true</code> to enable buttons after fade in
 * @param keepHeader <code>true</code> to prevent header from collapsing
 */
function changePageContent(title, content, enableButton, keepHeader) {
    if (!keepHeader) {
        collapseHeader();
    }

    disableButtons();

    var titleHeading = $('#title');
    if (titleHeading.text() !== title) {
        titleHeading.fadeOut(ANIMATION_DURATION, function () {
            titleHeading.text(title);
            titleHeading.fadeIn(ANIMATION_DURATION);
        });
    }

    contentDiv.fadeOut(ANIMATION_DURATION, function () {
        contentDiv.html(content);
        contentDiv.fadeIn(ANIMATION_DURATION);

        // support textarea resizing for IE and Edge
        if (document.documentMode || /Edge/.test(navigator.userAgent)) {
            var answerTextField = $(".answerTextField");

            console.log('answerTextField:');
            console.log(answerTextField);
            answerTextField.width('800px');
            answerTextField.resizable();
        }

        if (enableButton) {
            enableButtons();
        } else {
            // TODO disableButtons();
        }
    });
}

function showStartPage() {
	var informationHtml = '<p>Willkommen! Sie befinden sich bei der Bot-unterstützten Projekteingabe.<br/>' +
        'Falls sie das klassische Formular für die Projekteingabe ohne Bot-Unterstützung verwenden möchten, klicken sie ' +
        '<a href="https://www.fhnw.ch/de/forschung-und-dienstleistungen/technik/studierendenprojekte/formular-fuer-projekteinreichung">hier</a>.' +
        '</p><p>Vielen Dank für Ihr Interesse an unseren Studierendenprojekten.<br/></p>' +
        '<p><b>Alle durchgeführten Projektarbeiten unterliegen einer Gebühr von CHF 1\'500, unabhängig vom Projekterfolg.</b></p>\n' +
        '<br/>';
    var content = informationHtml + '<button onclick="showInformationPage()">Projekteingabe starten</button>';
    changePageContent('Bot zur Projekteingabe', content, true, true);
}

function showInformationPage() {
    playAudio(AUDIO_IDS.pageWelcomeInformation);

    var text = 'Ihnen werden nun einige Fragen zu Ihrem Projekt gestellt. Diese werden Sie mittels Texteingabe oder ' +
        'anklicken von Buttons beantworten können. Falls Sie die Sprachausgabe deaktivieren wollen, können Sie den ' +
        'Button unten rechts anklicken. Alle Fragen sind auch textuell vorhanden.';
    var content = '<p>' + text + '</p><button onclick="showNextQuestionPage()">Fortfahren</button>';
    changePageContent('Information', content, true);
    enableButtons();
}

function showNextQuestionPage() {
    // at least one form page has been shown to user, therefore page leave confirmation is necessary now
    confirmPageLeaveEnabled = true;

    showNextQuestionPageCounter = 0;
    questionPageIndex++;

    // check if was in edit mode
    if (questionPageIndexToEdit !== -1 && questionPageIndex > questionPageIndexToEdit) {
        // was in edit mode => set question page index to high value to show summary page
        questionPageIndex = projectQuestionPages.length + 1;
    }

    var title;
    var questionPage;
    if (questionPageIndex < projectQuestionPages.length) {
        switchActiveBreadcrumb(2);
        title = STEP_TITLE_PROJECT_OFFER_QUESTIONS;
        questionPage = projectQuestionPages[questionPageIndex];

    } else if (questionPageIndex === QUESTION_PAGE_INDEX_YES_NO) {
        switchActiveBreadcrumb(2);
        questionPageIndex--;
        getFirstDecisionTreeQuestionFromBackend();
        return;

    } else if (questionPageIndex === projectQuestionPages.length) {
        switchActiveBreadcrumb(3);
        title = STEP_TITLE_CONTACT_INFORMATION_QUESTIONS;
        questionPage = CONTACT_INFORMATION_QUESTION_PAGE;

    } else {
        showSummaryPage();
        return;
    }

    var audioId = questionPage.audio;
    if (audioId !== undefined) {
        playAudio(audioId);
    }

    var type = questionPage.type;
    var pageContent;
    if (type === QUESTION_TYPE_TEXT || type === QUESTION_TYPE_TEXT_UNITED) {
        pageContent = createTextQuestionPageContent(questionPage);
    } else if (type === QUESTION_TYPE_YES_NO) {
        pageContent = createYesNoQuestionPageContent(questionPage);
    }

    changePageContent(title, pageContent);
}

function getFirstDecisionTreeQuestionFromBackend() {
    sendPayloadToBackend('dt-question', {}, 'PUT');
}

/**
 * Callback function that gets called if the submit button has been clicked on a question page
 * @param answer <code>undefined</code> for text input question page, <code>true</code> and <code>false</code> for "Yes"
 * and "No" on a Yes/No question page
 */
function onClickQuestionAnswer(answer) {
    disableButtons();
    removeSuggestionsMessage();
    removeErrorMessage();

    showNextQuestionPageCounter = 0;
    ignoreSuggestions = receivedSuggestions;

    var questionPage;
    if (questionPageIndex < projectQuestionPages.length) {
        questionPage = projectQuestionPages[questionPageIndex];
    } else if (questionPageIndex === projectQuestionPages.length) {
        questionPage = CONTACT_INFORMATION_QUESTION_PAGE;
    }
    var type = questionPage.type;
    if (type === QUESTION_TYPE_TEXT || type === QUESTION_TYPE_TEXT_UNITED) {
    	var questions = questionPage.questions;
        for (var i = 0; i < questions.length; i++) {
        	var question = questions[i];
            answer = $('.answerTextField')[i].value.trim();
            if (answer.length === 0) {
                answer = undefined;
            }
            question.answer = answer;
        }
    } else if (type === QUESTION_TYPE_YES_NO) {
        questionPage.answer = answer;
    }

    // send answer to backend
    sendAnswerToBackend(questionPage);
}

function sendAnswerToBackend(questionPage) {
	var type = questionPage.type;
    if (type === QUESTION_TYPE_TEXT) {
    	var questions = questionPage.questions;
        for (var i = 0; i < questions.length; i++) {
        	var question = questions[i];
            var payload = {};
            payload.text = question.answer;
            sendPayloadToBackend(question.endpoint, payload);
        }
    } else if (type === QUESTION_TYPE_YES_NO) {
    	var payload = {};
        payload.proceedTrueBranch = questionPage.answer;
        sendPayloadToBackend(questionPage.endpoint, payload);
    } else if (type === QUESTION_TYPE_TEXT_UNITED) {
    	var payload = {};
        var questions = questionPage.questions;
        for (var i = 0; i < questions.length; i++) {
        	var question = questions[i];
            payload[question.jsonAttributeName] = question.answer;
        }
        sendPayloadToBackend(questionPage.endpoint, payload, 'POST');
    }
}

function sendPayloadToBackend(endpoint, payload, type) {
    if (type === undefined) {
        type = 'PUT';
    }
    payload.token = token;
    var json = JSON.stringify(payload);
    console.log(json);
    $.ajax({
        type: type,
        url: '/backend/api/project-offer/' + endpoint,
        contentType: "application/json",
        data: json,
        success: function (response) {
            onReceivedRespond(endpoint, response);
        },
        error: function () {
            // TODO
            console.log('error');
        }
    });
}

function onReceivedRespond(endpoint, response) {
    enableButtons();

    console.log(response);
    var newQuestionPage = {};
    var showNextQuestionPage = true;
    var commands = response.commands;
    commands.forEach(function (command) {
    	var commandType = command.command;
        if (commandType === 'word_suggestion') {
            receivedSuggestions = true;
            if (ignoreSuggestions && isPageValid() && $('.error-message').length === 0) {
                showNextQuestionPage = true;
            } else {
                showSuggestions(endpoint, command.value);
                showNextQuestionPage = false;
            }

        } else if (commandType === 'error') {
            showNextQuestionPage = false;
            receivedSuggestions = false;
            showErrorMessage(endpoint, command.value);

        } else if (commandType === 'explanation') {
            newQuestionPage.info = command.value;
            projectQuestionPages.push(newQuestionPage);

        } else if (commandType === 'speak') {
        	var audioId = AUDIO_IDS[command.value];
            if (audioId !== undefined) {
                playAudio(audioId);
            }

        } else if (commandType === 'ask_question') {
            receivedSuggestions = false;
            newQuestionPage.type = QUESTION_TYPE_YES_NO;
            newQuestionPage.text = command.value;
            newQuestionPage.endpoint = 'dt-question';

        } else if (commandType === 'success') {
            receivedSuggestions = false;
        }
    });
    if (showNextQuestionPage) {
        handleShowNextQuestionPage();
    }
}

function handleShowNextQuestionPage() {
    console.log('handleShowNextQuestionPage');
    showNextQuestionPageCounter++;
    if (questionPageIndex < projectQuestionPages.length) {
    	var questionPage = projectQuestionPages[questionPageIndex];
        var type = questionPage.type;
        if (type === QUESTION_TYPE_TEXT || type === QUESTION_TYPE_TEXT_UNITED) {
            if (questionPage.questions.length === showNextQuestionPageCounter) {
                showNextQuestionPage();
            }
            console.log("COUNTER: " + questionPage.questions.length);
        } else {
            showNextQuestionPage();
        }

    } else if (questionPageIndex === projectQuestionPages.length) {
        showNextQuestionPage();
    }
}

function showSuggestions(endpoint, suggestions) {
    removeSuggestionsMessage(endpoint);
    var text = 'Vorschläge um Text zu ergänzen: ' + suggestions;
    $('#question-' + endpoint).append('<span class="suggestions-message">' + text + '<br/></span>');
}

function showErrorMessage(endpoint, text) {
    removeErrorMessage(endpoint);

    if (text === undefined) {
        text = 'Eingabe ungültig';
    }
    $('#question-' + endpoint).append('<span class="error-message">' + text + '<br/></span>');
}

function removeErrorMessage(endpoint) {
    if (endpoint) {
        $('#question-' + endpoint).find('.error-message').remove();
    } else {
        $('.error-message').remove();
    }
}

function removeSuggestionsMessage(endpoint) {
    if (endpoint) {
        $('#question-' + endpoint).find('.suggestions-message').remove();
    } else {
        $('.suggestions-message').remove();
    }
}

/**
 * Creates HTML content for a text input question page
 * @param questionPage
 * @returns {string} HTML content
 */
function createTextQuestionPageContent(questionPage) {
	var questions = questionPage.questions;

    var idAttribute = '';
    var endpoint = questionPage.endpoint;
    if (endpoint) {
        idAttribute = ' id="question-' + endpoint + '"';
    }

    var content = '<div class="question-container"' + idAttribute + '>';

    for (var i = 0; i < questions.length; i++) {
    	var question = questions[i];
        content += createTextFieldQuestionEntryContent(question);
    }

    content += '<p>' +
        '<button onclick="onClickQuestionAnswer()">OK</button> ' +
        '</p>' +
        '</div>';
    return content;
}

/**
 * Creates HTML content a single text input question
 * @param question
 * @returns {string} HTML content
 */
function createTextFieldQuestionEntryContent(question) {
	var content = '';

    var idAttribute = '';
    var endpoint = question.endpoint;
    if (endpoint) {
        idAttribute = ' id="question-' + endpoint + '"';
    }
    var isMandatory = question.mandatory;
    var title = question.text;
    if (isMandatory) {
        title += ' *';
    }
    content += '<p' + idAttribute + '"><span class="question-title">' + title + '</span><br/>';

    if (question.info !== undefined) {
        content += '<span class="question-info">' + question.info + '</span>';
    }

    var value = question.answer;
    if (value === undefined) {
        value = "";
    }

    var maxLengthAttribute = '';
    if (question.maxLength) {
        maxLengthAttribute = ' maxlength="' + question.maxLength + '"';
    }

    var dataValidAttribute = ' data-valid="' + !isMandatory + '"';

    if (question.type === TEXT_INPUT_SINGLE_LINE) {
        content += '<input type="text" ' +
            'oninput="onInputChanged(this, \'' + question.text + '\')" ' +
            'onkeypress="onEnterPress(event)" ' +
            'class="text-field answerTextField" ' +
            'value="' + value + '"' +
            maxLengthAttribute +
            dataValidAttribute + '/>';
    } else {
        content += '<textarea oninput="onInputChanged(this, \'' + question.text + '\')" ' +
            'class="text-field multi-line answerTextField"' +
            maxLengthAttribute +
            dataValidAttribute + '>' + value + '</textarea>';
    }

    content += '</p>';
    return content;
}

/**
 * Creates HTML content for a Yes/No question page
 * @param questionPage
 * @returns {string} HTML content
 */
function createYesNoQuestionPageContent(questionPage) {
	var content = '<div class="question-container">' +
        '<span class="question-title">' + questionPage.text + '</span><br/>';

    if (questionPage.info) {
        // parse explanation parts (explanation as text, links for more information)
    	var explanationParts = questionPage.info.split('|');
        var explanation = explanationParts[0];
        var links = [];
        for (let i = 1; i < explanationParts.length; i++) {
            links.push(explanationParts[i]);
        }

        content += '<p>Frage unklar? <span onclick="showExplanation()" id="explanation-link">Erklärung</span></p>' +
            '<p id="explanation" style="display: none">' + explanation;

        // add links for more informationen
        if (links.length > 0) {
            content += '<br><br>Weitere Informationen:';
            for (var i = 0; i < links.length; i++) {
                content += '<br><a href="' + links[i] + '" target="_blank">' + links[i] + '</a>';
            }
        }
        content += '</p>';
    }

    content += '<p class="buttons">' +
        '<button onclick="onClickQuestionAnswer(false)">Nein</button> ' +
        '<button onclick="onClickQuestionAnswer(true)">Ja</button>' +
        '</p>' +
        '</div>';

    return content;
}

function onEnterPress(event) {
    console.log(typeof event.keyCode);
    console.log(typeof event.which);
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode === 13) { // 13 => enter key code
        onClickQuestionAnswer();
    }
}

function onInputChanged(element, questionTitle) {
	var value = element.value;
    var changedQuestion;
    var changedQuestionPage;
    projectQuestionPages.forEach(function (projectQuestionPage) {
    	var type = projectQuestionPage.type;
        if (type === QUESTION_TYPE_TEXT || type === QUESTION_TYPE_TEXT_UNITED) {
            projectQuestionPage.questions.forEach(function (question) {
                if (questionTitle === question.text) {
                    changedQuestion = question;
                }
            });
            if (changedQuestion !== undefined) {
                changedQuestionPage = projectQuestionPage;
            }
        }
    });
    if (changedQuestionPage === undefined) {
        CONTACT_INFORMATION_QUESTION_PAGE.questions.forEach(function (question) {
            if (questionTitle === question.text) {
                changedQuestion = question;
            }
        });
        if (changedQuestion !== undefined) {
            changedQuestionPage = CONTACT_INFORMATION_QUESTION_PAGE;
        }
    }

    if (changedQuestionPage !== undefined) {
    	var isValid = validate(value, changedQuestion);
        $(element).attr('data-valid', isValid);

        // check if page is valid
        if (isPageValid()) {
            enableButtons();
        }
    }
}

function isPageValid() {
	var pageValid = true;
    var textFields = $('.text-field');
    for (var i = 0; i < textFields.length; i++) {
    	var textField = $(textFields[i]);
        pageValid &= textField.attr('data-valid') === 'true';
    }
    return pageValid;
}

function validate(value, question) {
    return value.length > 0;
}

/**
 * Shows all questions and answers.
 */
function showSummaryPage() {
    switchActiveBreadcrumb(4);

    playAudio(AUDIO_IDS.pageOverview);

    // project offer questions
    var text = 'Hier sehen Sie nochmal Ihre eingegebenen Daten. Falls Sie noch etwas korrigieren möchten, klicken ' +
        'Sie das <img src="./img/edit.svg" alt="Bleistift" class="edit-image-disabled"/>-Symbol in der ' +
        'entsprechenden Zeile an.';
    var content = '<div class="summary"><span>' + text + '</span><br/><br/><h2>Fragen zum Projekt</h2>';
    for (var i = 0; i < projectQuestionPages.length; i++) {
    	var questionPage = projectQuestionPages[i];

        let type = questionPage.type;
        if (type === QUESTION_TYPE_TEXT || type === QUESTION_TYPE_TEXT_UNITED) {
            let questions = questionPage.questions;
            for (var j = 0; j < questions.length; j++) {
                content += createSummaryPageAnswerEntryContent(questions[j], i);
            }
        } else if (type === QUESTION_TYPE_YES_NO) {
            content += createSummaryPageAnswerEntryContent(questionPage);
        }
    }

    // contact information questions
    content += '<h2>Kontaktdaten <img src="./img/edit.svg" class="edit-image" onclick="editAnswer(' + projectQuestionPages.length + ')"/></h2>';
    var contactQuestions = CONTACT_INFORMATION_QUESTION_PAGE.questions;
    for (let i = 0; i < contactQuestions.length; i++) {
        let question = contactQuestions[i];
        content += createSummaryPageAnswerEntryContent(question);
    }

    content += '</div>';
    content += '<br/><b>Alle durchgeführten Projektarbeiten unterliegen einer Gebühr von CHF 1\'500, unabhängig vom Projekterfolg.</b>';
    content += '<p class="buttons"><button onclick="confirmProjectOffer()">Einreichen</button></p>';

    changePageContent('Zusammenfassung', content);
}

/**
 * Creates HTML content for an question answer entry of the summary page.
 * @param question Question to represent
 * @param index Index of the question page. Can be undefined to prevent adding the edit button
 * @returns {string} HTML content
 */
function createSummaryPageAnswerEntryContent(question, index) {
	var content = '<span class="question-title">' + question.text;

    if (index !== undefined && index >= 0) {
        content += '<img src="./img/edit.svg" class="edit-image" onclick="editAnswer(' + index + ')"/>';
    }

    content += '</span><br/>';

    var answer = question.answer;
    var answerClass = 'question-answer';
    if (answer === undefined) {
        answerClass += ' no-answer';
        answer = 'Keine Angabe';
    } else if (typeof(answer) === typeof(true)) {
        answer = answer ? 'Ja' : 'Nein';
    }

    content += '<span class="' + answerClass + '">' + answer + '</span><br/><br/>';
    return content;
}

/**
 * Switches active breadcrumb
 * @param breadcrumbId Id of the new active breadcrumb
 */
function switchActiveBreadcrumb(breadcrumbId) {
    if (questionPageIndexToEdit === -1) {
        $('#bci' + (breadcrumbId - 1)).removeClass('active');
        $('#bci' + breadcrumbId).addClass('active');
    }
}

/**
 * Shows additional explanation to user
 */
function showExplanation() {
    var explanationDiv = $('#explanation');
    explanationDiv.show(ANIMATION_DURATION);
}

/**
 * Goes into edit mode and shows questionPage of given index to edit
 * @param index
 */
function editAnswer(index) {
    questionPageIndexToEdit = index;
    questionPageIndex = index - 1;
    showNextQuestionPage();
}

function confirmProjectOffer() {
    // project offer has been submitted, therefore page lave confirmation is no longer necessary
    confirmPageLeaveEnabled = false;

    playAudio(AUDIO_IDS.pageDone);

    var text = 'Ihre Projekteinreichung wurde erfolgreich abgesendet. Wir werden innert zwei Arbeitswochen mit Ihnen ' +
        'Kontakt aufnehmen. Vielen Dank für Ihr Interesse an unseren Studierendenprojekten.';
    changePageContent('Abschluss', '<p>' + text + '</p>');
    
    // finalize on server side
    sendPayloadToBackend('finalize', {}, 'POST');
}

function disableButtons() {
    // disable all buttons within page content
    contentDiv.find('button').prop('disabled', true);
}

function enableButtons() {
    contentDiv.find('button').prop('disabled', false);
}
