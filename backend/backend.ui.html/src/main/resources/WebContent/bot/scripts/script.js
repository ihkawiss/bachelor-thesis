const BACKEND_BASE_URL = '/backend/api/project-offer/';
const headerCollapsedClass = 'collapsed';

// will dynamically changed based on page
let confirmPageLeaveEnabled = false;
let headerComponent = $('#header');
let token;

init();

function init() {
    disableButtons();

    // confirmation dialog on page leave
    $(window).bind('beforeunload', function(){
        if (confirmPageLeaveEnabled) {
            // most modern browsers will ignore the following text and show their own dialog message
            return 'Möchten Sie die Seite wirklich verlassen? Alle vorgenommenen Änderungen gehen dabei verloren.';
        }
    });

    // get token from backend
    $.ajax({
        type: 'POST',
        url: BACKEND_BASE_URL + 'token',
        success: function (data) {
            token = data;
            enableButtons();
        },
        error: function () {
            // TODO
            console.log('error');
        }
    });


    showStartPage();
}

function expandHeader() {
    headerComponent.removeClass(headerCollapsedClass);
}

function collapseHeader() {
    headerComponent.addClass(headerCollapsedClass);
}

function toggleMute() {
    let buttonIcon;
    if (getVolumeLevel() === VOLUME_ON) {
        mute();
        buttonIcon = './img/volume-off.svg';
    } else {
        unmute();
        buttonIcon = './img/volume-on.svg';
    }
    $('#img-mute-icon').attr('src', buttonIcon);
}
