const AUDIO_IDS = {
    // simple pages
    pageWelcomeInformation: 1001,
    pageOverview: 1006,
    pageDone: 1007,

    // form title
    formTitle: 1002,
    client_InvalidTitle: 1030,

    // form initial position and problem statement
    formInitialPositionAndProblemStatement: 1003,
    client_InitialPositionInvalid: 1027,
    client_InitialPositionTooLong: 1028,
    client_InitialPositionBadQuality: 1029,
    client_ProblemStatementInvalid: 1035,
    client_ProblemStatementTooLong: 1036,
    client_ProblemStatementBadQuality: 1037,

    // form objective
    formObjectives: 1004,
    client_ObjectiveInvalid: 1032,
    client_ObjectiveTooLong: 1033,
    client_ObjectiveBadQuality: 1034,

    // contact data form
    formContactData: 1005,
    client_ContactDataInvalid: 1011,
    client_ContactDataInvalidMail: 1012,
    client_ContactDataInvalidPLZ: 1013,
    client_ContactDataInvalidPhone: 1014,

    // decision tree questions
    client_ArtificialIntelligence: 1008,
    client_BigData: 1009,
    client_ComputerGraphics: 1010,
    client_DataVisualization: 1021,
    client_DistributetSystem: 1022,
    client_EfficientAlgorithms: 1023,
    client_FastData: 1024,
    client_Gameing: 1025,
    client_ICT: 1026,
    client_Mobile: 1031,
    client_SocialNetwork: 1038,
    client_ThingsFromUniverse: 1039,
    client_UserExperiance: 1040,

    // crucial questions
    client_CrucialQuestionArtificialIntelligence: 1015,
    client_CrucialQuestionExperience: 1016,
    client_CrucialQuestionResearch1: 1017,
    client_CrucialQuestionResearch2: 1018,
    client_CrucialQuestionResearch3: 1019,
    client_CrucialQuestionSurroundingSystems: 1020,
};

const VOLUME_ON = 1;
const VOLUME_OFF = 0;

let audio = new Audio();
let volumeLevel = VOLUME_ON;

function playAudio(audioId) {
    // stop ongoing audio
    audio.pause();

    // play new audio
    let filename = './audio/utt' + audioId + '.wav';
    audio = new Audio(filename);
    audio.volume = volumeLevel;
    audio.play();
}

function mute() {
    volumeLevel = VOLUME_OFF;
    audio.volume = volumeLevel;
}

function unmute() {
    volumeLevel = VOLUME_ON;
    audio.volume = volumeLevel;
}

function getVolumeLevel() {
    return volumeLevel;
}
