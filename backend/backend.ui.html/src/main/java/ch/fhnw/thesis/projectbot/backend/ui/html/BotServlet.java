package ch.fhnw.thesis.projectbot.backend.ui.html;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tika.Tika;
import org.eclipse.scout.rt.platform.resource.BinaryResource;
import org.eclipse.scout.rt.ui.html.res.loader.AbstractResourceLoader;
import org.eclipse.scout.rt.ui.html.res.loader.BinaryFileLoader;
import org.eclipse.scout.rt.ui.html.res.loader.HtmlFileLoader;

public class BotServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final String UI_THEME = "ui.theme";
	private static final String BASE_DIR = "bot";
	private static final String INDEX_FILE = "/index.html";
	private static final String FILE_TYPE_BOT_AUDIO = ".wav";

	private Tika tika = new Tika();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		setAccessControlHeaders(resp);

		String path = req.getRequestURI();
		req.getClass();
		// send index.html on root request
		if (path.equals("/")) {
			path = INDEX_FILE;
		}

		// set content type
		String detectedContentType = tika.detect(path);
		resp.setContentType(detectedContentType);

		// initialize ResourceLoader based on file type
		AbstractResourceLoader resourceLoader;
		if (path.endsWith(FILE_TYPE_BOT_AUDIO)) {
			resourceLoader = new BinaryFileLoader();
		} else {
			resourceLoader = new HtmlFileLoader(UI_THEME, true, true);
		}

		try (OutputStream os = resp.getOutputStream()) {
			String dir = BASE_DIR;
			if(path.equals("/login.html")) {
				dir = "";
			}
			
			BinaryResource resource = resourceLoader.loadResource(dir + path);
			byte[] content = resource.getContent();
			os.write(content, 0, content.length);
		} catch (Exception e) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
	}

	@Override
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		setAccessControlHeaders(resp);
		resp.setStatus(HttpServletResponse.SC_OK);
	}

	/**
	 * Set basic CORS filter to allow communication with API.
	 * 
	 * @param resp to enrich with CORS filter
	 */
	private void setAccessControlHeaders(HttpServletResponse resp) {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT");
	}

}
