package ch.fhnw.thesis.projectbot.backend.ui.html;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.scout.rt.platform.Replace;
import org.eclipse.scout.rt.platform.config.CONFIG;
import org.eclipse.scout.rt.server.commons.ServerCommonsConfigProperties.CspEnabledProperty;
import org.eclipse.scout.rt.server.commons.servlet.HttpClientInfo;
import org.eclipse.scout.rt.server.commons.servlet.HttpServletControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Replace
public class UiHttpServletControl extends HttpServletControl {
	private final static Logger LOG = LoggerFactory.getLogger(UiHttpServletControl.class);

	private static final long serialVersionUID = 2171764146877770898L;

	@Override
	protected void setResponseHeaders(HttpServlet servlet, HttpServletRequest req, HttpServletResponse resp) {
		if (!"GET".equals(req.getMethod())) {
			return;
		}
		resp.setHeader(HTTP_HEADER_X_FRAME_OPTIONS, SAMEORIGIN);
		resp.setHeader(HTTP_HEADER_X_XSS_PROTECTION, XSS_MODE_BLOCK);

		if (CONFIG.getPropertyValue(CspEnabledProperty.class)) {
			if (HttpClientInfo.get(req).isMshtml()) {
				resp.setHeader(HTTP_HEADER_CSP_LEGACY, getCspToken());
			} else {
				StringBuilder cspTokens = new StringBuilder();
				cspTokens.append("default-src *;");
				cspTokens.append("script-src * 'unsafe-inline';");
				cspTokens.append("style-src 'self' 'unsafe-inline' https://fonts.googleapis.com;");
				cspTokens.append("connect-src *;");
				cspTokens.append("frame-src *;");
				cspTokens.append("child-src *;");
				cspTokens.append("img-src 'self' data:;");
				cspTokens.append("report-uri csp-report");
				resp.setHeader(HTTP_HEADER_CSP, cspTokens.toString());
			}
		}
	}

}
