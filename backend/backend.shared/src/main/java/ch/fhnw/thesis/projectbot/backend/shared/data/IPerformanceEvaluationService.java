package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.shared.TunnelToServer;

@TunnelToServer
public interface IPerformanceEvaluationService {

	String evaluate();

}
