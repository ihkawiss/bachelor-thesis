package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

@TunnelToServer
public interface IProjectAdviserService extends IService {

	ProjectAdviserTablePageData getProjectAdviserTableData(SearchFilter filter);

	ProjectAdviserFormData prepareCreate(ProjectAdviserFormData formData);

	ProjectAdviserFormData create(ProjectAdviserFormData formData);

	ProjectAdviserFormData load(ProjectAdviserFormData formData);

	ProjectAdviserFormData store(ProjectAdviserFormData formData);
	
	String getAdviserProposal(Long instituteId);
}
