package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class InstituteCodeType extends AbstractCodeType<Long, Long> {

	public static final String I4DS = "I4DS";
	public static final String IMVS = "IMVS";
	public static final String IIT = "IIT";
	public static final String IP14 = "IP14";

	private static final long serialVersionUID = 1L;
	public static final long ID = 1L;

	@Override
	public Long getId() {
		return ID;
	}

	@Order(1000)
	public static class I4DSCode extends AbstractCode<Long> {

		private static final long serialVersionUID = 1L;
		public static final long ID = 1L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get(I4DS);
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(2000)
	public static class IMVSCode extends AbstractCode<Long> {

		private static final long serialVersionUID = 1L;
		public static final long ID = 2L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get(IMVS);
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(3000)
	public static class IITCode extends AbstractCode<Long> {

		private static final long serialVersionUID = 1L;
		public static final long ID = 3L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get(IIT);
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(3000)
	public static class IP14Code extends AbstractCode<Long> {

		private static final long serialVersionUID = 1L;
		public static final long ID = 4L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get(IP14);
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	public static long stringToInstituteId(String institute) {
		switch (institute.toUpperCase()) {
		case I4DS:
			return I4DSCode.ID;
		case IIT:
			return IITCode.ID;
		case IMVS:
			return IMVSCode.ID;
		case IP14:
			return IP14Code.ID;
		}

		return -1;
	}

	public static String idToInstituteString(long instituteId) {
		if (instituteId == I4DSCode.ID) {
			return I4DS;
		} else if (instituteId == IITCode.ID) {
			return IIT;
		} else if (instituteId == IMVSCode.ID) {
			return IMVS;
		} else if (instituteId == IP14Code.ID) {
			return IP14;
		}

		return null;
	}

}
