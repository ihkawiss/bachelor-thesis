package ch.fhnw.thesis.projectbot.backend.shared.rest.dto;

/**
 * Represents responses to client.
 * 
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 * @author Hoang Tran <hoang.tran@students.fhnw.ch>
 */
public class CommandDto {

	public static final String DO_ERROR = "error";
	public static final String DO_SUCCESS = "success";
	public static final String DO_NOTICE = "notice";
	public static final String DO_WORD_SUGGESTION = "word_suggestion";
	public static final String ASK_QUESTION = "ask_question";
	public static final String EXPLANATION = "explanation";
	public static final String SPEAK = "speak";

	private String command;
	private String value;

	public CommandDto() {
	}

	public CommandDto(String command, String value) {
		this.command = command;
		this.value = value;
	}

	public String getCommand() {
		return command;
	}

	public String getValue() {
		return value;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
