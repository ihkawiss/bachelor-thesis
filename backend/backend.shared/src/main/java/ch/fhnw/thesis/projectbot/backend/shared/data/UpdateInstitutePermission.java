package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class UpdateInstitutePermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateInstitutePermission() {
		super(UpdateInstitutePermission.class.getSimpleName());
	}
}
