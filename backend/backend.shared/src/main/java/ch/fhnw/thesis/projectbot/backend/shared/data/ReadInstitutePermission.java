package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class ReadInstitutePermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadInstitutePermission() {
		super(ReadInstitutePermission.class.getSimpleName());
	}
}
