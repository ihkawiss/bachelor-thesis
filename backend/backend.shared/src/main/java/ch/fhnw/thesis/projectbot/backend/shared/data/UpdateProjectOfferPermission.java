package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class UpdateProjectOfferPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateProjectOfferPermission() {
		super(UpdateProjectOfferPermission.class.getSimpleName());
	}
}
