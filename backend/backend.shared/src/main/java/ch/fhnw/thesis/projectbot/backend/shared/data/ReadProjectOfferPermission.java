package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class ReadProjectOfferPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadProjectOfferPermission() {
		super(ReadProjectOfferPermission.class.getSimpleName());
	}
}
