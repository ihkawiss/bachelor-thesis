package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

@TunnelToServer
public interface IProjectService extends IService {

	ProjectTablePageData getProjectOfferTableData(SearchFilter filter);

	ProjectFormData prepareCreate(ProjectFormData formData);

	ProjectFormData create(ProjectFormData formData);

	ProjectFormData load(ProjectFormData formData);

	ProjectFormData store(ProjectFormData formData);

	Object[][] loadDecisionTreeTraining();

	Object[][] loadTrainingData();
	
	Object[][] buildDecisionTreeTainMatrix(Object[][] result);

	Object[][] loadTrainingDataInTrx();
}
