package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.shared.services.lookup.ILookupService;
import org.eclipse.scout.rt.shared.services.lookup.LookupCall;

public class ProjectAdviserLookupCall extends LookupCall<Long> {

	private static final long serialVersionUID = 1L;

	@Override
	protected Class<? extends ILookupService<Long>> getConfiguredService() {
		return IProjectAdviserLookupService.class;
	}
}
