package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;

import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.ContactDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.FinalizeDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.QuestionDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.ResponseDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.TextDto;

@TunnelToServer
public interface IProjectOfferService extends IService {

	/**
	 * TODO
	 * 
	 * @param token
	 * @return
	 */
	void createProjectOfferRequest(String token);

	/**
	 * TODO
	 * 
	 * @param rawTitle
	 * @param token
	 * @return
	 */
	ResponseDto processTitle(TextDto dto);

	/**
	 * TODO
	 * 
	 * @param dto
	 * @return
	 */
	ResponseDto processInitialPosition(TextDto dto);

	/**
	 * TODO
	 * 
	 * @param dto
	 * @return
	 */
	ResponseDto processProblemStatement(TextDto dto);

	/**
	 * TODO
	 * 
	 * @param dto
	 * @return
	 */
	ResponseDto processObjective(TextDto dto);

	/**
	 * 
	 * @param dto
	 * @return
	 */
	ResponseDto getNextQuestion(QuestionDto dto);

	/**
	 * 
	 * @param dto
	 * @return
	 */
	ResponseDto processContactData(ContactDto dto);

	/**
	 * 
	 * @param trainData
	 */
	void writeTrainCSV(Object[][] trainData);

	/**
	 * 
	 * @param dto
	 */
	void finalizeRequest(FinalizeDto dto);

}
