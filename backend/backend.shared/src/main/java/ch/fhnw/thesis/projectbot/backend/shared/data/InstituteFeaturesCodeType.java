package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class InstituteFeaturesCodeType extends AbstractCodeType<Long, Long> {

	private static final long serialVersionUID = 1L;
	public static final Long ID = 1l;

	@Override
	public Long getId() {
		return ID;
	}

	@Order(1000)
	public static class UniverseCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 1L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("ThingsFromUniverse");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(2000)
	public static class ArtificialIntelligenceCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 2L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("ArtificialIntelligence");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(3000)
	public static class BigDataCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 3L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("BigData");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(4000)
	public static class SocialNetworkCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 4L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("SocialNetwork");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(5000)
	public static class ComputerGraphicsCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 5L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("ComputerGraphics");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(6000)
	public static class DataVisualizationCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 6L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("DataVisualization");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(7000)
	public static class GameingCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 7L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Gameing");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(8000)
	public static class UserExperianceCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 8L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("UserExperiance");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(9000)
	public static class MobileCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 9L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("Mobile");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(10000)
	public static class DistributetSystemCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 10L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("DistributetSystem");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(11000)
	public static class FastDataCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 11L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("FastData");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(12000)
	public static class ICTCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 12L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("ICT");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(13000)
	public static class EfficientAlgorithmsCode extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 13L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("EfficientAlgorithms");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}
	
	

}
