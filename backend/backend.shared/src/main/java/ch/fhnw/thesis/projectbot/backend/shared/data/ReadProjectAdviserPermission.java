package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class ReadProjectAdviserPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public ReadProjectAdviserPermission() {
		super(ReadProjectAdviserPermission.class.getSimpleName());
	}
}
