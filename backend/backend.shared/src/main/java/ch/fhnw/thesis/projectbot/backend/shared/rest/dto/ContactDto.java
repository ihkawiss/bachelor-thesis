package ch.fhnw.thesis.projectbot.backend.shared.rest.dto;

import org.eclipse.scout.rt.platform.util.StringUtility;

public class ContactDto extends AbstractTokenDto {

	private String institution;
	private String address;
	private String postcodeCity;
	private String name;
	private String phone;
	private String mail;

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostcodeCity() {
		return postcodeCity;
	}

	public void setPostcodeCity(String postcodeCity) {
		this.postcodeCity = postcodeCity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isValid() {
		return !StringUtility.isNullOrEmpty(institution) && !StringUtility.isNullOrEmpty(name)
				&& !StringUtility.isNullOrEmpty(mail);
	}
}
