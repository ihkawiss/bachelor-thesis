package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;

@TunnelToServer
public interface IInstituteService extends IService {

	InstituteTablePageData getInstituteTableData(SearchFilter filter);

	InstituteFormData prepareCreate(InstituteFormData formData);

	InstituteFormData create(InstituteFormData formData);

	InstituteFormData load(InstituteFormData formData);

	InstituteFormData store(InstituteFormData formData);
	
	String getAdvisorAdressesByInstitute(Long id);
	
	String getAdvisorRemainingAdresses(Long id);

}
