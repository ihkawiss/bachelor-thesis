package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class CreateProjectOfferPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateProjectOfferPermission() {
		super(CreateProjectOfferPermission.class.getSimpleName());
	}
}
