package ch.fhnw.thesis.projectbot.backend.shared.rest.dto;

/**
 * Object used to transfer text.
 * 
 * @author Kevin Kirn <kevin.kirn@fhnw.students.ch>
 */
public class TextDto extends AbstractTokenDto {

	private String text;

	public TextDto() {
	}

	public TextDto(String title, String token) {
		this.text = title;
		this.token = token;
	}

	public String getText() {
		return text;
	}

	public void setText(String title) {
		this.text = title;
	}

}
