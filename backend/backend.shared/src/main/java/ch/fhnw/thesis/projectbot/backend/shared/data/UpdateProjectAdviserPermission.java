package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class UpdateProjectAdviserPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public UpdateProjectAdviserPermission() {
		super(UpdateProjectAdviserPermission.class.getSimpleName());
	}
}
