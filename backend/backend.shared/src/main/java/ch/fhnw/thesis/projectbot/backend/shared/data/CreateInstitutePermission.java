package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class CreateInstitutePermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateInstitutePermission() {
		super(CreateInstitutePermission.class.getSimpleName());
	}
}
