package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.shared.TunnelToServer;
import org.eclipse.scout.rt.shared.services.lookup.ILookupService;

@TunnelToServer
public interface IInstituteLookupService extends ILookupService<Long> {
}
