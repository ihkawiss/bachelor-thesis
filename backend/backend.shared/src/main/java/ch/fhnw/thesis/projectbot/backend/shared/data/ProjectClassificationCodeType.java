package ch.fhnw.thesis.projectbot.backend.shared.data;

import org.eclipse.scout.rt.platform.Order;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCode;
import org.eclipse.scout.rt.shared.services.common.code.AbstractCodeType;

public class ProjectClassificationCodeType extends AbstractCodeType<Long, Long> {

	private static final long serialVersionUID = 1L;
	public static final long ID = 2L;

	@Override
	public Long getId() {
		return ID;
	}

	@Order(1000)
	public static class IP14Code extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 1L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("IP14");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(2000)
	public static class IP5Code extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 2L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("IP5");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

	@Order(3000)
	public static class IP6Code extends AbstractCode<Long> {
		private static final long serialVersionUID = 1L;
		public static final long ID = 3L;

		@Override
		protected String getConfiguredText() {
			return TEXTS.get("IP6");
		}

		@Override
		public Long getId() {
			return ID;
		}
	}

}
