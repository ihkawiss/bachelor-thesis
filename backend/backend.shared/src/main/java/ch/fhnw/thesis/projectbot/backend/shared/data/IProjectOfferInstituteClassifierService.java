package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.util.List;

import org.eclipse.scout.rt.platform.service.IService;
import org.eclipse.scout.rt.shared.TunnelToServer;

@TunnelToServer
public interface IProjectOfferInstituteClassifierService extends IService {

	void train(Object[][] trainData);

	Long classifyInstitute(String text);
	
	String extractNounsToString(String text);

	List<String> extractKeywordsAsList(String text);
}
