package ch.fhnw.thesis.projectbot.backend.shared.data;

import java.security.BasicPermission;

public class CreateProjectAdviserPermission extends BasicPermission {

	private static final long serialVersionUID = 1L;

	public CreateProjectAdviserPermission() {
		super(CreateProjectAdviserPermission.class.getSimpleName());
	}
}
