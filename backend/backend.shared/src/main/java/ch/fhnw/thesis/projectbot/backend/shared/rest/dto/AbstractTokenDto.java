package ch.fhnw.thesis.projectbot.backend.shared.rest.dto;

public abstract class AbstractTokenDto {

	protected String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
