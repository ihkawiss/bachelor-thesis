package ch.fhnw.thesis.projectbot.backend.shared.rest.dto;

import java.util.ArrayList;
import java.util.List;

public class ResponseDto {

	private final List<CommandDto> commands;

	private boolean error;

	public ResponseDto() {
		commands = new ArrayList<>();
	}

	public ResponseDto(CommandDto dto) {
		this();
		commands.add(dto);
	}

	public List<CommandDto> getCommands() {
		return commands;
	}

	public void addCommand(CommandDto dto) {
		commands.add(dto);
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public boolean isError() {
		return error;
	}
}
