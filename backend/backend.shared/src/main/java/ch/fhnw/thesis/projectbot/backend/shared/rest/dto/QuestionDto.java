package ch.fhnw.thesis.projectbot.backend.shared.rest.dto;

public class QuestionDto extends AbstractTokenDto {

	private boolean proceedTrueBranch;

	public boolean isProceedTrueBranch() {
		return proceedTrueBranch;
	}

	public void setProceedTrueBranch(boolean proceedTrueBranch) {
		this.proceedTrueBranch = proceedTrueBranch;
	}
}
