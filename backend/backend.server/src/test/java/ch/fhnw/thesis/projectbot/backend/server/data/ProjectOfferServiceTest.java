package ch.fhnw.thesis.projectbot.backend.server.data;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.scout.rt.testing.platform.runner.RunWithSubject;
import org.eclipse.scout.rt.testing.server.runner.RunWithServerSession;
import org.eclipse.scout.rt.testing.server.runner.ServerTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.fhnw.thesis.projectbot.backend.server.ServerSession;
import ch.fhnw.thesis.projectbot.backend.server.service.ProjectOfferService;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.TextDto;

@RunWithSubject("anonymous")
@RunWith(ServerTestRunner.class)
@RunWithServerSession(ServerSession.class)
public class ProjectOfferServiceTest {

	@Test(expected = IllegalArgumentException.class)
	public void testCreateProjectOfferRequest() {
		ProjectOfferService service = new ProjectOfferService();

		// there are never two equal tokens allowed
		service.createProjectOfferRequest("token1");
		service.createProjectOfferRequest("token1");
	}

	@Test
	public void testTitleValidation() {
		ProjectOfferService service = new ProjectOfferService();
		String token = "token1";
		service.createProjectOfferRequest(token);

		// valid titles, no exceptions here
		service.processTitle(new TextDto("Brandschutz Mobilde App", token));
		service.processTitle(new TextDto("Trenderkennung mittels ML Algorithmen", token));
		service.processTitle(new TextDto("Word2Vec Industrie App", token));

		// invalid titles
		List<String> invalidTitles = new ArrayList<>();
		invalidTitles.add("");
		invalidTitles.add(null);
		invalidTitles.add("123456789012345");
		invalidTitles.add("Very Short     ");
		invalidTitles.add("               ");
		invalidTitles.add("Fast richtig!!");
		invalidTitles.add("+*%&/()=?+*%&/()=?");

		for (String title : invalidTitles) {
			try {
				service.processTitle(new TextDto(title, token));
				Assert.fail("Invalid title considered valid!");
			} catch (IllegalArgumentException iae) {
				// NOP
			}
		}

		Assert.assertTrue(true); // passed
	}

}
