package ch.fhnw.thesis.projectbot.backend.server.rest;

import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;

import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectOfferService;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.ContactDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.FinalizeDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.QuestionDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.ResponseDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.TextDto;

@ApplicationScoped
@Path("/project-offer")
public class ProjectOfferResource {

	private IProjectOfferService projectService;

	@PostConstruct
	public void init() {
		projectService = BEANS.get(IProjectOfferService.class);
	}

	@POST
	@Path("/token")
	public Response createToken() {
		String uniqueID = UUID.randomUUID().toString();
		try {
			projectService.createProjectOfferRequest(uniqueID);
			return Response.status(Status.OK).entity(uniqueID).build();
		} catch (IllegalArgumentException iae) {
			return Response.status(Status.BAD_REQUEST).entity(iae.getMessage()).build();
		}
	}

	@PUT
	@Path("/title")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response putTitle(TextDto dto) {
		try {
			ResponseDto responseDto = projectService.processTitle(dto);
			return Response.ok().entity(responseDto).build();
		} catch (IllegalArgumentException iae) {
			return Response.status(Status.BAD_REQUEST).entity(iae.getMessage()).build();
		}
	}

	@PUT
	@Path("/initial-position")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response putInitialPosition(TextDto dto) {
		try {
			ResponseDto responseDto = projectService.processInitialPosition(dto);
			return Response.ok().entity(responseDto).build();
		} catch (IllegalArgumentException iae) {
			return Response.status(Status.BAD_REQUEST).entity(iae.getMessage()).build();
		}
	}

	@PUT
	@Path("/problem-statement")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response putProblemStatement(TextDto dto) {
		try {
			ResponseDto responseDto = projectService.processProblemStatement(dto);
			return Response.ok().entity(responseDto).build();
		} catch (IllegalArgumentException iae) {
			return Response.status(Status.BAD_REQUEST).entity(iae.getMessage()).build();
		}
	}

	@PUT
	@Path("/objective")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response putObjective(TextDto dto) {
		try {
			ResponseDto responseDto = projectService.processObjective(dto);
			return Response.ok().entity(responseDto).build();
		} catch (IllegalArgumentException iae) {
			return Response.status(Status.BAD_REQUEST).entity(iae.getMessage()).build();
		}
	}

	@PUT
	@Path("/dt-question")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNextDecisionTreeQuestion(QuestionDto dto) {
		try {
			ResponseDto responseDto = projectService.getNextQuestion(dto);
			return Response.ok().entity(responseDto).build();
		} catch (IllegalArgumentException iae) {
			return Response.status(Status.BAD_REQUEST).entity(iae.getMessage()).build();
		}
	}

	@POST
	@Path("/contact")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addContactData(ContactDto dto) {
		try {
			ResponseDto responseDto = projectService.processContactData(dto);
			return Response.ok().entity(responseDto).build();
		} catch (IllegalArgumentException iae) {
			return Response.status(Status.BAD_REQUEST).entity(iae.getMessage()).build();
		}
	}
	
	@POST
	@Path("/finalize")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response finalizeProjectOffer(FinalizeDto dto) {
		try {
			projectService.finalizeRequest(dto);
			return Response.ok().entity("success").build();
		} catch (IllegalArgumentException iae) {
			return Response.status(Status.BAD_REQUEST).entity(iae.getMessage()).build();
		}
	}

}
