package ch.fhnw.thesis.projectbot.backend.server.domain;

/**
 * @author Hoang Tran <hoang.tran@students.fhnw.ch>
 */
public class CrucialQuestionGroup {
    final private float weighting;
    final private CrucialQuestion[] crucialQuestions;

    public CrucialQuestionGroup(float weighting, CrucialQuestion... crucialQuestions) {
        this.weighting = weighting;
        this.crucialQuestions = crucialQuestions;
    }

    public float getWeighting() {
        return weighting;
    }

    public CrucialQuestion[] getCrucialQuestions() {
        return crucialQuestions;
    }
}
