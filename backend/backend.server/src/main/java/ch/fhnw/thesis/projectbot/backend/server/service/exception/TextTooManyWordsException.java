package ch.fhnw.thesis.projectbot.backend.server.service.exception;

public class TextTooManyWordsException extends RuntimeException {

    public TextTooManyWordsException(String message) {
        super(message);
    }
}
