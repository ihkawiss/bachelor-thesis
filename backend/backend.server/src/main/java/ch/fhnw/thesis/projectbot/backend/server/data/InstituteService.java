package ch.fhnw.thesis.projectbot.backend.server.data;

import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;

import ch.fhnw.thesis.projectbot.backend.server.sql.SQLs;
import ch.fhnw.thesis.projectbot.backend.shared.data.CreateInstitutePermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.IInstituteService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteFormData;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteTablePageData;
import ch.fhnw.thesis.projectbot.backend.shared.data.ReadInstitutePermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.UpdateInstitutePermission;

public class InstituteService implements IInstituteService {

	@Override
	public InstituteTablePageData getInstituteTableData(SearchFilter filter) {
		InstituteTablePageData pageData = new InstituteTablePageData();
		SQL.selectInto(SQLs.INSTITUTE_TABLE_SELECT, pageData);
		return pageData;
	}

	@Override
	public InstituteFormData prepareCreate(InstituteFormData formData) {
		if (!ACCESS.check(new CreateInstitutePermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [kevin] add business logic here.
		return formData;
	}

	@Override
	public InstituteFormData create(InstituteFormData formData) {
		if (!ACCESS.check(new CreateInstitutePermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		SQL.insert(SQLs.INSTITUTE_CREATE, formData);
		return formData;
	}

	@Override
	public InstituteFormData load(InstituteFormData formData) {
		if (!ACCESS.check(new ReadInstitutePermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		SQL.selectInto(SQLs.INSTITUTE_FORM_SELECT, formData);
		return formData;
	}

	@Override
	public InstituteFormData store(InstituteFormData formData) {
		if (!ACCESS.check(new UpdateInstitutePermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		SQL.update(SQLs.INSTITUTE_UPDATE, formData);
		return formData;
	}

	@Override
	public String getAdvisorAdressesByInstitute(Long id) {
		Object[][] result = SQL.select(SQLs.INSTITUTE_COORDINATOR_SELECT, new NVPair("id",id));

		String recipients = "";
		for (Object[] row : result) {
			recipients += row[0] + ",";
		}

		return recipients;
	}
	
	@Override
	public String getAdvisorRemainingAdresses(Long id) {
		Object[][] result = SQL.select(SQLs.INSTITUTE_REMAINING_COORDINATOR_SELECT, new NVPair("id",id));

		String recipients = "";
		for (Object[] row : result) {
			recipients += row[0] + ",";
		}

		return recipients;
	}
}
