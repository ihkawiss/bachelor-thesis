package ch.fhnw.thesis.projectbot.backend.server.sql;

import org.eclipse.scout.rt.platform.config.CONFIG;
import org.eclipse.scout.rt.server.jdbc.mysql.AbstractMySqlSqlService;

import ch.fhnw.thesis.projectbot.backend.server.sql.DatabaseProperties.JdbcMappingNameProperty;

public class MySqlService extends AbstractMySqlSqlService {

	@Override
	protected String getConfiguredUsername() {
		return CONFIG.getPropertyValue(DatabaseUsernameProperty.class);
	}

	@Override
	protected String getConfiguredPassword() {
		String password = CONFIG.getPropertyValue(DatabasePasswordProperty.class);
		password = password != null ? password : "";
		return password;
	}

	@Override
	protected String getConfiguredJdbcMappingName() {
		return CONFIG.getPropertyValue(JdbcMappingNameProperty.class);
	}
}
