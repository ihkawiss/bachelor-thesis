package ch.fhnw.thesis.projectbot.backend.server.service;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.config.CONFIG;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.fhnw.thesis.projectbot.backend.server.configuration.MailSmtpAuthProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.MailSmtpHostProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.MailSmtpPasswordProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.MailSmtpPortProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.MailSmtpStarTlsEnabledProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.MailSmtpUseSSLProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.MailSmtpUsernameProperty;

@ApplicationScoped
public class MailService {

	private final static Logger log = LoggerFactory.getLogger(MailService.class);

	/**
	 * General method to send a eMail using google mail server.
	 * 
	 * @param message
	 *            to send (text/html)
	 * @param recipients
	 *            to send mail to
	 * @param subject
	 *            of message
	 */
	public void sendMail(String message, String recipients, String carbonCopy, String subject) {
		log.info("sending a new mail to {}", recipients);

		String port = CONFIG.getPropertyValue(MailSmtpPortProperty.class);
		String host = CONFIG.getPropertyValue(MailSmtpHostProperty.class);
		String starTls = CONFIG.getPropertyValue(MailSmtpStarTlsEnabledProperty.class);
		String smtpAuth = CONFIG.getPropertyValue(MailSmtpAuthProperty.class);
		String username = CONFIG.getPropertyValue(MailSmtpUsernameProperty.class);
		String password = CONFIG.getPropertyValue(MailSmtpPasswordProperty.class);
		String ssl = CONFIG.getPropertyValue(MailSmtpUseSSLProperty.class);
		
		assert port != null;
		assert host != null;
		assert starTls != null;
		assert smtpAuth != null;
		assert username != null;
		assert password != null;
		assert ssl != null;

		// mail settings
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", smtpAuth);
		properties.put("mail.smtp.starttls.enable", starTls);
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.ssl.enable", ssl);

		// create user session
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		};

		Session session = Session.getInstance(properties, auth);
		session.setDebug(true);
		
		// create message
		Message msg = new MimeMessage(session);
		try {
			msg.setFrom(new InternetAddress(username));
			InternetAddress[] toAddresses = InternetAddress.parse(recipients);
			InternetAddress[] ccAddresses = InternetAddress.parse(carbonCopy);
			msg.setRecipients(Message.RecipientType.TO, toAddresses);
			msg.setRecipients(Message.RecipientType.CC, ccAddresses);
			msg.setSubject(subject);
			msg.setSentDate(new Date());

			// set template as mail content
			msg.setContent(message, "text/html; charset=UTF-8");

			// sends the e-mail
			Transport.send(msg);

			log.info("message successfully send to {}", recipients);

		} catch (AddressException e) {
			log.error("could not create InternetAddress for recipient", e);
		} catch (MessagingException e) {
			log.error("could not send mail", e);
		}
	}

}