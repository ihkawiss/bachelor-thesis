package ch.fhnw.thesis.projectbot.backend.server.domain;

/**
 * @author Hoang Tran <hoang.tran@students.fhnw.ch>
 */
public class CrucialQuestion {
    private final String question;
    private boolean answeredWithYes;

    public CrucialQuestion(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public boolean isAnsweredWithYes() {
        return answeredWithYes;
    }

    public void setAnsweredWithYes(boolean answeredWithYes) {
        this.answeredWithYes = answeredWithYes;
    }
}
