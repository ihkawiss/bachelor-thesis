package ch.fhnw.thesis.projectbot.backend.server.decisiontree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.scout.rt.platform.ApplicationScoped;

/**
 * This BEAN implements a decision tree classifier for institute evaluation.
 * Based on existing project data, this classifier tries to predict if a given
 * project belongs to a certain institute. However, this may only be used in
 * combination with other techniques which enhance the resulting confidence.
 * 
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 */
@ApplicationScoped
public class InstituteDecisionTreeClassifier {

	/**
	 * <p>
	 * Counts the number of times a type is present in provided data set.
	 * </p>
	 * <p>
	 * Invariant: last index of row must be a string representing the type.
	 * </p>
	 * 
	 * @param data
	 *            containing type label at index = data[n].length
	 * @return map containing counts for each found type
	 */
	public Map<String, Integer> getClassCount(Object[][] data) {
		Map<String, Integer> result = new HashMap<>();
		for (Object[] row : data) {
			if (!(row[row.length - 1] instanceof String)) {
				throw new IllegalArgumentException("Last index of dataset must be a type label (String).");
			}

			final String typeLabel = (String) row[row.length - 1];
			if (!result.containsKey(typeLabel)) {
				result.put(typeLabel, 0);
			}

			result.put(typeLabel, result.get(typeLabel) + 1);
		}
		return result;
	}

	/**
	 * Calculates the Gini impurity for provided data set.
	 * 
	 * @return the calculated impurity
	 */
	public double getGiniImpurity(Object[][] data) {
		double impurity = 1.0;
		Map<String, Integer> counts = getClassCount(data);

		for (Map.Entry<String, Integer> label : counts.entrySet()) {
			float propOfLabel = label.getValue() / (float) data.length;
			impurity -= Math.pow(propOfLabel, 2);
		}

		return impurity;
	}

	/**
	 * Calculates the uncertainty of given data set.
	 * 
	 * @param partition
	 *            to calculate information gain on
	 * @param uncertainty
	 *            (gini impurity) of whole data set
	 * @return
	 */
	public double getInformationGain(Partition partition, double uncertainty) {
		Object[][] left = partition.getTrueRows();
		Object[][] right = partition.getFalseRows();

		float p = left.length / (float) (left.length + right.length);

		return uncertainty - p * getGiniImpurity(left) - (1 - p) * getGiniImpurity(right);
	}

	/**
	 * Find the next best question to ask based on information gain.
	 * 
	 * @param data
	 *            set to perform split on
	 * @return split representing best question and information gain
	 */
	public Split getBestSplit(Object[][] data) {
		int numberOfFeatures = data[0].length - 1;
		double bestGain = 0;
		double currentUncertainty = getGiniImpurity(data);
		Question bestQuestion = null;

		for (int col = 0; col < numberOfFeatures; col++) {
			Set<Object> uniqueValues = new HashSet<Object>();

			for (Object[] row : data) {
				uniqueValues.add(row[col]);
			}

			for (Object value : uniqueValues) {
				Question question = new Question(col, value);

				// try to split the data set
				Partition partition = new Partition(question, data);

				// skip this split if it's not dividing the dataset
				if (partition.getTrueRows().length == 0 || partition.getFalseRows().length == 0) {
					continue;
				}

				double gain = getInformationGain(partition, currentUncertainty);

				if (gain >= bestGain) {
					bestGain = gain;
					bestQuestion = question;
				}

			}

		}

		return new Split(bestGain, bestQuestion);
	}

	/**
	 * Build a tree by recursive call to itself.
	 * 
	 * @param data
	 * @return
	 */
	public INode buildTree(Object[][] data) {
		Split bestSplit = getBestSplit(data);

		if (bestSplit.getGain() == 0)
			return new Leaf(data);

		Partition partition = new Partition(bestSplit.getQuestion(), data);

		INode trueBranch = buildTree(partition.getTrueRows());
		INode falseBranch = buildTree(partition.getFalseRows());

		return new DecisionNode(bestSplit.getQuestion(), partition, trueBranch, falseBranch);
	}

	/**
	 * Prints a decision tree in readable manner.
	 * 
	 * @param node
	 *            to begin printing at
	 * @param spacing
	 *            to indent child nodes
	 */
	public void printTree(INode node, Object[] headers, String spacing) {
		if (node instanceof Leaf) {
			System.out.println(spacing + "Predict" + ((Leaf) node).toString());
			return;
		}

		DecisionNode decisionNode = (DecisionNode) node;

		// Print Question
		Question q = decisionNode.getQuestion();
		String condition = q.getValue() instanceof Number ? ">=" : "==";
		System.out.println(spacing + String.format("Is %s %s %s?", headers[q.getColumn()], condition, q.getValue()));

		System.out.println(spacing + "--> True:");
		printTree(node.getTrueBranch(), headers, spacing + "  ");

		System.out.println(spacing + "--> False:");
		printTree(node.getFalseBranch(), headers, spacing + "  ");
	}

	/**
	 * Build a readable question for given node.
	 * 
	 * @param node
	 *            containing the question
	 * @param headers
	 *            of training set
	 * @return readable question
	 */
	public String getQuestion(INode node, Object[] headers) {
		if (node instanceof Leaf) {
			return "Predict" + ((Leaf) node).toString();
		}

		DecisionNode decisionNode = (DecisionNode) node;

		// Print Question
		Question q = decisionNode.getQuestion();
		String condition = q.getValue() instanceof Number ? ">=" : "==";
		// return String.format("Is %s %s %s?", headers[q.getColumn()], condition,
		// q.getValue());
		return (String) headers[q.getColumn()];
	}

	/**
	 * Classifies a unknown row based on the passed tree.
	 * 
	 * @param row
	 *            unknown row to classify
	 * @param node
	 *            to begin classification at
	 */
	public static Map<String, Integer> classify(Object[] row, INode node) {
		if (node instanceof Leaf) {
			return ((Leaf) node).getPredictions();
		}

		if (((DecisionNode) node).getQuestion().match(row)) {
			return classify(row, node.getTrueBranch());
		} else {
			return classify(row, node.getFalseBranch());
		}
	}
}
