package ch.fhnw.thesis.projectbot.backend.server.decisiontree;

public interface INode {
	
	Partition getPartition();
	
	INode getTrueBranch();

	INode getFalseBranch();

}
