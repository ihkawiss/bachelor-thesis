package ch.fhnw.thesis.projectbot.backend.server.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.security.auth.Subject;

import org.eclipse.scout.rt.platform.context.RunContexts;
import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.security.SimplePrincipal;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;

import ch.fhnw.thesis.projectbot.backend.server.sql.SQLs;
import ch.fhnw.thesis.projectbot.backend.shared.data.CreateProjectOfferPermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectService;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectFormData;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectTablePageData;
import ch.fhnw.thesis.projectbot.backend.shared.data.ReadProjectOfferPermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.UpdateProjectOfferPermission;

public class ProjectService implements IProjectService {

	@Override
	public ProjectTablePageData getProjectOfferTableData(SearchFilter filter) {
		ProjectTablePageData pageData = new ProjectTablePageData();
		SQL.selectInto(SQLs.PROJECT_OFFER_TABLE_SELECT, pageData);
		return pageData;
	}

	@Override
	public ProjectFormData prepareCreate(ProjectFormData formData) {
		if (!ACCESS.check(new CreateProjectOfferPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		// TODO [kevin] add business logic here.
		return formData;
	}

	@Override
	public ProjectFormData create(ProjectFormData formData) {
		if (!ACCESS.check(new CreateProjectOfferPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

		SQL.insert(SQLs.PROJECT_OFFER_CREATE, formData);

		return formData;
	}

	@Override
	public ProjectFormData load(ProjectFormData formData) {
		if (!ACCESS.check(new ReadProjectOfferPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

		SQL.selectInto(SQLs.PROJECT_OFFER_FORM_SELECT, formData);

		return formData;
	}

	@Override
	public ProjectFormData store(ProjectFormData formData) {
		if (!ACCESS.check(new UpdateProjectOfferPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}

		SQL.update(SQLs.PROJECT_OFFER_UPDATE, formData);

		return formData;
	}

	@Override
	public Object[][] loadDecisionTreeTraining() {
		Subject subject = new Subject();
		subject.getPrincipals().add(new SimplePrincipal("john")); // TODO: invoked by rest, check security here...
		subject.setReadOnly();

		Object[][] result = RunContexts.empty().withSubject(subject).withLocale(Locale.US)
				.call(new Callable<Object[][]>() {

					@Override
					public Object[][] call() throws Exception {
						return SQL.select(
								SQLs.DECISION_TREE_SELECT);
					}
				});

		return buildDecisionTreeTainMatrix(result);
	}

	@Override
	public Object[][] buildDecisionTreeTainMatrix(Object[][] result) {
		List<Object[]> matrix = new ArrayList<>();
		for (Object[] row : result) {
			String features = (String) row[1];
			if (StringUtility.isNullOrEmpty(features)) {
				continue;
			}
			String[] values = features.split(",");
			if (values.length == 0) {
				continue;
			}

			Object[] m = new Object[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, (String) row[0]};

			for (String val : values) {
				int numeric = Integer.parseInt(val);
				m[numeric - 1] = 1;
			}

			matrix.add(m);
		}

		Object[][] array = new Object[matrix.size()][];
		for (int i = 0; i < matrix.size(); i++) {
			array[i] = matrix.get(i);
		}
		return array;
	}

	@Override
	public Object[][] loadTrainingData() {
		return SQL.select(SQLs.TRAIN_DATA_SELECT);
	}

	@Override
	public Object[][] loadTrainingDataInTrx() {
		Subject subject = new Subject();
		subject.getPrincipals().add(new SimplePrincipal("voicebot"));
		subject.setReadOnly();

		return RunContexts.empty().withSubject(subject).withLocale(Locale.US).call(this::loadTrainingData);
	}
}
