package ch.fhnw.thesis.projectbot.backend.server.service.exception;

public class TextQualityException extends RuntimeException {

	private static final long serialVersionUID = -2558534522955643018L;

	public TextQualityException(String message) {
		super(message);
	}
}
