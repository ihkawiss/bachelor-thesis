package ch.fhnw.thesis.projectbot.backend.server.sql;

public interface SQLs {

	String PROJECT_OFFER_TABLE_SELECT = ""
			+ "SELECT	id,"
			+ "			title,"
			+ "			institute_id,"
			+ "			customer,"
			+ "			adviser_id, "
			+ "			project_class_id,"
			+ "			summary,"
			+ "			target,"
			+ "			problem,"
			+ "			technology,"
			+ "			skills,"
			+ "			comment, "
			+ "			quarantaine "
			+ "FROM		project_offer "
			+ "INTO		:id,"
			+ "			:title,"
			+ "			:institute,"
			+ "			:customer,"
			+ "			:adviser,"
			+ "			:projectClass,"
			+ "			:summary,"
			+ "			:target,"
			+ "			:problem,"
			+ "			:technology,"
			+ "			:skills,"
			+ "			:comment, "
			+ "			:quarantaine";
	
	String PROJECT_OFFER_FORM_SELECT = ""
			+ "SELECT	id,"
			+ "			title,"
			+ "			institute_id,"
			+ "			customer,"
			+ "			adviser_id, "
			+ "			adviser_id2, "
			+ "			project_class_id,"
			+ "			summary,"
			+ "			target,"
			+ "			problem,"
			+ "			technology,"
			+ "			skills,"
			+ "			comment, "
			+ "			institute_features,"
			+ "			quarantaine "
			+ "FROM		project_offer "
			+ "WHERE	id = :id "
			+ "INTO		:id,"
			+ "			:title,"
			+ "			:institute,"
			+ "			:customer,"
			+ "			:adviser,"
			+ "			:adviser2,"
			+ "			:projectClass,"
			+ "			:summary,"
			+ "			:target,"
			+ "			:problem,"
			+ "			:technology,"
			+ "			:skills,"
			+ "			:comment,"
			+ "			:instituteFeatures,"
			+ "			:quarantaine";
	
	String TRAIN_DATA_SELECT = ""
			+ "SELECT	po.id,"
			+ "			po.title,"
			+ "			po.institute_id,"
			+ "			po.customer,"
			+ "			po.adviser_id, "
			+ "			po.adviser_id2, "
			+ "			po.project_class_id,"
			+ "			po.summary,"
			+ "			po.target,"
			+ "			po.problem,"
			+ "			po.technology,"
			+ "			po.skills,"
			+ "			po.comment, "
			+ "			po.institute_features,"
			+ " 		i.name "
			+ "FROM		project_offer po, institute i "
			+ "WHERE 	po.institute_id = i.id "
			+ "AND		po.quarantaine = 0";
	
	String PROJECT_OFFER_UPDATE = ""
			+ "UPDATE	project_offer "
			+ "SET		title = :title,"
			+ "			institute_id = :institute,"
			+ "			customer = :customer,"
			+ "			adviser_id = :adviser,"
			+ "			adviser_id2 = :adviser2,"
			+ "			project_class_id = :projectClass,"
			+ "			summary = :summary,"
			+ "			target = :target,"
			+ "			problem = :problem,"
			+ "			technology = :technology,"
			+ "			skills = :skills, "
			+ "			institute_features = :instituteFeatures, "
			+ "			comment = :comment, "
			+ "			quarantaine = :quarantaine "
			+ "WHERE	id = :id";

	String PROJECT_OFFER_CREATE = ""
			+ "INSERT INTO project_offer ("
			+ "			title,"
			+ "			institute_id,"
			+ "			customer,"
			+ "			adviser_id, "
			+ "			adviser_id2, "
			+ "			project_class_id,"
			+ "			summary,"
			+ "			target,"
			+ "			problem,"
			+ "			technology,"
			+ "			skills,"
			+ "			comment, "
			+ "			institute_features, "
			+ "			quarantaine "
			+ ") VALUES ("
			+ "			:title,"
			+ "			:institute,"
			+ "			:customer,"
			+ "			:adviser,"
			+ "			:adviser2,"
			+ "			:projectClass,"
			+ "			:summary,"
			+ "			:target,"
			+ "			:problem,"
			+ "			:technology,"
			+ "			:skills,"
			+ "			:comment,"
			+ "			:instituteFeatures,"
			+ "			:quarantaine"
			+ ")";
	
	/*---------------------------------------------
	 * Project adviser 
	 *--------------------------------------------*/
	
	String PROJECT_ADVISER_TABLE_SELECT = ""
			+ "SELECT	id,"
			+ "			name,"
			+ "			mail,"
			+ "			institute_id,"
			+ "			available "
			+ "FROM		project_adviser "
			+ "INTO		:id,"
			+ "			:name,"
			+ "			:mail,"
			+ "			:institute, "
			+ "			:available";
	
	String PROJECT_ADVISER_FORM_SELECT = ""
			+ "SELECT	id,"
			+ "			name,"
			+ "			mail,"
			+ "			institute_id,"
			+ "			available "
			+ "FROM		project_adviser "
			+ "WHERE	id = :id "
			+ "INTO		:id,"
			+ "			:name,"
			+ "			:mail,"
			+ "			:institute,"
			+ "			:available";
	
	String PROJECT_ADVISER_CREATE = ""
			+ "INSERT INTO project_adviser ("
			+ "			name,"
			+ "			mail,"
			+ "			institute_id, "
			+ "			available)"
			+ "VALUES	(:name,"
			+ "			:mail,"
			+ "			:institute, "
			+ "			:available)";
	
	String PROJECT_ADVISER_UPDATE = ""
			+ "UPDATE 	project_adviser "
			+ "SET		name = :name,"
			+ "			mail = :mail,"
			+ "			institute_id = :institute,"
			+ "			available = :available "
			+ "WHERE	id = :id";
	
	String PROJECT_ADVISER_PROPOSAL = ""
			+ "SELECT 	name "
			+ "FROM 	project_adviser "
			+ "WHERE 	institute_id = :instituteId "
			+ "AND 		available = 1";
	
	/*---------------------------------------------
	 * Institutes 
	 *--------------------------------------------*/
	String INSTITUTE_TABLE_SELECT = ""
			+ "SELECT	id,"
			+ "			name,"
			+ "			coordinator_id "
			+ "FROM		institute "
			+ "INTO		:id,"
			+ "			:name,"
			+ "			:coordinator";
	
	String INSTITUTE_COORDINATOR_SELECT = "" +
			"SELECT		p.mail " +
			"FROM 		project_adviser p, " +
			"			institute i " +
			"WHERE 		p.id = i.coordinator_id " + 
			"AND		i.id = :id";
	
	String INSTITUTE_REMAINING_COORDINATOR_SELECT = "" +
			"SELECT		p.mail " +
			"FROM 		project_adviser p, " +
			"			institute i " +
			"WHERE 		p.id = i.coordinator_id " + 
			"AND		i.id <> :id";
	
	String INSTITUTE_FORM_SELECT = ""
			+ "SELECT	id,"
			+ "			name,"
			+ "			coordinator_id "
			+ "FROM		institute "
			+ "WHERE	id = :id "
			+ "INTO		:id,"
			+ "			:name,"
			+ "			:coordinator";
	
	String INSTITUTE_CREATE = ""
			+ "INSERT INTO institute ("
			+ "			name,"
			+ "			coordinator_id)"
			+ "VALUES	(:name,"
			+ "			:coordinator)";
	
	String INSTITUTE_UPDATE = ""
			+ "UPDATE 	institute "
			+ "SET		name = :name,"
			+ "			coordinator_id = :coordinator "
			+ "WHERE	id = :id";

	String DECISION_TREE_SELECT = ""
			+ "SELECT 	i.name, "
			+ "			p.institute_features "
			+ "FROM 	project_offer p, "
			+ "			institute i "
			+ "WHERE 	p.institute_id = i.id";
}
