package ch.fhnw.thesis.projectbot.backend.server.decisiontree;

/**
 * This class is simply used to partition a data set.
 * 
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 */
public class Question {

	private final int column;
	private final Object value;

	public Question(int column, Object value) {
		this.column = column;
		this.value = value;
	}

	/**
	 * Compares the feature value in given example to the value in this question.
	 * 
	 * @param exampleRow row of a example to evaluate
	 * @return if provided example matches question
	 */
	public boolean match(Object[] exampleRow) {
		Object value = exampleRow[this.column];
		if (value instanceof Number && this.value instanceof Number) {
			if (value instanceof Integer && this.value instanceof Integer) {
				return (int) value >= (int) this.value;
			} else {
				String typeName = value.getClass().getName();
				throw new UnsupportedOperationException(
						String.format("Explicit casting of Object to %s not supported.", typeName));
			}
		} else {
			return value.equals(this.value);
		}
	}

	public int getColumn() {
		return column;
	}

	public Object getValue() {
		return value;
	}

}
