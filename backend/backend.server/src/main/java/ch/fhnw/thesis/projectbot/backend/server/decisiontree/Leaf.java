package ch.fhnw.thesis.projectbot.backend.server.decisiontree;

import java.util.Map;

/**
 * Leaf node of the tree that classifies data.
 * 
 * It holds a dictionary of classes (e.g., "Apple") -> number of times it
 * appears in the training data set that reaches this leaf.
 * 
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 */
public class Leaf implements INode {

	private final Map<String, Integer> predictions;

	public Leaf(Object[][] data) {
		this.predictions = new InstituteDecisionTreeClassifier().getClassCount(data);
	}

	public Map<String, Integer> getPredictions() {
		return predictions;
	}

	@Override
	public INode getTrueBranch() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public INode getFalseBranch() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Partition getPartition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return predictions.toString();
	}

}
