package ch.fhnw.thesis.projectbot.backend.server.data;

import org.eclipse.scout.rt.server.jdbc.lookup.AbstractSqlLookupService;

import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectAdviserLookupService;

public class ProjectAdviserLookupService extends AbstractSqlLookupService<Long>
		implements IProjectAdviserLookupService {
	
	@Override
	protected String getConfiguredSqlSelect() {
		return ""
				+ "SELECT	id, name "
				+ "FROM 	project_adviser "
				+ "WHERE	1=1 "
				+ "<key>AND id = :key</key> "
				+ "<text> AND UPPER(name) LIKE UPPER(CONCAT(:text,'%'))</text> "
				+ "<all> </all>";
	}
}
