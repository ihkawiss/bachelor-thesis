package ch.fhnw.thesis.projectbot.backend.server;

import java.util.regex.Pattern;

public class ValidationUtil {

	/**
	 * Validation of eMail address.
	 * 
	 * Taken from: https://stackoverflow.com/questions/8204680/java-regex-email
	 */
	private static final String EMAIL_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";

	/**
	 * Validation of swiss phone number.
	 * 
	 * Taken from: https://gist.github.com/peyerluk/8357036
	 */
	private static final String PHONE_REGEX = "^(?:(?:|0{1,2}|\\+{0,2})41(?:|\\(0\\))|0)([1-9]\\d)(\\d{3})(\\d{2})(\\d{2})$";

	/**
	 * Validation of swiss zip codes.
	 */
	private static final String PLZ_REGEX = "^\\d{4}\\s[a-zA-ZäëïöüáéíóúàèìòùâêîôûãõÄËÏÖÜÁÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕẞß\\- .]+$";

	/**
	 * Validates if provided mail address is valid
	 * 
	 * @param candidate
	 *            to validate
	 * @return true if mail is valid, false otherwise
	 */
	public static boolean isValidMail(String candidate) {
		Pattern mail = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
		return mail.matcher(candidate).find();
	}

	/**
	 * Validates if provided phone number is valid
	 * 
	 * @param candidate
	 *            to validate
	 * @return true if phone number is valid, false otherwise
	 */
	public static boolean isValidPhone(String candidate) {
		Pattern mail = Pattern.compile(PHONE_REGEX, Pattern.CASE_INSENSITIVE);
		return mail.matcher(candidate).find();
	}

	/**
	 * Validates if provided phone number is valid
	 * 
	 * @param candidate
	 *            to validate
	 * @return true if phone number is valid, false otherwise
	 */
	public static boolean isValidPLZ(String candidate) {
		Pattern mail = Pattern.compile(PLZ_REGEX, Pattern.CASE_INSENSITIVE);
		return mail.matcher(candidate).find();
	}

}
