package ch.fhnw.thesis.projectbot.backend.server.decisiontree;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a partition created for a question. It's a simple
 * holder for both true and false rows matching / violating the question.
 * 
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 */
public class Partition {

	private final Question question;
	private final Object[][] data;

	private final Object[][] trueRows;
	private final Object[][] falseRows;

	public Partition(Question question, Object[][] data) {
		this.question = question;
		this.data = data;

		List<Object[]> trueRowList = new ArrayList<>();
		List<Object[]> falseRowList = new ArrayList<>();

		// perform partitioning
		for (Object[] row : data) {
			if (question.match(row)) {
				trueRowList.add(row);
			} else {
				falseRowList.add(row);
			}
		}

		if (trueRowList.size() > 0) {
			trueRows = new Object[trueRowList.size()][(trueRowList.get(0)).length];
			for (int i = 0; i < trueRowList.size(); i++)
				trueRows[i] = trueRowList.get(i);
		} else {
			trueRows = new Object[0][0];
		}

		if (falseRowList.size() > 0) {
			falseRows = new Object[falseRowList.size()][(falseRowList.get(0)).length];
			for (int i = 0; i < falseRowList.size(); i++)
				falseRows[i] = falseRowList.get(i);
		} else {
			falseRows = new Object[0][0];
		}

	}

	public Question getQuestion() {
		return question;
	}

	public Object[][] getData() {
		return data;
	}

	public Object[][] getTrueRows() {
		return trueRows;
	}

	public Object[][] getFalseRows() {
		return falseRows;
	}

}
