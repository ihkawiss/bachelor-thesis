package ch.fhnw.thesis.projectbot.backend.server.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;

import javax.annotation.PostConstruct;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.CreateImmediately;
import org.eclipse.scout.rt.platform.config.CONFIG;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.fhnw.thesis.projectbot.backend.server.configuration.PythonTrainingDirectoryProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.PythonTrainingFilenameProperty;
import ch.fhnw.thesis.projectbot.backend.server.decisiontree.DecisionNode;
import ch.fhnw.thesis.projectbot.backend.server.decisiontree.InstituteDecisionTreeClassifier;
import ch.fhnw.thesis.projectbot.backend.server.domain.ProjectOffer;
import ch.fhnw.thesis.projectbot.backend.shared.data.IPerformanceEvaluationService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectOfferInstituteClassifierService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectOfferService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteCodeType;

@ApplicationScoped
@CreateImmediately
public class PerformanceEvaluationService implements IPerformanceEvaluationService {
	private final static Logger LOG = LoggerFactory.getLogger(PerformanceEvaluationService.class);

	private final int FOLD_SIZE = 5;

	private List<Float> decisionTreeResults = new ArrayList<>();
	private List<Float> pythonResults = new ArrayList<>();
	private List<Float> htResults = new ArrayList<>();
	private List<Float> overAllResults = new ArrayList<>();

	private String DATA_CSV;
	private String TMP_VOICEBOT;

	@PostConstruct
	public void postConstruct() {
		DATA_CSV = CONFIG.getPropertyValue(PythonTrainingFilenameProperty.class);
		TMP_VOICEBOT = CONFIG.getPropertyValue(PythonTrainingDirectoryProperty.class);

		assert DATA_CSV != null;
		assert TMP_VOICEBOT != null;
	}

	@Override
	public String evaluate() {
		Object[][] trainingData = BEANS.get(IProjectService.class).loadTrainingData();

		Map<String, int[]> confusionMatrixDecisionTree = new HashMap<>();
		confusionMatrixDecisionTree.put("I4DS", new int[3]);
		confusionMatrixDecisionTree.put("IMVS", new int[3]);
		confusionMatrixDecisionTree.put("IIT", new int[3]);

		Map<String, int[]> confusionMatrixOverAll = new HashMap<>();
		confusionMatrixOverAll.put("I4DS", new int[3]);
		confusionMatrixOverAll.put("IMVS", new int[3]);
		confusionMatrixOverAll.put("IIT", new int[3]);

		Map<String, int[]> confusionMatrixHT = new HashMap<>();
		confusionMatrixHT.put("I4DS", new int[3]);
		confusionMatrixHT.put("IMVS", new int[3]);
		confusionMatrixHT.put("IIT", new int[3]);

		// perform a k=FOLD_SIZE cross validation on our training data
		for (int offset = 0; offset < trainingData.length; offset = offset + FOLD_SIZE) {
			int endIndex = trainingData.length < offset ? trainingData.length : offset + FOLD_SIZE - 1;
			LOG.error("END INDEX:" + endIndex);
			// 1) build test and train set
			Object[][] trainData = getTrainData(trainingData, offset, endIndex);
			Object[][] testData = Arrays.copyOfRange(trainingData, offset, endIndex);

			// 2) train decision tree
			DecisionTreeService decisionTreeService = BEANS.get(DecisionTreeService.class);
			Object[][] decisionTreeBaseData = prepareDataForDecisionTree(trainData);
			Object[][] dtTrain = BEANS.get(IProjectService.class).buildDecisionTreeTainMatrix(decisionTreeBaseData);
			decisionTreeService.setTrainData(dtTrain);

			// 3) train python text classifier
			BEANS.get(IProjectOfferService.class).writeTrainCSV(trainData);

			// 4) train naive classifier
			IProjectOfferInstituteClassifierService htService = BEANS
					.get(IProjectOfferInstituteClassifierService.class);
			htService.train(trainData);

			/*
			 * GET PREDICTIONS FOR OUR TEST SET
			 */
			int dtCorrect = 0;
			int pyCorrect = 0;
			int htCorrect = 0;
			int overAllCorrect = 0;

			for (Object[] row : testData) {
				// get result from decision tree
				long dtPrediction = evaluateDecisionTreePrediction(row, decisionTreeService);

				// get result from ht's classifier
				String textOfProjectOffer = (String) row[7] + (String) row[8] + (String) row[9] + (String) row[10];
				long htPrediction = htService.classifyInstitute(textOfProjectOffer);

				// get result from python
				long pythonPrediction = evaluatePythonProblemClassification(row);

				long actual = (long) row[2];

				// confusion matrix index
				int index = -1;
				String key = "";
				if (actual == InstituteCodeType.I4DSCode.ID) {
					index = 0;
					key = "I4DS";
				} else if (actual == InstituteCodeType.IMVSCode.ID) {
					index = 1;
					key = "IMVS";
				} else if (actual == InstituteCodeType.IITCode.ID) {
					index = 2;
					key = "IIT";
				}

				if (dtPrediction == actual) {
					dtCorrect++;
					confusionMatrixDecisionTree.get(key)[index] += 1;
				} else {
					int predictedIndex = -1;
					if (dtPrediction == InstituteCodeType.I4DSCode.ID) {
						predictedIndex = 0;
					} else if (dtPrediction == InstituteCodeType.IMVSCode.ID) {
						predictedIndex = 1;
					} else if (dtPrediction == InstituteCodeType.IITCode.ID) {
						predictedIndex = 2;
					}
					confusionMatrixDecisionTree.get(key)[predictedIndex] += 1;
				}

				if (pythonPrediction == actual) {
					pyCorrect++;
				}

				if (htPrediction == actual) {
					htCorrect++;
					confusionMatrixHT.get(key)[index] += 1;
				} else {
					int predictedIndex = -1;
					if (htPrediction == InstituteCodeType.I4DSCode.ID) {
						predictedIndex = 0;
					} else if (htPrediction == InstituteCodeType.IMVSCode.ID) {
						predictedIndex = 1;
					} else if (htPrediction == InstituteCodeType.IITCode.ID) {
						predictedIndex = 2;
					}
					confusionMatrixHT.get(key)[predictedIndex] += 1;
				}

				long[] evaluateInstitutes = BEANS.get(ProjectOfferService.class).evaluateInstitutes(pythonPrediction,
						dtPrediction, htPrediction);
				if (evaluateInstitutes[0] == actual) {
					overAllCorrect++;
					confusionMatrixOverAll.get(key)[index] += 1;
				} else {
					int predictedIndex = -1;
					if (evaluateInstitutes[0] == InstituteCodeType.I4DSCode.ID) {
						predictedIndex = 0;
					} else if (evaluateInstitutes[0] == InstituteCodeType.IMVSCode.ID) {
						predictedIndex = 1;
					} else if (evaluateInstitutes[0] == InstituteCodeType.IITCode.ID) {
						predictedIndex = 2;
					}
					confusionMatrixOverAll.get(key)[predictedIndex] += 1;
				}

			}

			decisionTreeResults.add(dtCorrect / (float) testData.length);
			pythonResults.add(pyCorrect / (float) testData.length);
			htResults.add(htCorrect / (float) testData.length);
			overAllResults.add(overAllCorrect / (float) testData.length);

			OptionalDouble averagePython = pythonResults.stream().mapToDouble(a -> a).average();
			LOG.error("CURRENT python average is {}",
					averagePython.isPresent() ? averagePython.getAsDouble() : "NOT PRESENT!");

			OptionalDouble averageOverAll = overAllResults.stream().mapToDouble(a -> a).average();
			LOG.error("CURRENT Over All average is {}",
					averageOverAll.isPresent() ? averageOverAll.getAsDouble() : "NOT PRESENT!");
		}

		LOG.error("CONFUSION MATRIX DECISION TREE");
		for (Map.Entry<String, int[]> entry : confusionMatrixDecisionTree.entrySet()) {
			int[] value = entry.getValue();
			LOG.error(entry.getKey() + "\t" + value[0] + "\t" + value[1] + "\t" + value[2]);
		}

		LOG.error("CONFUSION MATRIX OVER ALL");
		for (Map.Entry<String, int[]> entry : confusionMatrixOverAll.entrySet()) {
			int[] value = entry.getValue();
			LOG.error(entry.getKey() + "\t" + value[0] + "\t" + value[1] + "\t" + value[2]);
		}
		
		LOG.error("CONFUSION MATRIX HT");
		for (Map.Entry<String, int[]> entry : confusionMatrixHT.entrySet()) {
			int[] value = entry.getValue();
			LOG.error(entry.getKey() + "\t" + value[0] + "\t" + value[1] + "\t" + value[2]);
		}

		OptionalDouble average = decisionTreeResults.stream().mapToDouble(a -> a).average();
		LOG.error("decision tree average is {}", average.isPresent() ? average.getAsDouble() : "NOT PRESENT!");

		OptionalDouble averagePython = pythonResults.stream().mapToDouble(a -> a).average();
		LOG.error("python average is {}", averagePython.isPresent() ? averagePython.getAsDouble() : "NOT PRESENT!");

		OptionalDouble averageHT = htResults.stream().mapToDouble(a -> a).average();
		LOG.error("HT average is {}", averageHT.isPresent() ? averageHT.getAsDouble() : "NOT PRESENT!");

		OptionalDouble averageOverAll = overAllResults.stream().mapToDouble(a -> a).average();
		LOG.error("Over All average is {}", averageOverAll.isPresent() ? averageOverAll.getAsDouble() : "NOT PRESENT!");

		return null;
	}

	private long evaluatePythonProblemClassification(Object[] row) {
		String rawText = StringUtility.isNullOrEmpty((String) row[9]) ? "" : (String) row[9];
		ProjectOffer dummyOffer = new ProjectOffer();
		try {
			BEANS.get(ProjectOfferService.class).classifyPythonProblemClassifier(rawText, dummyOffer,
					TMP_VOICEBOT + DATA_CSV);
			return dummyOffer.getEvaluatedProblemInstitute();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException npe) {
			npe.printStackTrace();
		}

		return 0;
	}

	private Object[][] prepareDataForDecisionTree(Object[][] trainData) {
		Object[][] target = new Object[trainData.length][2];
		int rowId = 0;
		for (Object[] row : trainData) {
			target[rowId][0] = row[14];
			target[rowId][1] = row[13];
			rowId++;
		}
		return target;
	}

	private long evaluateDecisionTreePrediction(Object[] data, DecisionTreeService decisionTreeService) {
		String[] path = ((String) data[13]).split(",");

		// build answer rows (13 columns)
		Object[] answers = new Object[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		for (String answerId : path) {
			if (StringUtility.isNullOrEmpty(answerId)) {
				continue;
			}

			int index = Integer.parseInt(answerId);
			answers[index - 1] = 1;
		}

		DecisionNode rootNode = decisionTreeService.getRootNode();
		Map<String, Integer> predictions = InstituteDecisionTreeClassifier.classify(answers, rootNode);
		String result = ProjectOfferService.getMostSignificantInstitute(predictions);

		LOG.error(result);

		// extract institute
		long id = 0;
		if (result.toUpperCase().contains(InstituteCodeType.I4DS)) {
			id = InstituteCodeType.I4DSCode.ID;
		} else if (result.toUpperCase().contains(InstituteCodeType.IIT)) {
			id = InstituteCodeType.IITCode.ID;
		} else if (result.toUpperCase().contains(InstituteCodeType.IMVS)) {
			id = InstituteCodeType.IMVSCode.ID;
		} else if (result.toUpperCase().contains(InstituteCodeType.IP14)) {
			id = InstituteCodeType.IP14Code.ID;
		}

		return id;
	}

	private Object[][] getTrainData(Object[][] data, int offset, int endIndex) {
		Object[][] destination = new Object[data.length - FOLD_SIZE][data[0].length];

		int lastIndex = 0;
		for (int i = 0; i < data.length; ++i) {
			if (i >= offset && i <= endIndex) {
				continue;
			}
			destination[lastIndex++] = data[i];
		}

		return destination;
	}

}
