package ch.fhnw.thesis.projectbot.backend.server.sql;

import org.eclipse.scout.rt.platform.config.AbstractStringConfigProperty;

public class DatabaseProperties {

	public static class JdbcMappingNameProperty extends AbstractStringConfigProperty {

		@Override
		protected String getDefaultValue() {
			return "jdbc:mysql://localhost:3306/projectbot";
		}

		@Override
		public String getKey() {
			return "projectbot.database.jdbc.mapping.name";
		}

	}

}
