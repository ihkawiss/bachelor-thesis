package ch.fhnw.thesis.projectbot.backend.server.service.exception;

public class TextTooFewWordsException extends RuntimeException {

    public TextTooFewWordsException(String message) {
        super(message);
    }
}
