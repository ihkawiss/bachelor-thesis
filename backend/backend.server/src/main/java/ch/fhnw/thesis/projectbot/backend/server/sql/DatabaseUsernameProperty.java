package ch.fhnw.thesis.projectbot.backend.server.sql;

import org.eclipse.scout.rt.platform.config.AbstractStringConfigProperty;

public class DatabaseUsernameProperty extends AbstractStringConfigProperty {

	@Override
	public String getKey() {
		return "projectbot.database.username";
	}

}
