package ch.fhnw.thesis.projectbot.backend.server.data;

import org.eclipse.scout.rt.server.jdbc.lookup.AbstractSqlLookupService;

import ch.fhnw.thesis.projectbot.backend.shared.data.IInstituteLookupService;

public class InstituteLookupService extends AbstractSqlLookupService<Long> implements IInstituteLookupService {
	
	@Override
	protected String getConfiguredSqlSelect() {
		return ""
				+ "SELECT	id, name "
				+ "FROM 	institute "
				+ "WHERE	1=1 "
				+ "<key>AND id = :key</key> "
				+ "<text> AND UPPER(name) LIKE UPPER(CONCAT(:text,'%'))</text> "
				+ "<all> </all>";
	}
	
}
