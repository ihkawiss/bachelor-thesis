package ch.fhnw.thesis.projectbot.backend.server.data;

import org.eclipse.scout.rt.platform.exception.VetoException;
import org.eclipse.scout.rt.platform.holders.NVPair;
import org.eclipse.scout.rt.server.jdbc.SQL;
import org.eclipse.scout.rt.shared.TEXTS;
import org.eclipse.scout.rt.shared.services.common.jdbc.SearchFilter;
import org.eclipse.scout.rt.shared.services.common.security.ACCESS;

import ch.fhnw.thesis.projectbot.backend.server.sql.SQLs;
import ch.fhnw.thesis.projectbot.backend.shared.data.CreateProjectAdviserPermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectAdviserService;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectAdviserFormData;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectAdviserTablePageData;
import ch.fhnw.thesis.projectbot.backend.shared.data.ReadProjectAdviserPermission;
import ch.fhnw.thesis.projectbot.backend.shared.data.UpdateProjectAdviserPermission;

public class ProjectAdviserService implements IProjectAdviserService {

	@Override
	public ProjectAdviserTablePageData getProjectAdviserTableData(SearchFilter filter) {
		ProjectAdviserTablePageData pageData = new ProjectAdviserTablePageData();
		SQL.selectInto(SQLs.PROJECT_ADVISER_TABLE_SELECT, pageData);
		return pageData;
	}

	@Override
	public ProjectAdviserFormData prepareCreate(ProjectAdviserFormData formData) {
		if (!ACCESS.check(new CreateProjectAdviserPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		return formData;
	}

	@Override
	public ProjectAdviserFormData create(ProjectAdviserFormData formData) {
		if (!ACCESS.check(new CreateProjectAdviserPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		
		SQL.insert(SQLs.PROJECT_ADVISER_CREATE, formData);
		
		return formData;
	}

	@Override
	public ProjectAdviserFormData load(ProjectAdviserFormData formData) {
		if (!ACCESS.check(new ReadProjectAdviserPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		
		SQL.selectInto(SQLs.PROJECT_ADVISER_FORM_SELECT, formData);
		
		return formData;
	}

	@Override
	public ProjectAdviserFormData store(ProjectAdviserFormData formData) {
		if (!ACCESS.check(new UpdateProjectAdviserPermission())) {
			throw new VetoException(TEXTS.get("AuthorizationFailed"));
		}
		
		SQL.update(SQLs.PROJECT_ADVISER_UPDATE, formData);
		
		return formData;
	}

	@Override
	public String getAdviserProposal(Long instituteId) {
		Object[][] result = SQL.select(SQLs.PROJECT_ADVISER_PROPOSAL, new NVPair("instituteId", instituteId));
		String proposal = result.length == 0 ? "Es konnten keine passenden Betreuer gefunden werden." : "";

		for (int i = 0; i < result.length; ++i) {
			proposal += (String) result[i][0] + "<br>";
		}

		return proposal;
	}
}
