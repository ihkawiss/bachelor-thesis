package ch.fhnw.thesis.projectbot.backend.server.service;

import java.util.Map;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.CreateImmediately;

import ch.fhnw.thesis.projectbot.backend.server.decisiontree.DecisionNode;
import ch.fhnw.thesis.projectbot.backend.server.decisiontree.INode;
import ch.fhnw.thesis.projectbot.backend.server.decisiontree.InstituteDecisionTreeClassifier;
import ch.fhnw.thesis.projectbot.backend.server.decisiontree.Leaf;
import ch.fhnw.thesis.projectbot.backend.shared.data.IDecisionTreeService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectService;

@ApplicationScoped
@CreateImmediately
public class DecisionTreeService implements IDecisionTreeService {

	private Object[][] traningsData;
	private final Object[] decisionTreeHeaders;
	private final InstituteDecisionTreeClassifier classifier;

	public DecisionTreeService() {
		traningsData = BEANS.get(IProjectService.class).loadDecisionTreeTraining();

		decisionTreeHeaders = new Object[] { "ThingsFromUniverse", "ArtificialIntelligence", "BigData", "SocialNetwork",
				"ComputerGraphics", "DataVisualization", "Gameing", "UserExperiance", "Mobile", "DistributetSystem",
				"FastData", "ICT", "EfficientAlgorithms", "label" };

		classifier = new InstituteDecisionTreeClassifier();
	}

	public DecisionNode getRootNode() {
		return (DecisionNode) classifier.buildTree(traningsData);
	}

	public boolean hasNextQuestion(INode node, boolean trueBranch) {
		INode nextNode = trueBranch ? node.getTrueBranch() : node.getFalseBranch();

		if (nextNode instanceof Leaf) {
			return false;
		} else {
			return true;
		}
	}

	public Map<String, Integer> getPrediction(Leaf leaf) {
		return leaf.getPredictions();
	}

	public INode getNextNode(INode node, boolean trueBranch) {
		return trueBranch ? node.getTrueBranch() : node.getFalseBranch();
	}

	public String getQuestionText(DecisionNode node) {
		return classifier.getQuestion(node, decisionTreeHeaders);
	}

	public void setTrainData(Object[][] data) {
		traningsData = data;
	}

}
