package ch.fhnw.thesis.projectbot.backend.server.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.CreateImmediately;

import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectOfferInstituteClassifierService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteCodeType;

@ApplicationScoped
@CreateImmediately
public class ProjectOfferInstituteClassifierService implements IProjectOfferInstituteClassifierService {

	private static final int NOUN_VALUE_DEFAULT = 1;
	private static final float NOUN_VALUE_GROUP_MEMBER = 0.5F;

	@PostConstruct
	public void init() {
		Object[][] trainData = BEANS.get(IProjectService.class).loadTrainingDataInTrx();
		train(trainData);
	}

	@Override
	public void train(Object[][] trainData) {
		// clear all lists and maps
		nouns.clear();
		nounGroups.clear();
		nounGroupMembers.clear();
		imvsNouns.clear();
		imvsNounGroups.clear();
		imvsNounGroupMembers.clear();
		i4dsNouns.clear();
		i4dsNounGroups.clear();
		i4dsNounGroupMembers.clear();
		iitNouns.clear();
		iitNounGroups.clear();
		iitNounGroupMembers.clear();

		for (Object[] row : trainData) {
			Long instituteId = (Long) row[2];
			String text = row[7] + " " + row[8] + " " + row[9] + " " + row[10];
			buildInstituteNounsMap(instituteId, text);
		}
	}

	@Override
public synchronized Long classifyInstitute(String text) {
	extractNounsToLists(text);

	int imvsScore = calculateInstituteScore(InstituteCodeType.IMVSCode.ID);
	int i4DsScore = calculateInstituteScore(InstituteCodeType.I4DSCode.ID);
	int iitScore = calculateInstituteScore(InstituteCodeType.IITCode.ID);

	if (imvsScore >= i4DsScore && imvsScore >= iitScore) {
		return InstituteCodeType.IMVSCode.ID;
	} else if (i4DsScore >= imvsScore && i4DsScore >= iitScore) {
		return InstituteCodeType.I4DSCode.ID;
	} else {
		return InstituteCodeType.IITCode.ID;
	}
}

	private static final String REGEX_WORDS = "([\\wäëïöüáéíóúàèìòùâêîôûãõÄËÏÖÜÁÉÍÓÚÀÈÌÒÙÂÊÎÔÛÃÕẞß]+)";
	private static final HashSet<String> STOP_WORDS = new HashSet<>(Arrays.asList("a", "ab", "aber", "aber", "ach",
			"acht", "achte", "achten", "achter", "achtes", "ag", "alle", "allein", "allem", "allen", "aller",
			"allerdings", "alles", "allgemeinen", "als", "als", "also", "am", "an", "andere", "anderen", "andern",
			"anders", "au", "auch", "auch", "auf", "aus", "ausser", "außer", "ausserdem", "außerdem", "b", "bald",
			"bei", "beide", "beiden", "beim", "beispiel", "bekannt", "bereits", "besonders", "besser", "besten", "bin",
			"bis", "bisher", "bist", "c", "d", "da", "dabei", "dadurch", "dafür", "dagegen", "daher", "dahin",
			"dahinter", "damals", "damit", "danach", "daneben", "dank", "dann", "daran", "darauf", "daraus", "darf",
			"darfst", "darin", "darüber", "darum", "darunter", "das", "das", "dasein", "daselbst", "dass", "daß",
			"dasselbe", "davon", "davor", "dazu", "dazwischen", "dein", "deine", "deinem", "deiner", "dem",
			"dementsprechend", "demgegenüber", "demgemäss", "demgemäß", "demselben", "demzufolge", "den", "denen",
			"denn", "denn", "denselben", "der", "deren", "derjenige", "derjenigen", "dermassen", "dermaßen", "derselbe",
			"derselben", "des", "deshalb", "desselben", "dessen", "deswegen", "dich", "die", "diejenige", "diejenigen",
			"dies", "diese", "dieselbe", "dieselben", "diesem", "diesen", "dieser", "dieses", "dir", "doch", "dort",
			"drei", "drin", "dritte", "dritten", "dritter", "drittes", "du", "durch", "durchaus", "dürfen", "dürft",
			"durfte", "durften", "e", "eben", "ebenso", "ehrlich", "ei", "eigen", "eigene", "eigenen", "eigener",
			"eigenes", "ein", "einander", "eine", "einem", "einen", "einer", "eines", "einige", "einigen", "einiger",
			"einiges", "einmal", "einmal", "eins", "elf", "en", "ende", "endlich", "entweder", "entweder", "er",
			"Ernst", "erst", "erste", "ersten", "erster", "erstes", "es", "etwa", "etwas", "euch", "f", "früher",
			"fünf", "fünfte", "fünften", "fünfter", "fünftes", "für", "g", "gab", "ganz", "ganze", "ganzen", "ganzer",
			"ganzes", "gar", "gedurft", "gegen", "gegenüber", "gehabt", "gehen", "geht", "gekannt", "gekonnt",
			"gemacht", "gemocht", "gemusst", "genug", "gerade", "gern", "gesagt", "gesagt", "geschweige", "gewesen",
			"gewollt", "geworden", "gibt", "ging", "gleich", "gott", "gross", "groß", "grosse", "große", "grossen",
			"großen", "grosser", "großer", "grosses", "großes", "gut", "gute", "guter", "gutes", "h", "habe", "haben",
			"habt", "hast", "hat", "hatte", "hätte", "hatten", "hätten", "heisst", "her", "heute", "hier", "hin",
			"hinter", "hoch", "i", "ich", "ihm", "ihn", "ihnen", "ihr", "ihre", "ihrem", "ihren", "ihrer", "ihres",
			"im", "im", "immer", "in", "in", "indem", "infolgedessen", "ins", "irgend", "ist", "j", "ja", "ja", "jahr",
			"jahre", "jahren", "je", "jede", "jedem", "jeden", "jeder", "jedermann", "jedermanns", "jedoch", "jemand",
			"jemandem", "jemanden", "jene", "jenem", "jenen", "jener", "jenes", "jetzt", "k", "kam", "kann", "kannst",
			"kaum", "kein", "keine", "keinem", "keinen", "keiner", "kleine", "kleinen", "kleiner", "kleines", "kommen",
			"kommt", "können", "könnt", "konnte", "könnte", "konnten", "kurz", "l", "lang", "lange", "lange", "leicht",
			"leide", "lieber", "los", "m", "machen", "macht", "machte", "mag", "magst", "mahn", "man", "manche",
			"manchem", "manchen", "mancher", "manches", "mann", "mehr", "mein", "meine", "meinem", "meinen", "meiner",
			"meines", "mensch", "menschen", "mich", "mir", "mit", "mittel", "mochte", "möchte", "mochten", "mögen",
			"möglich", "mögt", "morgen", "muss", "muß", "müssen", "musst", "müsst", "musste", "mussten", "n", "na",
			"nach", "nachdem", "nahm", "natürlich", "neben", "nein", "neue", "neuen", "neun", "neunte", "neunten",
			"neunter", "neuntes", "nicht", "nicht", "nichts", "nie", "niemand", "niemandem", "niemanden", "noch", "nun",
			"nun", "nur", "o", "ob", "ob", "oben", "oder", "oder", "offen", "oft", "oft", "ohne", "ordnung", "p", "q",
			"r", "recht", "rechte", "rechten", "rechter", "rechtes", "richtig", "rund", "s", "sa", "sache", "sagt",
			"sagte", "sah", "satt", "schlecht", "Schluss", "schon", "sechs", "sechste", "sechsten", "sechster",
			"sechstes", "sehr", "sei", "sei", "seid", "seien", "sein", "seine", "seinem", "seinen", "seiner", "seines",
			"seit", "seitdem", "selbst", "selbst", "sich", "sie", "sieben", "siebente", "siebenten", "siebenter",
			"siebentes", "sind", "so", "solang", "solche", "solchem", "solchen", "solcher", "solches", "soll", "sollen",
			"sollte", "sollten", "sondern", "sonst", "sowie", "später", "statt", "t", "tag", "tage", "tagen", "tat",
			"teil", "tel", "tritt", "trotzdem", "tun", "u", "über", "überhaupt", "übrigens", "uhr", "um", "und", "uns",
			"unser", "unsere", "unserer", "unter", "v", "vergangenen", "viel", "viele", "vielem", "vielen",
			"vielleicht", "vier", "vierte", "vierten", "vierter", "viertes", "vom", "von", "vor", "w", "während",
			"währenddem", "währenddessen", "wann", "war", "wäre", "waren", "wart", "warum", "was", "wegen", "weil",
			"weit", "weiter", "weitere", "weiteren", "weiteres", "welche", "welchem", "welchen", "welcher", "welches",
			"wem", "wen", "wenig", "wenig", "wenige", "weniger", "weniges", "wenigstens", "wenn", "wenn", "wer",
			"werde", "werden", "werdet", "wessen", "wie", "wie", "wieder", "will", "willst", "wir", "wird", "wirklich",
			"wirst", "wo", "wohl", "wollen", "wollt", "wollte", "wollten", "worden", "wurde", "würde", "wurden",
			"würden", "x", "y", "z", "zehn", "zehnte", "zehnten", "zehnter", "zehntes", "zeit", "zu", "zuerst",
			"zugleich", "zum", "zum", "zunächst", "zur", "zurück", "zusammen", "zwanzig", "zwar", "zwar", "zwei",
			"zweite", "zweiten", "zweiter", "zweites", "zwischen", "zwölf"

			, "zudem", "evtl", "etc"));

	private static final HashSet<String> FHNW_NOUN_BLACKLIST = new HashSet<>(Arrays.asList("daten", "arbeit",
			"arbeiten", "ziel", "ziele", "projekt", "projekte", "informationen", "infos", "info", "rahmen", "verfügung",
			"hilfe", "einsatz", "methode", "methoden", "möglichkeit", "möglichkeiten", "frage", "fragen", "person",
			"personen", "institut", "schweiz", "umsetzung", "firma", "rätsel", "FHNW", "antwort", "antworten",
			"studierende", "brugg", "windisch", "imvs", "iit", "i4ds"));

	private Map<String, Float> imvsNouns = new HashMap<>();
	private Map<String, Float> i4dsNouns = new HashMap<>();
	private Map<String, Float> iitNouns = new HashMap<>();
	private Map<String, Float> imvsNounGroups = new HashMap<>();
	private Map<String, Float> i4dsNounGroups = new HashMap<>();
	private Map<String, Float> iitNounGroups = new HashMap<>();
	private Map<String, Float> imvsNounGroupMembers = new HashMap<>();
	private Map<String, Float> i4dsNounGroupMembers = new HashMap<>();
	private Map<String, Float> iitNounGroupMembers = new HashMap<>();

	private List<String> nouns = new ArrayList<>();
	private List<String> nounGroups = new ArrayList<>();
	private List<String> nounGroupMembers = new ArrayList<>();

private void buildInstituteNounsMap(Long instituteId, String text) {
	extractNounsToLists(text);
	if (instituteId.equals(InstituteCodeType.IMVSCode.ID)) {
		addNounsListToNounsMap(imvsNouns, nouns, NOUN_VALUE_DEFAULT);
		addNounsListToNounsMap(imvsNounGroups, nounGroups, NOUN_VALUE_DEFAULT);
		addNounsListToNounsMap(imvsNounGroupMembers, nounGroupMembers, NOUN_VALUE_GROUP_MEMBER);

	} else if (instituteId.equals(InstituteCodeType.I4DSCode.ID)) {
		addNounsListToNounsMap(i4dsNouns, nouns, NOUN_VALUE_DEFAULT);
		addNounsListToNounsMap(i4dsNounGroups, nounGroups, NOUN_VALUE_DEFAULT);
		addNounsListToNounsMap(i4dsNounGroupMembers, nounGroupMembers, NOUN_VALUE_GROUP_MEMBER);

	} else if (instituteId.equals(InstituteCodeType.IITCode.ID)) {
		addNounsListToNounsMap(iitNouns, nouns, NOUN_VALUE_DEFAULT);
		addNounsListToNounsMap(iitNounGroups, nounGroups, NOUN_VALUE_DEFAULT);
		addNounsListToNounsMap(iitNounGroupMembers, nounGroupMembers, NOUN_VALUE_GROUP_MEMBER);

	}
}

private void addNounsListToNounsMap(
		Map<String, Float> map, List<String> list, float value) {
	for (String noun : list) {
		if (map.containsKey(noun)) {
			map.put(noun, map.get(noun) + value);
		} else {
			map.put(noun, value);
		}
	}
}

	@Override
	public String extractNounsToString(String text) {
		return String.join(" ", extractKeywordsAsList(text));
	}

	@Override
	public List<String> extractKeywordsAsList(String text) {
		extractNounsToLists(text);

		// merge nouns with noun group members
		List<String> extractedKeyWords = new ArrayList<>();
		extractedKeyWords.addAll(nouns);
		extractedKeyWords.addAll(nounGroupMembers);
		return extractedKeyWords;
	}

	private void extractNounsToLists(String text) {
		nouns.clear();
		nounGroups.clear();
		nounGroupMembers.clear();

		Matcher matcher = Pattern.compile(REGEX_WORDS).matcher(text);

		// extract words
		List<String> words = new ArrayList<>();
		while (matcher.find()) {
			words.add(matcher.group());
		}

		// extract nouns
		for (String word : words) {
			if (Character.isUpperCase(word.codePointAt(0))) {
				nouns.add(word);
			}
		}

		// remove stop words
		nouns.removeIf(noun -> STOP_WORDS.contains(noun.toLowerCase()));

		// remove blacklisted fhnw words
		nouns.removeIf(noun -> FHNW_NOUN_BLACKLIST.contains(noun.toLowerCase()));

		// regroup words
		String textLowerCase = text.toLowerCase();
		for (int i = nouns.size() - 1; i > 0; i--) {
			// check for groups of nouns separated by spaces (Machine Learning) or by dashes (Machine-Learning)
			String group = nouns.get(i);
			String foundGroup = null;

			boolean expandGroup = true;
			int groupSize = 1;
			while (expandGroup && i >= groupSize) {
				String spaceGroup = nouns.get(i - groupSize) + " " + group;
				String dashGroup = nouns.get(i - groupSize) + "-" + group;

				if (textLowerCase.contains(spaceGroup.toLowerCase())) {
					foundGroup = spaceGroup;
					group = spaceGroup;
					expandGroup = true;
				} else if (textLowerCase.contains(dashGroup.toLowerCase())) {
					foundGroup = dashGroup;
					group = dashGroup;
					expandGroup = true;
				} else {
					expandGroup = false;
				}

				if (expandGroup) {
					groupSize++;
				}
			}

			if (foundGroup != null) {
				nounGroups.add(foundGroup);
				// remove nouns that are now part of a group
				for (int j = i; j > i - groupSize; j--) {
					String nounGroupMember = nouns.remove(j);
					nounGroupMembers.add(nounGroupMember);
				}

				// fix index
				i -= (groupSize - 1);
			}
		}
	}

	private int calculateInstituteScore(Long instituteId) {
		int score = 0;

		Map<String, Float> instituteNounsMap;
		Map<String, Float> instituteNounGroupsMap;
		Map<String, Float> instituteNounGroupMembersMap;
		if (instituteId == InstituteCodeType.IMVSCode.ID) {
			instituteNounsMap = imvsNouns;
			instituteNounGroupsMap = imvsNounGroups;
			instituteNounGroupMembersMap = imvsNounGroupMembers;

		} else if (instituteId == InstituteCodeType.I4DSCode.ID) {
			instituteNounsMap = i4dsNouns;
			instituteNounGroupsMap = i4dsNounGroups;
			instituteNounGroupMembersMap = i4dsNounGroupMembers;

		} else if (instituteId == InstituteCodeType.IITCode.ID) {
			instituteNounsMap = iitNouns;
			instituteNounGroupsMap = iitNounGroups;
			instituteNounGroupMembersMap = iitNounGroupMembers;

		} else {
			// TODO
			return 0;
		}

		Map<String, Float> fuzzyNounGroupsMap = new HashMap<>();
		for (String key : instituteNounGroupsMap.keySet()) {
			Float value = instituteNounGroupsMap.get(key);
			key = key.toLowerCase().replaceAll("-", "").replaceAll(" ", "");
			fuzzyNounGroupsMap.put(key, value);
		}

		for (String noun : nouns) {
			// calculate scores based on model
			Float counterNoun = instituteNounsMap.get(noun);
			if (counterNoun != null) {
				score += counterNoun;
			}
		}

		for (String nounGroup : nounGroups) {
			// calculate scores based on model
			Float counterNounGroup = instituteNounGroupsMap.get(nounGroup);
			if (counterNounGroup != null) {
				score += counterNounGroup;

			} else {
				String fuzzyNoun = nounGroup.toLowerCase().replaceAll("-", "").replaceAll(" ", "");
				Float counterFuzzyNounGroup = fuzzyNounGroupsMap.get(fuzzyNoun);
				if (counterFuzzyNounGroup != null) {
					score += counterFuzzyNounGroup;
				}
			}
		}

		for (String nounGroupMember : nounGroupMembers) {
			// calculate scores based on model
			Float counterNounGroupMember = instituteNounGroupMembersMap.get(nounGroupMember);
			if (counterNounGroupMember != null) {
				score += counterNounGroupMember;
			}
		}

		return score;
	}
}
