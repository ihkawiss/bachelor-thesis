package ch.fhnw.thesis.projectbot.backend.server.configuration;

import org.eclipse.scout.rt.platform.config.AbstractStringConfigProperty;

public class PythonLinearTextClassificationProperty extends AbstractStringConfigProperty {

	@Override
	public String getKey() {
		return "voicebot.python.text.linearsvc";
	}

}
