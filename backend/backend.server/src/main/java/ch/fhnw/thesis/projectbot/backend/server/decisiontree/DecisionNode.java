package ch.fhnw.thesis.projectbot.backend.server.decisiontree;

/**
 * A Decision Node asks a question.
 * 
 * This holds a reference to the question, and to the two child nodes.
 * 
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 */
public class DecisionNode implements INode {

	private final Question question;
	private final Partition branchData;
	private final INode trueBranch;
	private final INode falseBranch;

	public DecisionNode(Question question, Partition branchData, INode trueBranch, INode falseBranch) {
		this.question = question;
		this.branchData = branchData;
		this.trueBranch = trueBranch;
		this.falseBranch = falseBranch;
	}

	public Question getQuestion() {
		return question;
	}

	@Override
	public INode getTrueBranch() {
		return trueBranch;
	}

	@Override
	public INode getFalseBranch() {
		return falseBranch;
	}

	@Override
	public Partition getPartition() {
		return this.branchData;
	}

}
