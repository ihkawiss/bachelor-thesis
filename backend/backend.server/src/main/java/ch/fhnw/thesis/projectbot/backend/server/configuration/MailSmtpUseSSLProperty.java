package ch.fhnw.thesis.projectbot.backend.server.configuration;

import org.eclipse.scout.rt.platform.config.AbstractStringConfigProperty;

public class MailSmtpUseSSLProperty extends AbstractStringConfigProperty {

	@Override
	public String getKey() {
		return "mail.smtp.ssl.enable";
	}

}
