package ch.fhnw.thesis.projectbot.backend.server.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ch.fhnw.thesis.projectbot.backend.server.decisiontree.INode;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.ContactDto;

/**
 * Simple entity to track a project offer being processed.
 *
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 * @author Hoang Tran <hoang.tran@students.fhnw.ch>
 */
public class ProjectOffer {

	private final Date created;

	private String title;
	private String initialPosition;
	private String problemStatement;
	private String objective;
	private long[] evaluatedInstitutes;
	private long evaluatedProblemInstitute;
	private long decisionTreeEvaluation;
	private long naiveEvaluatedInstitute;
	private int nextDecisionTreeBreadCrumb = 1;
	private String decisionTreeBreadCrumbs = "";
	private boolean isIP14;
	private ContactDto contact;

	private INode currentNode;
	private Map<String, Integer> decisionTreePrediction;

	private final List<CrucialQuestionGroup> crucialQuestionGroups;
	private final List<CrucialQuestion> crucialQuestions;
	private int currentQuestionIndex = 0;

	public ProjectOffer() {
		created = new Date();

        // define crucial questions
        CrucialQuestion cqResearch1 = new CrucialQuestion("client_CrucialQuestionResearch1");
        CrucialQuestion cqResearch2 = new CrucialQuestion("client_CrucialQuestionResearch2");
        CrucialQuestion cqResearch3 = new CrucialQuestion("client_CrucialQuestionResearch3");
        CrucialQuestion cqSurroundingSystems = new CrucialQuestion("client_CrucialQuestionSurroundingSystems");
        CrucialQuestion cqExperience = new CrucialQuestion("client_CrucialQuestionExperience");
        CrucialQuestion cqArtificialIntelligence = new CrucialQuestion("client_CrucialQuestionArtificialIntelligence");

        // define list of question groups
        crucialQuestionGroups = new ArrayList<>(Arrays.asList(
                new CrucialQuestionGroup(2, cqResearch1, cqResearch2, cqResearch3),
                new CrucialQuestionGroup(1, cqSurroundingSystems),
                new CrucialQuestionGroup(1, cqExperience),
                new CrucialQuestionGroup(1, cqArtificialIntelligence)
        ));

		// add questions to list in different order to disperse questions of same group
		crucialQuestions = new ArrayList<>(Arrays.asList(cqResearch1, cqSurroundingSystems, cqResearch2, cqExperience,
				cqArtificialIntelligence, cqResearch3));
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setInitialPosition(String initialPosition) {
		this.initialPosition = initialPosition;
	}

	public String getInitialPosition() {
		return initialPosition;
	}

	public void setProblemStatement(String problemStatement) {
		this.problemStatement = problemStatement;
	}

	public String getProblemStatement() {
		return problemStatement;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public String getObjective() {
		return objective;
	}

	public INode getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(INode currentNode) {
		this.currentNode = currentNode;
	}

	public void setDecisionTreePrediction(Map<String, Integer> decisionTreePrediction) {
		this.decisionTreePrediction = decisionTreePrediction;
	}

	public Map<String, Integer> getDecisionTreePrediction() {
		return decisionTreePrediction;
	}

	public Date getCreated() {
		return created;
	}

	public long[] getEvaluatedInstitutes() {
		return evaluatedInstitutes;
	}

	/**
	 * Set evaluated institutes ordered by priority.
	 *
	 * @param evaluatedInstitutes
	 *            IDs of institutes
	 */
	public void setEvaluatedInstitutes(long... evaluatedInstitutes) {
		this.evaluatedInstitutes = evaluatedInstitutes;
	}

	public ContactDto getContact() {
		return contact;
	}

	public void setContact(ContactDto contact) {
		this.contact = contact;
	}

	public String getText() {
		return title + " " + initialPosition + " " + problemStatement + " " + objective;
	}

	public Long getDecisionTreeEvaluation() {
		return decisionTreeEvaluation;
	}

	public void setDecisionTreeEvaluation(long decisionTreeEvaluation) {
		this.decisionTreeEvaluation = decisionTreeEvaluation;
	}

	public boolean isDecisionTreeEvaluated() {
		return decisionTreeEvaluation != 0L;
	}

	public Long getEvaluatedProblemInstitute() {
		return evaluatedProblemInstitute;
	}

	public void setEvaluatedProblemInstitute(long evaluatedProblemInstitute) {
		this.evaluatedProblemInstitute = evaluatedProblemInstitute;
	}

	public void setNaiveEvaluatedInstitute(long classifiedInstitute) {
		this.naiveEvaluatedInstitute = classifiedInstitute;
	}

	public Long getNaiveEvaluatedInstitute() {
		return naiveEvaluatedInstitute;
	}

	public CrucialQuestion getNextCrucialQuestion() {
		CrucialQuestion nextCrucialQuestion = crucialQuestions.get(currentQuestionIndex);
		currentQuestionIndex++;
		return nextCrucialQuestion;
	}

	public boolean hasNextCrucialQuestion() {
		return currentQuestionIndex < crucialQuestions.size();
	}

	public void setCrucialQuestionAnswer(boolean answeredWithYes) {
		int lastQuestionIndex = currentQuestionIndex - 1;
		if (lastQuestionIndex >= 0) {
			crucialQuestions.get(lastQuestionIndex).setAnsweredWithYes(answeredWithYes);
		}
	}

	public float getAnswerRatio() {
		int groupYesCounter = 0;
		for (CrucialQuestionGroup crucialQuestionGroup : crucialQuestionGroups) {
			// calculate ratio of questions answered with yes
			int yesCounter = 0;
			CrucialQuestion[] crucialQuestions = crucialQuestionGroup.getCrucialQuestions();
			for (CrucialQuestion crucialQuestion : crucialQuestions) {
				if (crucialQuestion.isAnsweredWithYes()) {
					yesCounter++;
				}
			}

			// increase group yes counter by group weighting if most of the questions were
			// answered with yes
			if (yesCounter / (float) crucialQuestions.length > 0.5) {
				groupYesCounter += crucialQuestionGroup.getWeighting();
			}
		}

		// return ratio
		return groupYesCounter / (float) crucialQuestionGroups.size();
	}

	public String decisionTreePredictionToString() {
		StringBuilder value = new StringBuilder();
		for (Map.Entry<String, Integer> entry : decisionTreePrediction.entrySet()) {
			value.append(entry.getKey()).append(" => ").append(entry.getValue()).append("\n");
		}
		return value.toString();
	}

	public boolean isIP14() {
		return isIP14;
	}

	public void setIP14(boolean isIP14) {
		this.isIP14 = isIP14;
	}

	public String getDecisionTreeBreadCrumbs() {
		return decisionTreeBreadCrumbs;
	}

	public void addDecisionTreeBreadCrumb(boolean isTruebranch) {
		String delimeter = nextDecisionTreeBreadCrumb == 1 ? "" : ",";
		this.decisionTreeBreadCrumbs += isTruebranch ? delimeter + nextDecisionTreeBreadCrumb : "";
		nextDecisionTreeBreadCrumb++;
	}
}
