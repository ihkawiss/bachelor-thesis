package ch.fhnw.thesis.projectbot.backend.server.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;
import javax.security.auth.Subject;

import org.eclipse.scout.rt.platform.ApplicationScoped;
import org.eclipse.scout.rt.platform.BEANS;
import org.eclipse.scout.rt.platform.CreateImmediately;
import org.eclipse.scout.rt.platform.config.CONFIG;
import org.eclipse.scout.rt.platform.context.RunContexts;
import org.eclipse.scout.rt.platform.job.JobInput;
import org.eclipse.scout.rt.platform.job.Jobs;
import org.eclipse.scout.rt.platform.security.SimplePrincipal;
import org.eclipse.scout.rt.platform.util.StringUtility;
import org.eclipse.scout.rt.server.context.ServerRunContexts;
import org.eclipse.scout.rt.shared.TEXTS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.fhnw.thesis.projectbot.backend.server.ValidationUtil;
import ch.fhnw.thesis.projectbot.backend.server.configuration.LocalPythonEnvironmentProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.PythonCreateCSVUtilPathProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.PythonLinearTextClassificationProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.PythonTrainingDirectoryProperty;
import ch.fhnw.thesis.projectbot.backend.server.configuration.PythonTrainingFilenameProperty;
import ch.fhnw.thesis.projectbot.backend.server.decisiontree.DecisionNode;
import ch.fhnw.thesis.projectbot.backend.server.decisiontree.Leaf;
import ch.fhnw.thesis.projectbot.backend.server.domain.ProjectOffer;
import ch.fhnw.thesis.projectbot.backend.server.service.exception.TextQualityException;
import ch.fhnw.thesis.projectbot.backend.server.service.exception.TextTooFewKeywordsException;
import ch.fhnw.thesis.projectbot.backend.server.service.exception.TextTooFewWordsException;
import ch.fhnw.thesis.projectbot.backend.server.service.exception.TextTooManyWordsException;
import ch.fhnw.thesis.projectbot.backend.shared.data.IInstituteService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectAdviserService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectOfferInstituteClassifierService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectOfferService;
import ch.fhnw.thesis.projectbot.backend.shared.data.IProjectService;
import ch.fhnw.thesis.projectbot.backend.shared.data.InstituteCodeType;
import ch.fhnw.thesis.projectbot.backend.shared.data.ProjectFormData;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.CommandDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.ContactDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.FinalizeDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.QuestionDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.ResponseDto;
import ch.fhnw.thesis.projectbot.backend.shared.rest.dto.TextDto;

@ApplicationScoped
@CreateImmediately
public class ProjectOfferService implements IProjectOfferService {
	private final static Logger LOG = LoggerFactory.getLogger(ProjectOfferService.class);

	private static final int MIN_WORD_COUNT = 40;
	private static final int AVG_WORD_COUNT = 130;
	private static final int MIN_CHAR_COUNT = 300;
	private static final int AVG_CHAR_COUNT = 900;
	private static final int MAX_CHAR_COUNT = 5000;
	private static final int MIN_KEYWORD_COUNT = 25;
	private static final float THRESHOLD_IP14_ANSWER_RATIO = 0.5F;

	private String DATA_CSV;
	private String TMP_VOICEBOT;

	private Map<String, ProjectOffer> currentRequests;

	public ProjectOfferService() {
		currentRequests = new HashMap<>();
	}

	@PostConstruct
	public void postConstruct() {
		DATA_CSV = CONFIG.getPropertyValue(PythonTrainingFilenameProperty.class);
		TMP_VOICEBOT = CONFIG.getPropertyValue(PythonTrainingDirectoryProperty.class);

		assert DATA_CSV != null;
		assert TMP_VOICEBOT != null;

		prepareTrainDateForPython();
	}

	/**
	 * Loads available training data from the database and creates a corpus as CSV
	 * file.
	 */
	private void prepareTrainDateForPython() {
		Object[][] trainingData = BEANS.get(IProjectService.class).loadTrainingDataInTrx();
		writeTrainCSV(trainingData);
	}

	/**
	 * Creates a new project offer request entry for given token.
	 *
	 * @param token
	 *            unique id of client sending request
	 */
	@Override
	public void createProjectOfferRequest(String token) {
		if (currentRequests.containsKey(token)) {
			throw new IllegalArgumentException("token already in use");
		}

		LOG.info("created a new project offer request for token={}", token);
		currentRequests.put(token, new ProjectOffer());
	}

	/**
	 * Validation of a user entered project title.
	 *
	 * @param dto
	 *            entered title to validate
	 * @return true if title passed validation
	 */
	@Override
	public ResponseDto processTitle(TextDto dto) {
		String cleaned = getValidatedTitle(dto.getText());
		if (!currentRequests.containsKey(dto.getToken())) {
			throw new IllegalArgumentException("invalid token");
		}

		if (cleaned == null) {
			ResponseDto response = new ResponseDto();
			String textId = "client_InvalidTitle";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
			return response;
		}

		LOG.debug("accepted title={}", cleaned);
		currentRequests.get(dto.getToken()).setTitle(cleaned);
		return new ResponseDto((new CommandDto(CommandDto.DO_SUCCESS, TEXTS.get("client_TitleValid"))));
	}

	@Override
	public ResponseDto processInitialPosition(TextDto dto) {
		ResponseDto response = new ResponseDto();
		if (!currentRequests.containsKey(dto.getToken())) {
			throw new IllegalArgumentException("invalid token");
		}

		String text = null;
		try {
			text = validateText(dto);
			LOG.debug("accepted initial position={}", text);
			currentRequests.get(dto.getToken()).setInitialPosition(text);
			response.addCommand(new CommandDto(CommandDto.DO_SUCCESS, TEXTS.get("client_InitialPositionValid")));
		} catch (TextQualityException tqe) {
			LOG.info("accepted initial position with lower quality={}", text);
			response.addCommand(new CommandDto(CommandDto.DO_NOTICE, TEXTS.get("client_InitialPositionBadQuality")));
			currentRequests.get(dto.getToken()).setInitialPosition(dto.getText());
		} catch (TextTooFewWordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_InitialPositionInvalid";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
		} catch (TextTooManyWordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_InitialPositionTooLong";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));

		} catch (TextTooFewKeywordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_InitialPositionBadQuality";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
		}

		return response;
	}

	@Override
	public ResponseDto processProblemStatement(TextDto dto) {
		ResponseDto response = new ResponseDto();
		if (!currentRequests.containsKey(dto.getToken())) {
			throw new IllegalArgumentException("invalid token");
		}

		String text = null;
		try {
			text = validateText(dto);
			LOG.debug("accepted problem statement={}", text);
			currentRequests.get(dto.getToken()).setProblemStatement(text);
			response.addCommand(new CommandDto(CommandDto.DO_SUCCESS, TEXTS.get("client_ProblemStatementValid")));
		} catch (TextQualityException tqe) {
			text = dto.getText(); // accept anyway
			LOG.info("accepted problem statement with lower quality={}", text);
			currentRequests.get(dto.getToken()).setProblemStatement(text);
			response.addCommand(new CommandDto(CommandDto.DO_NOTICE, TEXTS.get("client_ProblemStatementBadQuality")));

		} catch (TextTooFewWordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_ProblemStatementInvalid";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));

		} catch (TextTooManyWordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_ProblemStatementTooLong";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));

		} catch (TextTooFewKeywordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_ProblemStatementBadQuality";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
		}

		if (text != null) {
			ProjectOffer projectOffer = currentRequests.get(dto.getToken());
			final String nouns = BEANS.get(IProjectOfferInstituteClassifierService.class).extractNounsToString(text);
			scheduleProblemClassificationAsync(nouns, projectOffer, TMP_VOICEBOT + DATA_CSV);
		}

		return response;
	}

	public void invokePythonClassifierAsync(String text, ProjectOffer projectOffer, String trainingDirectory) {
		final String nouns = BEANS.get(IProjectOfferInstituteClassifierService.class).extractNounsToString(text);
		scheduleProblemClassificationAsync(nouns, projectOffer, trainingDirectory);
	}

	public void scheduleProblemClassificationAsync(final String acceptedText, ProjectOffer projectOffer,
			String trainCSV) {
		// schedule problem classification
		Jobs.schedule(() -> {
			classifyPythonProblemClassifier(acceptedText, projectOffer, trainCSV);
		}, new JobInput().withRunContext(ServerRunContexts.copyCurrent(true)));
	}

	public String classifyPythonProblemClassifier(final String acceptedText, ProjectOffer projectOffer, String trainCSV)
			throws IOException {

		final String python = CONFIG.getPropertyValue(LocalPythonEnvironmentProperty.class);
		final String script = CONFIG.getPropertyValue(PythonLinearTextClassificationProperty.class);

		Runtime runtime = Runtime.getRuntime();
		Process p = runtime.exec(new String[] { python, script, acceptedText, trainCSV });
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

		String output = null;
		while ((output = stdInput.readLine()) != null) {
			LOG.info("python problem classification predicts: {}", output);
			projectOffer.setEvaluatedProblemInstitute(InstituteCodeType.stringToInstituteId(output));
		}

		// read any errors from the attempted command
		String error = null;
		while ((output = stdError.readLine()) != null) {
			error += output;
		}

		if (error != null) {
			LOG.error(error);
		}

		return output;
	}

	public String invokeTrainCSVCreation(String dir, String filename) throws IOException {
		final String python = CONFIG.getPropertyValue(LocalPythonEnvironmentProperty.class);
		final String script = CONFIG.getPropertyValue(PythonCreateCSVUtilPathProperty.class);

		Runtime runtime = Runtime.getRuntime();
		Process p = runtime.exec(new String[] { python, script, dir, dir + filename });
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

		String output = null;
		while ((output = stdInput.readLine()) != null) {
			LOG.info(output);
		}

		// read any errors from the attempted command
		String error = null;
		while ((output = stdError.readLine()) != null) {
			error += output;
		}

		if (error != null) {
			LOG.error(error);
		}

		return output;
	}

	/**
	 * Writes a two dimensional array, representing the training data, to the
	 * TMP_VOICEBOT directory. After each training document was successfully
	 * written, a Python script is invoked to create a training CSV corpus.
	 */
	@Override
	public void writeTrainCSV(Object[][] trainData) {
		new File(TMP_VOICEBOT).mkdirs();
		Arrays.stream(new File(TMP_VOICEBOT).listFiles()).forEach(File::delete);

		for (Object[] row : trainData) {
			String institute = InstituteCodeType.idToInstituteString((Long) row[2]);

			String text = (String) row[7] + " " + (String) row[8] + " " + (String) row[9] + " " + (String) row[10];
			String nouns = BEANS.get(IProjectOfferInstituteClassifierService.class).extractNounsToString(text);

			try {
				String path = TMP_VOICEBOT + System.currentTimeMillis() + "_" + institute + ".txt";
				BufferedWriter writer = new BufferedWriter(new FileWriter(path));

				writer.write(nouns);
				writer.close();
			} catch (IOException e) {
				LOG.error("failed to save training data", e);
			}

		}

		// invoke csv creation
		try {
			invokeTrainCSVCreation(TMP_VOICEBOT, DATA_CSV);
		} catch (IOException e) {
			throw new RuntimeException("unable to create train csv!");
		}
	}

	@Override
	public ResponseDto processObjective(TextDto dto) {
		ResponseDto response = new ResponseDto();
		if (!currentRequests.containsKey(dto.getToken())) {
			throw new IllegalArgumentException("invalid token");
		}

		String text = null;
		try {
			text = validateText(dto);
			LOG.debug("accepted objective={}", text);
			currentRequests.get(dto.getToken()).setObjective(text);
			response.addCommand(new CommandDto(CommandDto.DO_SUCCESS, TEXTS.get("client_ObjectiveValid")));
		} catch (TextQualityException tqe) {
			text = dto.getText();
			currentRequests.get(dto.getToken()).setObjective(text);
			LOG.info("accepted objective with lower quality={}", text);
			response.addCommand(new CommandDto(CommandDto.DO_NOTICE, TEXTS.get("client_ObjectiveBadQuality")));

		} catch (TextTooFewWordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_ObjectiveInvalid";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));

		} catch (TextTooManyWordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_ObjectiveTooLong";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));

		} catch (TextTooFewKeywordsException e) {
			LOG.info("rejected initial position={}", text);
			String textId = "client_ObjectiveBadQuality";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
		}

		if (text != null) {
			ProjectOffer projectOffer = currentRequests.get(dto.getToken());
			scheduleNaiveTextClassifier(text, projectOffer);
		}

		return response;
	}

	private void scheduleNaiveTextClassifier(String text, ProjectOffer offer) {
		Jobs.schedule(() -> {
			Long classifiedInstitute = BEANS.get(IProjectOfferInstituteClassifierService.class).classifyInstitute(text);
			offer.setNaiveEvaluatedInstitute(classifiedInstitute);
		}, new JobInput().withRunContext(ServerRunContexts.copyCurrent(true)));
	}

	@Override
	public ResponseDto getNextQuestion(QuestionDto dto) {
		ResponseDto response = new ResponseDto();
		if (!currentRequests.containsKey(dto.getToken())) {
			throw new IllegalArgumentException("invalid token");
		}

		DecisionTreeService service = BEANS.get(DecisionTreeService.class);

		// create new node if there is none
		ProjectOffer offer = currentRequests.get(dto.getToken());

		if (offer.isDecisionTreeEvaluated()) {
			return getNextCrucialQuestion(offer, response, dto);
		}

		if (offer.getCurrentNode() == null) {
			DecisionNode rootNode = service.getRootNode();
			String question = service.getQuestionText(rootNode);
			offer.setCurrentNode(rootNode);
			String textId = "client_" + question;
			response.addCommand(new CommandDto(CommandDto.ASK_QUESTION, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.EXPLANATION, TEXTS.get(textId + "Explanation")));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
			return response;
		}

		// ask next question if there is one
		if (service.hasNextQuestion(offer.getCurrentNode(), dto.isProceedTrueBranch())) {
			DecisionNode node = (DecisionNode) service.getNextNode(offer.getCurrentNode(), dto.isProceedTrueBranch());
			String question = service.getQuestionText(node);
			offer.setCurrentNode(node);
			String textId = "client_" + question;
			offer.addDecisionTreeBreadCrumb(dto.isProceedTrueBranch());
			response.addCommand(new CommandDto(CommandDto.ASK_QUESTION, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.EXPLANATION, TEXTS.get(textId + "Explanation")));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
			return response;
		} else {
			Leaf leaf = (Leaf) service.getNextNode(offer.getCurrentNode(), dto.isProceedTrueBranch());
			offer.setDecisionTreePrediction(service.getPrediction(leaf));
			offer.addDecisionTreeBreadCrumb(dto.isProceedTrueBranch());
			LOG.error(offer.decisionTreePredictionToString());

			// check against decision tree output
			String mostSignificant = getMostSignificantInstitute(offer.getDecisionTreePrediction());

			offer.setDecisionTreeEvaluation(InstituteCodeType.stringToInstituteId(mostSignificant));

			return getNextCrucialQuestion(offer, response, dto);
		}
	}

	public static String getMostSignificantInstitute(Map<String, Integer> predictions) {
		String mostSignificant = "";
		int mostTimes = 0;
		for (Map.Entry<String, Integer> item : predictions.entrySet()) {
			if (mostTimes < item.getValue()) {
				mostTimes = item.getValue();
				mostSignificant = item.getKey();
			}
		}
		return mostSignificant;
	}

	private ResponseDto getNextCrucialQuestion(ProjectOffer offer, ResponseDto response, QuestionDto dto) {
		// check if institute already evaluated
		if (offer.getEvaluatedInstitutes() == null) {
			// get classified institutes
			long pythonProblemClassifierResult = offer.getEvaluatedProblemInstitute();
			long decisionTreeResult = offer.getDecisionTreeEvaluation();
			long naiveClassifierResult = offer.getNaiveEvaluatedInstitute();
			LOG.error("Classification of Project: python: {}, decisionTree: {}, naive: {}",
					pythonProblemClassifierResult, decisionTreeResult, naiveClassifierResult);
			// evaluate institute based on results of classifiers and set result on project
			// offer
			long[] evaluateInstitutes = evaluateInstitutes(pythonProblemClassifierResult, decisionTreeResult,
					naiveClassifierResult);
			offer.setEvaluatedInstitutes(evaluateInstitutes);
		}

		// ask crucial questions
		if (offer.hasNextCrucialQuestion()) {
			// save answer to question
			offer.setCrucialQuestionAnswer(dto.isProceedTrueBranch());

			// return next question
			String question = offer.getNextCrucialQuestion().getQuestion();
			response.addCommand(new CommandDto(CommandDto.ASK_QUESTION, TEXTS.get(question)));
			response.addCommand(
					new CommandDto(CommandDto.EXPLANATION, TEXTS.getWithFallback(question + "Explanation", "")));
			response.addCommand(new CommandDto(CommandDto.SPEAK, question));
			return response;

		} else {
			// save answer to question
			offer.setCrucialQuestionAnswer(dto.isProceedTrueBranch());

			// evaluate answers
			if (offer.getAnswerRatio() < THRESHOLD_IP14_ANSWER_RATIO) {
				// answer ratio is below threshold, therefore it evaluated as an IP14 project
				offer.setEvaluatedInstitutes(InstituteCodeType.IP14Code.ID);
			}
		}

		response.addCommand(new CommandDto(CommandDto.DO_SUCCESS, TEXTS.get("client_DecisionTreeOk")));
		return response;
	}

	/**
	 * Evaluates institutes based on results of classifiers.
	 *
	 * @param pythonClassifierEvaluation
	 *            ID of institute evaluated by python classifier
	 * @param decisionTreeEvaluation
	 *            ID of institute evaluated by decision tree
	 * @param naiveClassifierEvaluation
	 *            ID of institute evaluated by naive classifier
	 * @return Array of possible institutes IDs ordered by confidence
	 */
	public long[] evaluateInstitutes(long pythonClassifierEvaluation, long decisionTreeEvaluation,
			long naiveClassifierEvaluation) {
		// TODO implement using weighting and confidence of classifiers

		if (pythonClassifierEvaluation == decisionTreeEvaluation
				&& decisionTreeEvaluation == naiveClassifierEvaluation) {
			// 3/3 classifiers have same evaluated institute: that institute will be the
			// only one to be returned
			return new long[] { pythonClassifierEvaluation };

		} else if (pythonClassifierEvaluation == decisionTreeEvaluation) {
			// 2/3 classifiers have same evaluated institute: institute that has been
			// evaluated by two classifiers will
			// be priority 1 and the other institute priority 2
			return new long[] { pythonClassifierEvaluation, naiveClassifierEvaluation };

		} else if (decisionTreeEvaluation == naiveClassifierEvaluation) {
			// 2/3 classifiers have same evaluated institute: institute that has been
			// evaluated by two classifiers will
			// be priority 1 and the other institute priority 2
			return new long[] { decisionTreeEvaluation, pythonClassifierEvaluation };

		} else if (pythonClassifierEvaluation == naiveClassifierEvaluation) {
			// 2/3 classifiers have same evaluated institute: institute that has been
			// evaluated by two classifiers will
			// be priority 1 and the other institute priority 2
			return new long[] { pythonClassifierEvaluation, decisionTreeEvaluation };

		} else {
			// all classifiers have different evaluated institute: return all institutes
			return new long[] { pythonClassifierEvaluation, decisionTreeEvaluation, naiveClassifierEvaluation };
		}
	}

	@Override
	public ResponseDto processContactData(ContactDto dto) {
		ResponseDto response = new ResponseDto();
		if (!currentRequests.containsKey(dto.getToken())) {
			throw new IllegalArgumentException("invalid token");
		}

		if (!dto.isValid()) {
			String textId = "client_ContactDataInvalid";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
			return response;
		}

		// validate mail
		if (!ValidationUtil.isValidMail(dto.getMail())) {
			String textId = "client_ContactDataInvalidMail";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
			return response;
		}

		// validate phone number
		if (!StringUtility.isNullOrEmpty(dto.getPhone()) && !ValidationUtil.isValidPhone(dto.getPhone())) {
			String textId = "client_ContactDataInvalidPhone";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
			return response;
		}

		// validate plz
		if (!StringUtility.isNullOrEmpty(dto.getPostcodeCity()) && !ValidationUtil.isValidPLZ(dto.getPostcodeCity())) {
			String textId = "client_ContactDataInvalidPLZ";
			response.addCommand(new CommandDto(CommandDto.DO_ERROR, TEXTS.get(textId)));
			response.addCommand(new CommandDto(CommandDto.SPEAK, textId));
			return response;
		}

		ProjectOffer offer = currentRequests.get(dto.getToken());
		offer.setContact(dto);
		response.addCommand(new CommandDto(CommandDto.DO_SUCCESS, TEXTS.get("client_ContactDataAccepted")));

		return response;
	}

	private String buildMailMessage(ProjectOffer offer, String adviserProposal) {
		StringBuilder message = new StringBuilder();
		// class of project and institute name
		long[] evaluatedInstitutes = offer.getEvaluatedInstitutes();

		message.append("Sehr geehrte Damen und Herren<br><br>");
		message.append("Die folgende Projektanfrage ist soeben über den Projekt-Bot eingereicht worden. <br><br>");

		// title
		message.append("<b>Titel</b><br>" + offer.getTitle());

		// class of project
		message.append("<br><br><b>Vorgeschlagene Projekt-Klasse</b>: ");
		long priorityOneInstitute = evaluatedInstitutes[0];
		if (priorityOneInstitute == InstituteCodeType.IP14Code.ID) {
			message.append("IP14");
			offer.setIP14(true);
		} else {
			message.append("IP56");

			// evaluated institute
			message.append("<br><b>Vorgeschlagenes Institut:</b> ")
					.append(InstituteCodeType.idToInstituteString(priorityOneInstitute));
			if (evaluatedInstitutes.length > 1) {
				message.append(", (");
				for (int i = 1; i < evaluatedInstitutes.length; i++) {
					if (i > 1) {
						message.append(", ");
					}
					message.append("<i>").append(InstituteCodeType.idToInstituteString(evaluatedInstitutes[i]))
							.append("</i>");
				}
				message.append(")");
			}
		}

		// adviser proposal
		message.append("<br><br><b>Vorgeschlagene Betreuer</b><br>");
		message.append(adviserProposal);

		// contact information
		message.append("<br><b>Kontakinformationen des Kunden</b>");
		message.append("<br>").append(offer.getContact().getName()).append("<br>")
				.append(offer.getContact().getInstitution());

		if (!StringUtility.isNullOrEmpty(offer.getContact().getAddress())
				&& !StringUtility.isNullOrEmpty(offer.getContact().getPostcodeCity())) {
			message.append("<br>" + offer.getContact().getAddress());
			message.append("<br>" + offer.getContact().getPostcodeCity());
		}

		message.append("<br>").append(offer.getContact().getMail());

		if (!StringUtility.isNullOrEmpty(offer.getContact().getPhone())) {
			message.append("<br>").append(offer.getContact().getPhone());
		}

		message.append("<br><br><b>Ausgangslage</b><br>");
		message.append(offer.getInitialPosition());

		message.append("<br><br><b>Problemstellung</b><br>");
		message.append(offer.getProblemStatement());

		message.append("<br><br><b>Zielsetzung</b><br>");
		message.append(offer.getObjective() + "<br><br>");
		message.append("Freundliche Grüsse<br><br>");
		message.append("Der Projekt Bot");
		return message.toString();
	}

	/**
	 * Validates given text regarding min and max char / word count.
	 *
	 * @param dto
	 *            TextDto to validate
	 * @return validated text to save into current request
	 */
	private String validateText(TextDto dto) {
		String text = StringUtility.cleanup(dto.getText());
		String[] tokens = StringUtility.tokenize(text, ' ');
		if (tokens.length < MIN_WORD_COUNT) {
			throw new TextTooFewWordsException("word count too low");
		}

		int textLength = text.replace(" ", " ").length();
		if (textLength < MIN_CHAR_COUNT) {
			throw new TextTooFewWordsException("char count too low");
		}

		if (textLength > MAX_CHAR_COUNT) {
			throw new TextTooManyWordsException("char count too high");
		}

		List<String> keywords = BEANS.get(IProjectOfferInstituteClassifierService.class).extractKeywordsAsList(text);
		if (keywords.size() < MIN_KEYWORD_COUNT) {
			throw new TextTooFewKeywordsException("keywords count too low");
		}

		if (textLength < AVG_CHAR_COUNT) {
			throw new TextQualityException(TEXTS.get("client_LowerCharCount"));
		}

		if (tokens.length < AVG_WORD_COUNT) {
			throw new TextQualityException(TEXTS.get("client_LowerWordCount"));
		}

		return text;
	}

	/**
	 * Cleans and validates given title.
	 *
	 * @param title
	 *            to process
	 * @return null if title is invalid, processed title otherwise
	 */
	private String getValidatedTitle(String title) {
		if (StringUtility.isNullOrEmpty(title)) {
			LOG.debug("title was null or empty, reject");
			return null;
		}

		String cleaned = StringUtility.cleanup(title);

		// check if title length is at least 15 chars
		if (cleaned.replaceAll(" ", "").length() < 15) {
			LOG.debug("title was too short, reject");
			return null;
		}

		// check if is numeric input
		if (cleaned.chars().allMatch(Character::isDigit)) {
			LOG.debug("title was a number, reject");
			return null;
		}

		// check if contains chars
		if (cleaned.chars().noneMatch(Character::isAlphabetic)) {
			LOG.debug("title was not alphanumeric, reject");
			return null;
		}

		return cleaned;
	}

	@Override
	public void finalizeRequest(FinalizeDto dto) {
		if (!currentRequests.containsKey(dto.getToken())) {
			throw new IllegalArgumentException("invalid token");
		}

		ProjectOffer offer = currentRequests.get(dto.getToken());

		// read project adviser proposals
		Subject subject = new Subject();
		subject.getPrincipals().add(new SimplePrincipal("voicebot"));
		subject.setReadOnly();
		String adviserProposal = RunContexts.empty().withSubject(subject).withLocale(Locale.US)
				.call(new Callable<String>() {
					@Override
					public String call() throws Exception {
						return BEANS.get(IProjectAdviserService.class)
								.getAdviserProposal(offer.getEvaluatedInstitutes()[0]);
					}
				});

		// build mail message
		String message = buildMailMessage(offer, adviserProposal);

		// save project offer into database
		ProjectFormData data = new ProjectFormData();
		data.getTitle().setValue(offer.getTitle());
		data.getInstitute().setValue(offer.getEvaluatedInstitutes()[0]);
		data.getCustomer().setValue(offer.getContact().getInstitution());
		data.getProjectClass().setValue(offer.isIP14() ? 1l : 2l);
		data.getSummary().setValue(offer.getInitialPosition());
		data.getTarget().setValue(offer.getObjective());
		data.getProblem().setValue(offer.getProblemStatement());
		data.getSkills().setValue("");
		data.getInstituteFeaturesProperty().setValue(offer.getDecisionTreeBreadCrumbs());
		data.getQuarantaine().setValue(true);

		RunContexts.empty().withSubject(subject).withLocale(Locale.US).run(() -> {
			BEANS.get(IProjectService.class).create(data);
		});

		String advisorAdresses = RunContexts.empty().withSubject(subject).withLocale(Locale.US).call(new Callable<String>() {
			@Override
			public String call() throws Exception {
				return BEANS.get(IInstituteService.class).getAdvisorAdressesByInstitute(data.getInstitute().getValue());
			}
		});
		
		String ccAdresses = RunContexts.empty().withSubject(subject).withLocale(Locale.US).call(new Callable<String>() {
			@Override
			public String call() throws Exception {
				return BEANS.get(IInstituteService.class).getAdvisorRemainingAdresses(data.getInstitute().getValue());
			}
		});

		// trigger mail send
		BEANS.get(MailService.class).sendMail(message, advisorAdresses, ccAdresses, "Neues Projekt");
	}

}
