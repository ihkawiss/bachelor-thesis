package ch.fhnw.thesis.projectbot.backend.server.decisiontree;

/**
 * Represents a split within the decision tree.
 * 
 * @author Kevin Kirn <kevin.kirn@students.fhnw.ch>
 */
public class Split {

	private final double gain;
	private final Question question;

	public Split(double gain, Question question) {
		this.gain = gain;
		this.question = question;
	}

	public double getGain() {
		return gain;
	}

	public Question getQuestion() {
		return question;
	}

}
