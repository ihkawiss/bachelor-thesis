package ch.fhnw.thesis.projectbot.backend.server.service.exception;

public class TextTooFewKeywordsException extends RuntimeException {

    public TextTooFewKeywordsException(String message) {
        super(message);
    }
}
