package ch.fhnw.thesis.projectbot.backend.server.configuration;

import org.eclipse.scout.rt.platform.config.AbstractStringConfigProperty;

public class MailSmtpPortProperty extends AbstractStringConfigProperty {

	@Override
	public String getKey() {
		return "mail.smtp.port";
	}

}
