package ch.fhnw.thesis.projectbot.backend.server.domain;

import java.util.List;
import java.util.Locale;

/**
 * @author Hoang Tran <hoang.tran@students.fhnw.ch>
 */
public class ProjectOfferDataHolder {
    public String id;
    public String title;
    public Long instituteId;
    public String initialPosition;
    public String objective;
    public String problemStatement;
    public String technologies;
    public Locale language;
    public List<String> advisors;
}


